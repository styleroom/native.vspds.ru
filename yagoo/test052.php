<?php
/**
 * http://www.gps-coordinates.net/
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title>Place Autocomplete</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/test051.css">
</head>
<body>
<input id="pac-input" class="controls" type="text"
       placeholder="Enter a location">
<div id="map" style="width: 90%; height: 60%;"></div>
<div id="address"></div>

<script src="js/test052.js" async defer></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoM1pP9bTkecUBIDf-oi7JH31J9xyAGqc&libraries=places&callback=initMap&language=en"
        async defer></script>
</body>
</html>