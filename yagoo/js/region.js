$(document).on('ready pjax:success', function () {
    var $region = $('.region');

    if($region.length) {
        var $city = $region.find('#header_region_city');
        var $icon = $region.find('#icon');

        try {
            var regionInput = document.querySelector("input#region");
            if (regionInput) {
                var awesomplete = new Awesomplete(regionInput, {
                    minChars: 2
                });
                var regionSelector = regionInput;
                regionSelector.addEventListener('awesomplete-select', function (e) {
                    var place = e.text;
                    var name = place.label;
                    var region_kladr_id = place.value;
                    $.ajax('/visitor_region/region/setRegion', {
                        method: 'GET',
                        data: {
                            kladr: region_kladr_id,
                            name: name
                        },
                        beforeSend: function() {
                            aristosAjax.buttonBlockUI({target: $region});
                        },
                        success: function (data) {
                            $city.text(data);
                            var $productInfo = $('#product-info');
                            var $shippingInfoWrap = $('.shipping-info-wrap');
                            if($productInfo.length && $shippingInfoWrap.length) {
                                $.pjax.reload('.shipping-info-wrap', {
                                    timeout: 10000,
                                    push: false,
                                    fragment: '.shipping-info-wrap'
                                });
                            }
                        },
                        complete: function () {
                            aristosAjax.buttonUnblockUI($region);
                        }
                    });
                });
                regionSelector.addEventListener('awesomplete-selectcomplete', function (e) {
                    var place = e.text;
                    $('input#region').val(place.label);
                });
                var findPlacesTimeout = null;
                $region.on('keyup', 'input#region', function (e) {
                    if (e.which <= 90 && e.which >= 48) {
                        if (findPlacesTimeout) clearTimeout(findPlacesTimeout);
                        var $target = $(e.target);
                        if($target.val().length > 2) {
                            findPlacesTimeout = setTimeout(function () {
                                $.ajax('/findAddress.php', {
                                    method: 'POST',
                                    dataType: 'json',
                                    data: {
                                        fields: {
                                            city: $target.val()
                                        }
                                    },
                                    success: function (data) {
                                        var regions = [];
                                        var places = data.places;
                                        if (typeof places != 'undefined') {
                                            for (var i in places) {
                                                if (places.hasOwnProperty(i)) {
                                                    var elem = places[i];
                                                    var place = {
                                                        label: elem.displayName,
                                                        value: elem.region_kladr_id
                                                    };
                                                    regions.push(place);
                                                }
                                            }
                                            awesomplete.list = regions;
                                        }
                                    }
                                });
                            }, 500);
                        }
                    }
                });
            }
        } catch(e) {
            console.error(e);
        }
    }

});
