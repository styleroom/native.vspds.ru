/**
 * Created by papasha on 19.09.2017.
 */
/**
 * Ceske Budejovice, Czechia
 Latitude: 48.975658 | Longitude: 14.480255
 */

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 48.975658, lng: 14.480255},
        zoom: 5
    });
    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input'));

    var types = document.getElementById('type-selector');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29),
        draggable: true
    });

    google.maps.event.addListener(marker, 'dragend', function(ev){
        markerNewPlace(marker);
    });

    // --- MY code ------------------------------------------------------------------------------------------------------------

    /**
     * Не все адреса могут быть взяты подробно
     * @param lat
     * @param lng
     */

    function codeLatLng(e) {
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({
            latLng: e
        }, function(o, n) {
            n === google.maps.GeocoderStatus.OK ? o[1] ? (marker.setPosition(e),
                infowindow.setContent(o[1].formatted_address),
                infowindow.open(map, marker), document.getElementById("address").innerHTML = o[1].formatted_address) : alert("No results found") : alert("Geocoder failed due to: " + n);
            console.log('o',o);
            console.log('o[1]',o[1]);
            console.log('o[1].formatted_address',o[1].formatted_address);
            console.log('o[1].address_components',o[1].address_components);
        });
    }

    /**
     * Fill form value object
     * @param arrAddresses
     * @returns {{country: string, postal_code: string, street_number: string, locality: string}}
     */
    function formValues(arrAddresses) {
        var result = {
            country:'',
            postal_code:'',
            street_number:'',
            locality:''
        };
        for (var i = 0; i <= arrAddresses.length; i++) {
            var now = arrAddresses[i];
            if (typeof now !== 'undefined') {
                var type = now.types[0];
                var value = now.long_name;
                switch (type) {
                    case 'country':
                        result.country = value;
                        break;
                    case 'postal_code':
                        result.postal_code = value;
                        break;
                    case 'street_number':
                        result.street_number = value;
                        break;
                    case 'locality':
                        result.locality = value;
                        break;
                }
            }
        }
        console.log('arrAddresses',arrAddresses);
        return result;
    }
    
    function redrawInfoWindow(place) {
        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
        infowindow.setContent('<div><strong>Address:</strong><br>' + address + '</div>');
        infowindow.open(map, marker);
    }

    /**
     * Event end drag marker
     * @param marker
     */
    function markerNewPlace(marker) {
        var pos = marker.getPosition();
        var lat = pos.lat();
        var lng = pos.lng();
        var o = new google.maps.LatLng(lat,lng);
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({
            latLng: o
        }, function(o) {
            var place = o[1];
            redrawInfoWindow(place);
            var arrAddresses = place.address_components;
            var result = formValues(arrAddresses);
            console.log('result',result);
            console.log("Marker draged! Lat=" + lat + "; Lng=" + lng);
        });
    }

    // --- MY code ------------------------------------------------------------------------------------------------------------

    autocomplete.addListener('place_changed', function(event) {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
        redrawInfoWindow(place);
        var arrAddresses = place.address_components;
        var result = formValues(arrAddresses);
        console.log('result',result);
        console.log('place change!');
    });

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
        });
    }
}