;
(function ($, window, document, undefined) {

    $.fn.extend({
        listAddress: function (callback) {
            var $self = this;
            $self.block();
            $.get('/aristoscheckout/onepage/listAddress', {}, function (text) {
                $self.unblock();
                $self.showSmooth(text, function () {
                    bindAddressEvents();
                    if (typeof callback == "function") callback();
                });
            });
        }
    });

    $(document).on('ready pjax:success', function () {

        var $addressForm = $('.address_form');
        var $newAddressForm = $("#new-address-form");
        if(!$newAddressForm.length) {
            $newAddressForm = $('<div id="new-address-form" class="a-hidden clear"/>');
            $newAddressForm.append($addressForm);
        }

        var $addressesCont = $addressForm.find('.addresses');
        //Выбор адреса
        $addressesCont.on('block.item.selected', 'li', function (e) {
            var $address = $(this);
            var addressId = $address.data('id');
            if (!$address.hasClass('add')) {
                $addressForm.trigger('address.selected', {
                    target: $address,
                    id: addressId
                });
            }
        });
        //Редактирование адресов
        $addressesCont.on('click', '.btn-change-address', function(e){
            $(document).trigger('omniture:address:interact', 'edit');

            $('#shipping-select-delivery').trigger('tab.click');
            $addresses = $addressesCont.find('li');
            var $address = $(this).closest('li');
            $addresses.each(function(){
                $(this).removeClass('active');
            });
            $address.addClass('active');
            if($newAddressForm.is(':visible')) {
                hideNewAddressForm();
            }
            $(this).find('i').removeClass('fa-edit').addClass('fa-circle-o-notch fa-spin');
            $addressForm.trigger('address.edit.open');
            $newAddressForm.showNewAddressForm({
                address_id: $address.data('id'),
                countries: ['RU', 'KZ', 'BY'],
                phoneSource: $('#phone'),
                nameSource: $('#name'),
                emailSource: $('#email')
            }, function (res) {
                $addresses.filter('li[data-id="'+res.id+'"]').find('.btn-change-address').find('i');
                var $currentAddress = $addresses.filter('li[data-id="'+res.id+'"]');
                $currentAddress.find('div:not(.wrap)').html(res.html);
                bindAddressEvents();
                $('#shipping-select-delivery').trigger('tab.click');
                setTimeout(function () {
                    $addressForm.trigger('address.created', {
                        target: $currentAddress,
                        id: res.id,
                        edit: true
                    });
                },500);

            });
            return false;
        });
        //Удаление адресов
        $addressesCont.on('click', '.btn-remove-address', function (e) {
            $(document).trigger('omniture:address:interact', 'delete');

            e.preventDefault();
            if (confirm(__('Are you sure you want to delete this address?','addressMessages'))) {
                var $addressDeleteBtn = $(this);
                $addressDeleteBtn.unbind('click').find('i').removeClass('fa-trash').addClass('fa-circle-o-notch fa-spin');
                var $address = $addressDeleteBtn.closest('li');
                $address.unbind('block.item.selected');
                if (aStorage.get('currentAddress') == $address.data('id')) {
                    $('#shipping-select-delivery').trigger('tab.click');
                }
                $addressForm.ajaxRequest('/checkout/onepage/deleteAddress', {
                    id: $address.data('id')
                }, function (res) {
                    $address.remove();
                }, {
                    showSuccessMessage: false,
                    fadeOutMessage: false
                });
            }
            return false;
        });

        //Событие о бинде адресов
        $addressForm.trigger('address.bind', {
            elements: $addresses
        });

        //Ajax-подгрузка данных адреса
        $.fn.loadAddress = function (id, callback) {
            var $this = $(this);
            callback = typeof callback == "function" ? callback : function () {
            };

            $this.ajaxRequest('/checkout/onepage/loadAddress', {
                id: id
            }, function (res) {
                $('body').trigger('loadAddressAfter', res);
                $addressesCont.find('li[data-id="'+id+'"] .btn-change-address i')
                    .removeClass('fa-circle-o-notch fa-spin')
                    .addClass('fa-edit');
                if (typeof res.address !== 'undefined') {
                    callback(res.address);
                }
            }, {
                showSuccessMessage: false,
                fadeOutMessage: false
            });
        };

        function hideNewAddressForm() {
            $newAddressForm.fadeOut(function(){
                $newAddressForm.html('');
            });
        }

        function bindAddressEvents() {
          
        }

        var $addresses = $addressesCont.find('li');
        var $newAddressBtn = $addresses.filter('.add');
        $newAddressBtn.click(function(){
            $addresses = $addressesCont.find('li');
            if($newAddressForm.is(':visible')) {
                hideNewAddressForm();
            } else {
                $addressForm.trigger('address.new.open');
                $newAddressForm.showNewAddressForm({
                    countries: ['RU', 'KZ', 'BY'],
                    phoneSource: $('#phone'),
                    nameSource: $('#name'),
                    emailSource: $('#email')
                }, function (res) {
                    var $createdAddress = $('<li class="address_' + res.id + '"  data-id="' + res.id + '">' +
                        '<b><i class="fa fa-envelope-o"></i> ' + __('Your address', 'addressMessages') + ' ' + $addresses.length + '</b>' +
                        '<div>' + res.html + '</div>' +
                        '<a href="#" class="btn-change-address"><i class="fa fa-edit"></i></a>' +
                        '<a href="#" class="btn-remove-address"><i class="fa fa-trash"></i></a>' +
                        '</li>').hide();
                    $addresses.filter('.add').before($createdAddress);
                    bindFormElements();
                    $createdAddress.fadeIn(function () {
                        $(document).trigger('omniture:address:interact', 'add');

                        $addressForm.trigger('address.created', {
                            target: $createdAddress,
                            id: res.id
                        });
                    });
                });
            }
        });

        $newAddressForm.on('address::map::init', function(){
            try {
                $newAddressForm.attention();
                var $body = $("html, body");
                $body.stop().animate({scrollTop:$newAddressForm.offset().top - 100}, '400', 'swing', function() {
                });
            } catch(e) {}
        });

        if(
            $addresses.length == 1
                && aStorage.get('checkout.delivery.method') != 'pickup'
                && $newAddressBtn.data('empty-addresses-auto-click') !== false
        ) {
            $newAddressBtn.click();
            $newAddressBtn.hide();
        }

    });
})(jQuery, window, document);
