/**
 * Created by p.vasin on 07.03.17.
 */

jQuery('body').on('dragMarkerProcessAddress', function(evt,place) {
    var formattedAddress = place.formatted_address;
    if (typeof formattedAddress !== 'undefined') {
        var lastComma = formattedAddress.lastIndexOf(' ')+1;
        var countryName = formattedAddress.substring(lastComma).trim();
        var countryCode, dialCode = false;
        for (var key in imtCountryInfo) {
            if (imtCountryInfo[key].country.indexOf(countryName) !== -1) {
                dialCode = key;
                countryCode = imtCountryInfo[key].code;
                var selectedFlag = jQuery('.flag-dropdown .selected-flag'),
                    insideSelectedFlag = selectedFlag.find('.iti-flag');
                selectedFlag.removeAttr('title');
                insideSelectedFlag.removeAttr('class');
                selectedFlag.attr('title',countryName + ': ' + dialCode);
                insideSelectedFlag.addClass('iti-flag ' + countryCode);
                jQuery('#address-phone').attr('placeholder','+' + dialCode);
            }
        }
    }
});

jQuery('body').on('loadAddressAfter', function(evt,data) {
    var countryCode = data.address.country_id.toLowerCase();
    var countryName = imtCountryCodeToName[countryCode];
    data.address.country = countryName;
    data.address['address-phone'] = data.address['intl-tel-input'];
});

$.defaultIntlTelSettings = $.extend($.defaultIntlTelSettings,{
    utilsScript: "/js/lib/intl-tel-input/js/utils.js",
    defaultCountry: "fr",
    preferredCountries: ["de", "fr", "it"],
    onlyCountries: [
        "al", "ad", "at", "by", "be", "ba", "bg", "hr", "cz", "dk",
        "ee", "fo", "fi", "fr", "de", "gi", "gr", "va", "hu", "is", "ie", "it", "lv",
        "li", "lt", "lu", "mk", "mt", "md", "mc", "me", "nl", "no", "pl", "pt", "ro",
        "ru", "sm", "rs", "sk", "si", "es", "se", "ch", "ua", "gb"
    ],
    autoPlaceholder: false,
    numberType: 'UNKNOWN'
});

if (typeof $.defaultAddressSettings !== 'undefined') {
    $.defaultAddressSettings.bindForm = $.extend($.defaultAddressSettings.bindForm, {
        country: {
            sort: 5,
            label: __('Your Country', 'addressMessages'),
            options: {
                "class": 'mini',
                required: 'required',
                autocomplete: "off"
            },
            bind: 'country',
            type: 'long_name'
        },
        postcode: {
            sort: 20,
            label: __('Postal Code', 'addressMessages'),
            options: {
                "class": 'mini',
                minlength: 3,
                maxlength: 8
            },
            bind: 'postal_code'
        }
    });
}
