;
(function ($, window, document, undefined) {

    $.defaultAddressSettings = {
        bindForm: {
            address_id: {
                label: 'Address ID',
                bind: 'address_id',
                options: {
                    "class": 'hidden'
                },
                type: 'raw'
            },
            city: {
                sort: 10,
                label: __('Your City', 'addressMessages'),
                options: {
                    "class": 'mini',
                    required: 'required',
                    autocomplete: "off"
                },
                bind: 'locality',
                type: 'long_name'
            },
            postcode: {
                sort: 20,
                label: __('Postal Code', 'addressMessages'),
                options: {
                    "class": 'mini',
                    minlength: 5,
                    maxlength: 6
                },
                bind: 'postal_code'
            },
            street: {
                sort: 30,
                label: __('Street Name', 'addressMessages'),
                options: {
                    "class": '',
                    required: 'required'
                },
                bind: 'route'
            },
            house: {
                sort: 40,
                label: __('House', 'addressMessages'),
                options: {
                    "class": '',
                    required: 'required'
                },
                bind: 'street_number'
            },
            office: {
                sort: 50,
                label: __('Apartments', 'addressMessages'),
                options: {
                    "class": 'mini'
                }
            },
            country_id: {
                label: __('Country Id', 'addressMessages'),
                bind: 'country',
                options: {
                    "class": 'hidden'
                }
            },
            geo_latitude: {
                label: 'Latitude', //Широта
                bind: 'geo.lat',
                options: {
                    "class": 'hidden'
                },
                type: 'raw'
            },
            geo_longitude: {
                label: 'Longitude', //Долгота
                bind: 'geo.lon',
                options: {
                    "class": 'hidden'
                },
                type: 'raw'
            }
        },
        address_id: null,
        placeholder: __('Start typing your location', 'addressMessages'),
        label: __('Please enter Your City, Street and House', 'addressMessages'),
        loadingMessage: __('Map is loading', 'addressMessages'),
        mapId: 'address-map',
        countries: null,  //countries: ['RU', 'KZ', 'BY'],
        height: '350',
        phoneSource: null,
        nameSource: null,
        emailSource: null
    };

    $(document).on('ready pjax:success', function () {

        var markers = [],
            currentMarker,
            map,
            addressInfo,
            searchInput,
            streetInput,
            cityInput,
            houseInput,
            infowindow;

        $.fn.extend({
            showNewAddressForm: function (options, callback) {
                callback = callback || function () {
                    };
                options = $.extend($.defaultAddressSettings, options);

                var $container = $(this),
                    $addressMapCont = $('<div class="address-map-cont"></div>'),
                    $map = $('<div id="' + options.mapId + '" class="address-map"></div>'),
                    $inputRow = $('<div class="c-row"><div class="controls"><div id="map-address-input-cont" class="c-input prep">' +
                        '<span class="addon"><i class="fa fa-map-marker"></i></span> ' +
                        '<input id="input-'+ options.mapId +'" type="text" class="" data-label="' + options.label + '" placeholder="' + options.placeholder + '">' +
                        '</div></div>'),
                    $inputCont = $inputRow.find('.c-input'),
                    $input = $inputCont.find('input'),
                    $addressFormRow = $('<div style="margin-top: 23px;"><form class="controls inline"></form></div>'),
                    $addressFormRowCont = $addressFormRow.find('.controls');

                $addressMapCont.append($map, $inputRow);

                var baseIndex = 30;
                var bindForm = {},
                    delay = {};
                $.each(options.bindForm, function (id, item) {
                    item.id = id;
                    if (item.hasOwnProperty('sort')) {
                        delay[item['sort']] = item;
                    } else {
                        bindForm[id] = item;
                    }
                });
                $.each(delay, function (id, item) {
                    bindForm[item.id] = item;
                });
                $.each(bindForm, function (id, item) {
                    var $cont = $addressFormRowCont.addInput(id, item.label, item.options).css('zIndex', baseIndex--);
                    if(typeof item.onCreate == "function") {
                        item.onCreate($cont);
                    }
                });

                var customerName = $('#customer-name').val();
                var $addressForm = $('.address_form');

                if(empty(customerName)) customerName = $addressForm.data('customer-name');
                $addressFormRowCont.addInput('address-name', __('Contact name','addressMessages'), {
                    value: customerName,
                    fa: 'user'
                });
                var customerPhone = $('#customer-phone').val();
                if(empty(customerPhone)) customerPhone = $addressForm.data('customer-phone');
                $addressFormRowCont.addInput('address-phone', __('Contact phone','addressMessages'), {
                    "class": 'c-phone',
                    type: 'tel',
                    fa: 'phone',
                    value: customerPhone
                });
                $addressFormRowCont.append('<div class="clear">' +
                    '<button type="button" id="btn-add-address" disabled="disabled" class="a-btn a-btn-success prep on">' +
                    '<span class="addon right"><i class="fa fa-save"></i></span> ' + __('Add Address', 'addressMessages') +
                    '</button> ' +
                    '<span id="address-status" class="form-status status"></span>' +
                    '</div>');

                var $btnAddAddress = $addressFormRowCont.find('#btn-add-address');
                var $addressFormInputs = $addressFormRowCont.find('input');
                var $addressStatus = $addressFormRowCont.find('#address-status');

                $addressFormInputs.change(function (e) {
                    var valid = true;
                    $addressFormInputs.each(function () {
                        if (!$(this).valid()) {
                            $(this).focus();
                            valid = false;
                            return false;
                        }
                    });
                    if (valid) {
                        $btnAddAddress.enable();
                    } else {
                        $btnAddAddress.disable();
                    }
                }).keydown(function (e) {
                    /*if (e.keyCode == 13) {
                     e.preventDefault();
                     $btnAddAddress.click();
                     return false;
                     }*/
                });

                if (options.height == "auto") options.height = $container.height();
                $map.css('height', options.height + "px");

                var processAddress = function(place, options) {
                    $('body').trigger('dragMarkerProcessAddress', place);
                    options = $.extend({
                        showErrors: true
                    }, options);
                    if (place.length == 0) {
                        if(options.showErrors) $input.setError(__('Please enter valid address', 'addressMessages'));
                        return;
                    }

                    if (empty(place.address_components)) {
                        if(options.showErrors) $input.setError(__('Please enter valid address', 'addressMessages'));
                        return;
                    }

                    var parsedAddress = {};
                    for (var i = 0; i < place.address_components.length; i++) {
                        parsedAddress[place.address_components[i].types[0]] = place.address_components[i];
                    }
                    if (place.hasOwnProperty('geometry')) {
                        //add geo params
                        if(typeof place.geometry.location.lat == "function") {
                            parsedAddress['geo.lat'] = place.geometry.location.lat();
                            parsedAddress['geo.lon'] = place.geometry.location.lng();
                        } else {
                            parsedAddress['geo.lat'] = place.geometry.location.G ? place.geometry.location.G : place.geometry.location.H;
                            parsedAddress['geo.lon'] = place.geometry.location.K ? place.geometry.location.K : place.geometry.location.L;
                        }
                    }

                    if (!empty(options.countries) && !options.countries.in_array(parsedAddress.country.short_name)) {
                        if(options.showErrors) $input.setError(__('Sorry, no delivery in this country yet!', 'addressMessages'));
                        return;
                    }

                    for (var component in bindForm) {
                        if (bindForm.hasOwnProperty(component)) {
                            if (component == 'address_id') continue;
                            var $item = $container.find('#' + component);
                            if ($item.length) {
                                var config = $.extend({type: 'short_name'}, bindForm[component]);
                                if (config.hasOwnProperty('bind')) {
                                    $item.val(''); //Clean old value
                                    var bind = config['bind'],
                                        type = config['type'];
                                    if (parsedAddress.hasOwnProperty(bind)) {
                                        if (type == "raw") {
                                            $item.fillValue(parsedAddress[bind]);
                                        } else {
                                            $item.fillValue(parsedAddress[bind][type]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
                var addMarker = function (pos, title, options) {
                    options = $.extend({
                        zoom: 19
                    }, options);

                    markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                    markers = [];
                    if (pos) {
                        // Create a marker for target place.
                        var marker = new google.maps.Marker({
                            map: map,
                            //icon: place.icon,
                            label: title,
                            title: title,
                            draggable: true,
                            position: pos
                        });
                        currentMarker = marker;
                        markers.push(marker);

                        google.maps.event.addListener(marker, 'dragend', function (evt) {
                            console.log('Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3));
                            geocodePosition(marker.getPosition());
                            map.setCenter(marker.position);
                            //map.setZoom(options.zoom);
                        });

                        map.setCenter(marker.getPosition());
                        map.setZoom(options.zoom);
                    }
                };
                var geocodePosition = function (pos) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        latLng: pos
                    }, function (responses) {
                        if (responses && responses.length > 0) {
                            currentMarker.label = responses[0].formatted_address;
                            currentMarker.title = responses[0].formatted_address;
                            searchInput.value = responses[0].formatted_address;
                            processAddress(responses[0]);
                            //searchInput.focus();
                            $(searchInput).focus().trigger('change');
                        } else {
                            currentMarker.label = 'Cannot determine address at this location.';
                        }
                    });
                };
                var validateAddress = function (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({address: address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {

                            geocodePosition(results[0].geometry.location);
                        } else {
                            alert(address + ' not found');
                        }
                    });
                };


                var handleNoGeolocation = function (errorFlag) {
                    if (errorFlag) {
                        var content = __('The Geolocation service failed', 'addressMessages');
                    } else {
                        var content = __('Your browser doesnt support geolocation', 'addressMessages');
                    }

                    var options = {
                        map: map,
                        position: new google.maps.LatLng(60, 105),
                        content: content
                    };

                    var infowindow = new google.maps.InfoWindow(options);
                    map.setCenter(options.position);
                };

                //Submit Address Form
                $btnAddAddress.click(function () {
                    if ($btnAddAddress.is(':disabled')) return false;

                    var data = {};
                    $addressFormInputs.each(function () {
                        var $i = $(this);
                        data[$i.attr('name')] = $i.val();
                    });
                    if($addressFormRowCont.find('#address-lastname')) {
                        data.lastname = $addressFormRowCont.find('#address-lastname').val();
                    }
                    if (options.phoneSource && typeof options.phoneSource.val == 'function') {
                        data.telephone = options.phoneSource.val();
                    }
                    if (options.nameSource && typeof options.nameSource.val == 'function') {
                        data.fullname = options.nameSource.val();
                    }
                    if (options.emailSource && typeof options.emailSource.val == 'function') {
                        data.email = options.emailSource.val();
                    }
                    $addressFormRowCont.formPost('/acustomer/address/createAddress', data, function (res) {

                        if(!res.error) {
                            $container.showSmooth('<div class="form-status big" style="padding: 40px 0; text-align: center"><span class="success">' +
                                __('Address successfully created', 'addressMessages') + '</span></div>', function () {
                                callback(res, data);
                            }, {keepHeight: false});

                            setTimeout(function () {
                                $container.fadeOut(function () {
                                    $container.html('');
                                })
                            }, 2500);
                        } else {
                            if(res.code == -100) {
                                var $lastName = $('#address-lastname');
                                if(!$lastName.length) {
                                    var $lastNameRow = $('<div></div>');
                                    $lastNameRow.hide();
                                    $lastName = $lastNameRow.addInput('address-lastname', res.error, { required: 'required' });
                                    $btnAddAddress.parent().before($lastNameRow);
                                    $lastNameRow.fadeIn(function(){
                                        $lastName.focus();
                                    });
                                    if (typeof bindFormElements == "function") bindFormElements();
                                } else {
                                    $lastName.focus();
                                }
                            }
                        }
                    }, {
                        statusObj: $addressStatus,
                        callbackOnError: true
                    });
                });

                var containerFadeOut = function (address) {
                    addressInfo = address;

                    $container.fadeOut(function () {
                        $map.html('<div class="preloading"><span class="ajax">' + options.loadingMessage + '</span></div>');
                        $container.html('').append($addressMapCont, $addressFormRow).fadeIn(function () {
                            if (typeof bindFormElements == "function") bindFormElements();

                            if (!empty(addressInfo)) {
                                for (var component in addressInfo) {
                                    if (addressInfo.hasOwnProperty(component)) {
                                        var $item = $container.find('#' + component);
                                        if ($item.length) {
                                            $item.fillValue(addressInfo[component]);
                                        }
                                    }
                                }
                            }

                            getApi('google-maps', function () {
                                var mapOptions = {
                                    center: {lat: 55.755826, lng: 37.6173},
                                    zoom: 3
                                };
                                searchInput = $input[0];
                                map = new google.maps.Map(document.getElementById(options.mapId), mapOptions);
                                infowindow = new google.maps.InfoWindow();
                                var autocomplete = new google.maps.places.Autocomplete(searchInput, {types: ['geocode']});

                                //Set autocomplete for street input
                                streetInput = document.getElementById('street');
                                var streetComplete = new google.maps.places.Autocomplete(streetInput, {
                                    types: ['address']
                                    //componentRestrictions: {country: 'RU'}
                                });

                                //Set autocomplete for house input
                                houseInput = document.getElementById('house');
                                var houseComplete = new google.maps.places.Autocomplete(houseInput, {
                                    types: ['address']
                                    //componentRestrictions: {country: 'RU'}
                                });

                                var disableSubmitOnEnter = function(e){
                                    var code = e.keyCode || e.which;
                                    if(code == 13) {
                                        e.preventDefault();
                                        return false;
                                    }
                                };

                                google.maps.event.addListener(streetComplete, 'place_changed', function () {
                                    var place = streetComplete.getPlace();
                                    if (place.length == 0) {
                                        return;
                                    }
                                    addMarker(place.geometry.location, place.name);
                                    processAddress(place);
                                    searchInput.value = place.formatted_address;
                                    updateBounds();
                                });

                                google.maps.event.addListener(houseComplete, 'place_changed', function () {
                                    var place = houseComplete.getPlace();
                                    if (place.length == 0) {
                                        return;
                                    }
                                    addMarker(place.geometry.location, place.name);
                                    processAddress(place);
                                    searchInput.value = place.formatted_address;
                                });

                                function updateBounds() {
                                    setTimeout(function(){
                                        streetComplete.setBounds(map.getBounds());
                                        houseComplete.setBounds(streetComplete.getBounds());
                                    }, 200);
                                }

                                cityInput =  document.getElementById('city');
                                var cityComplete =  new google.maps.places.Autocomplete(cityInput, {
                                    types: ['(cities)']
                                });
                                google.maps.event.addListener(cityComplete, 'place_changed', function () {
                                    var place = cityComplete.getPlace();
                                    if (place.length == 0) {
                                        return;
                                    }
                                    if(place.name) {
                                        cityInput.value = place.name;
                                        searchInput.value = place.name;
                                        addMarker(place.geometry.location, place.name);
                                        updateBounds();
                                    }
                                });

                                // Listen for the event fired when the user selects an item from the
                                // pick list. Retrieve the matching places for that item.
                                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                                    $input.clearError();
                                    var place = autocomplete.getPlace();
                                    processAddress(place, { showErrors: true });
                                    addMarker(place.geometry.location, place.name);
                                    updateBounds();
                                });

                                setTimeout(function () {
                                    $inputRow.addClass('animated fadeInDown');
                                    setTimeout(function () {
                                        $inputRow.addClass('loaded').removeClass('animated');
                                        if (navigator.geolocation) {

                                            var $confirmRow = $('<div class="confirm" style="display: none">' +
                                                '<div>' +
                                                '<p>' + __('We can try to detect your proximately location', 'addressMessages') + '</p>' +
                                                '<button type="button" id="auto-geo-yes" class="a-btn a-btn-success">' + __('Yes, please', 'addressMessages') + '</button> ' +
                                                '<button type="button" id="auto-geo-no" class="a-btn a-btn-danger">' + __('No, thanks', 'addressMessages') + '</button> ' +
                                                '</div> ' +
                                                '</div>');
                                            $addressMapCont.prepend($confirmRow);
                                            $confirmRow.fadeIn();
                                            $confirmRow.find('#auto-geo-yes').click(function(){
                                                $confirmRow.fadeOut(function(){
                                                    $confirmRow.remove();
                                                    $input.focus();
                                                });
                                                $map.block({
                                                    message: __('Loading your location', 'addressMessages')
                                                });
                                                navigator.geolocation.getCurrentPosition(function (position) {
                                                    $map.unblock();
                                                    var pos = new google.maps.LatLng(position.coords.latitude,
                                                        position.coords.longitude);
                                                    addMarker(pos, __('Your proximately location', 'addressMessages'), {
                                                        zoom: 16
                                                    });
                                                    geocodePosition(pos);
                                                }, function () {
                                                    $map.unblock();
                                                    handleNoGeolocation(true);
                                                });
                                            });
                                            $confirmRow.find('#auto-geo-no').click(function(){
                                                $confirmRow.fadeOut(function(){
                                                    $confirmRow.remove();
                                                    $input.focus();
                                                });
                                            });
                                            var canceled = false;
                                            $(document).on('keydown', function(e){
                                                if(e.keyCode == 27 && !canceled && $confirmRow.find('#auto-geo-no').length) {
                                                    $confirmRow.find('#auto-geo-no').click();
                                                    canceled = true;
                                                }
                                            });
                                        } else {
                                            $map.unblock();
                                            $input.focus();
                                            //handleNoGeolocation(false);
                                        }
                                    }, 2000);
                                }, 500);

                            });
                        });
                    });

                    bindFormElements($container.find('form'));
                };

                if (!empty(options.address_id)) {
                    $container.loadAddress(options.address_id, containerFadeOut);
                }
                else {
                    containerFadeOut();
                }

                return this;
            }
        });
    });
})(jQuery, window, document);
