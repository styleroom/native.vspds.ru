/**
 * Created by papasha on 19.09.2017.
 */
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoM1pP9bTkecUBIDf-oi7JH31J9xyAGqc&libraries=places">

/**
 * Ceske Budejovice, Czechia
 Latitude: 48.975658 | Longitude: 14.480255
 */

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        //center: {lat: -33.8688, lng: 151.2195},
        center: {lat: 48.975658, lng: 14.480255},
        zoom: 5
    });
    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input'));

    var types = document.getElementById('type-selector');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    // --- MY code ------------------------------------------------------------------------------------------------------------

    /**
     * Не все адреса могут быть взяты подробно
     * @param lat
     * @param lng
     */

    function showlatlong(lat,lng) {
        event.preventDefault && event.preventDefault();
        var o = new google.maps.LatLng(lat,lng);
        map.setCenter(o), marker.setPosition(o); // , map.setZoom(12)
        var n = "(" + o.lat().toFixed(6) + " , " + +o.lng().toFixed(6) + ")";
        codeLatLng(o);
        // document.getElementById(
        //     "latlngspan").innerHTML = n,
        //     document.getElementById("coordinatesurl").value = "http://www.latlong.net/c/?lat=" + o.lat().toFixed(6) + "&long=" + o.lng().toFixed(6),
        //     infowindow.setContent(n),
        // infowindow && infowindow.close(), google.maps.event.addListener(marker, "click", function() {
        //     infowindow.open(map, marker)
        // }), dec2dms(), codeLatLng(o)
    }

    /**
     * results[]: {
 types[]: string,
 formatted_address: string,
 address_components[]: {
   short_name: string,
   long_name: string,
   postcode_localities[]: string,
   types[]: string
 },
 partial_match: boolean,
 place_id: string,
 postcode_localities[]: string,
 geometry: {
   location: LatLng,
   location_type: GeocoderLocationType
   viewport: LatLngBounds,
   bounds: LatLngBounds
 }
}
     * @param e
     */

    /**
     var arrAddress = item.address_components;
     var itemRoute='';
     var itemLocality='';
     var itemCountry='';
     var itemPc='';
     var itemSnumber='';

     // iterate through address_component array
     $.each(arrAddress, function (i, address_component) {
    console.log('address_component:'+i);

    if (address_component.types[0] == "route"){
        console.log(i+": route:"+address_component.long_name);
        itemRoute = address_component.long_name;
    }

    if (address_component.types[0] == "locality"){
        console.log("town:"+address_component.long_name);
        itemLocality = address_component.long_name;
    }

    if (address_component.types[0] == "country"){
        console.log("country:"+address_component.long_name);
        itemCountry = address_component.long_name;
    }

    if (address_component.types[0] == "postal_code_prefix"){
        console.log("pc:"+address_component.long_name);
        itemPc = address_component.long_name;
    }

    if (address_component.types[0] == "street_number"){
        console.log("street_number:"+address_component.long_name);
        itemSnumber = address_component.long_name;
    }
    //return false; // break the loop
});
     * @type {{locality: string, postal_code: string, country: string, place_id: string}}
     */

    var toForm = {
        locality:'',
        postal_code:'',
        country:'',
        place_id:''
    };

    function codeLatLng(e) {
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({
            latLng: e
        }, function(o, n) {
            n === google.maps.GeocoderStatus.OK ? o[1] ? (marker.setPosition(e),
                infowindow.setContent(o[1].formatted_address),
                infowindow.open(map, marker), document.getElementById("address").innerHTML = o[1].formatted_address) : alert("No results found") : alert("Geocoder failed due to: " + n);
            console.log('o',o);
            console.log('o[1]',o[1]);
            console.log('o[1].formatted_address',o[1].formatted_address);
            console.log('o[1].address_components',o[1].address_components);
        });
    }

    var markers = [];

    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }
    function clearMarkers() {
        setMapOnAll(null);
    }
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    // get coordinates via double click
    google.maps.event.addListener(map, "dblclick", function(event) {
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();
        // populate yor box/field with lat, lng
        console.log("Lat=" + lat + "; Lng=" + lng);

        var myLatlng = {lat: lat, lng: lng};

        deleteMarkers();

        // add new marker
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Click to zoom'
        });
        markers.push(marker);
        showlatlong(lat,lng);
    });

    // --- MY code ------------------------------------------------------------------------------------------------------------

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    });

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
        });
    }

    // setupClickListener('changetype-all', []);
    // setupClickListener('changetype-address', ['address']);
    // setupClickListener('changetype-establishment', ['establishment']);
    // setupClickListener('changetype-geocode', ['geocode']);

    // --------------------------------------------

    // function codeLatLng(origin) {
    //     var lat = parseFloat(document.getElementById("latitude").value) || 0;
    //     var lng = parseFloat(document.getElementById("longitude").value) || 0;
    //     document.getElementById("latlong").value = lat + "," + lng;
    //     var latlng = new google.maps.LatLng(lat,lng);
    //     if (origin == 1)
    //         ddversdms();
    //     geocoder.geocode({
    //         latLng: latlng
    //     }, function(results, status) {
    //         if (status == google.maps.GeocoderStatus.OK) {
    //             if (results[0]) {
    //                 if (marker != null)
    //                     marker.setMap(null);
    //                 marker = new google.maps.Marker({
    //                     position: latlng,
    //                     map: map
    //                 });
    //                 infowindow.setContent(infowindowContent(results[0].formatted_address, lat, lng));
    //                 infowindow.open(map, marker);
    //                 document.getElementById("address").value = results[0].formatted_address;
    //                 bookUp(document.getElementById("address").value, lat, lng)
    //             }
    //         } else {
    //             if (marker != null)
    //                 marker.setMap(null);
    //             marker = new google.maps.Marker({
    //                 position: latlng,
    //                 map: map
    //             });
    //             infowindow.setContent(infowindowContent(trans.NoResolvedAddress, lat, lng));
    //             infowindow.open(map, marker);
    //             document.getElementById("address").value = trans.NoResolvedAddress;
    //             bookUp(document.getElementById("address").value, lat, lng);
    //             alert(trans.GeocodingError + status)
    //         }
    //     });
    //     map.setCenter(latlng);
    //     fromPlace = 0
    // }

}