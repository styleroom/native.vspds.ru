<!DOCTYPE html>
<html>
<head>
    <title>Place Autocomplete</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/test05.css">
</head>
<body>
<input id="pac-input" class="controls" type="text"
       placeholder="Enter a location">
<div id="type-selector" class="controls">
    <input type="radio" name="type" id="changetype-all" checked="checked">
    <label for="changetype-all">All</label>

    <input type="radio" name="type" id="changetype-establishment">
    <label for="changetype-establishment">Establishments</label>

    <input type="radio" name="type" id="changetype-address">
    <label for="changetype-address">Addresses</label>

    <input type="radio" name="type" id="changetype-geocode">
    <label for="changetype-geocode">Geocodes</label>
</div>
<div id="map"></div>

<script src="js/test05.js" async defer></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoM1pP9bTkecUBIDf-oi7JH31J9xyAGqc&libraries=places&callback=initMap"
        async defer></script>
</body>
</html>