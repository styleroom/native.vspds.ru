/**
 * Created by p.vasin on 22.08.17.
 */
declare let globalVar;
class Test01 {
    public titleFromClass = 'Заголовок из JS';
    public setTitle(selector: string, titleNew: string) {
        let title = document.querySelector(selector);
        title.innerHTML = titleNew;
    }
    public static alert(string: string = 'default hello!', string2?: string): void {
        let res: string = string;
        if (string2) {
            res = string + ' ' + string2;
        }
        alert(res);
    }
    public iterateList(from: string, to: string): void {
        let items = document.querySelectorAll(from);
        let place = document.querySelector(to);
        let item: string;
        let paragraf: any;
        for (var i=0;i<items.length;i++) {
            item = items.item(i).textContent.trim();
            paragraf = document.createElement('p');
            paragraf.innerHTML = item;
            place.appendChild(paragraf);
        }
    }
}
//Test01.alert('hello world!','это второй аргумент функции!');
let test01: Test01 = new Test01();
test01.setTitle('.wow', 'Новый заголовок');
test01.iterateList('.list li','#listItems');