/**
 * Created by p.vasin on 21.08.17.
 */

// --- boolean ---
let isEnabled = true;
let isAlive: boolean = false;


// --- number ---
let decimal: number = 6;
let floatNumber: number = 123.456;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;


// --- string ---
let firstName: string = "Tom";
let lastName = 'Johns';
let age: number = 28;
////// шаблон строки
let info: string = `Имя ${firstName}    Возраст: ${age}`;


// --- array ---
let list: number[] = [10, 20, 30];
let colors: string[] = ["red", "green", "blue"];
let names: Array<string> = ["Tom", "Bob", "Alice"];


// --- tuple - кортеж ---
// определение кортежа - кортеж состоит из двух элементов - строки и числа
let userInfo: [string, number];
// инициализация кортежа
userInfo = ["Tom", 28];
// использование кортежа
console.log(userInfo[1]); // 28


// --- enum ---
enum Season { Winter, Spring, Summer, Autumn };
let current: Season = Season.Summer;
current = Season.Autumn; // изменение значения
/*
 enum Season { Winter=0, Spring=1, Summer=2, Autumn=3 };
 может сами определить значения
 enum Season { Winter=0, Spring=1, Summer=2, Autumn=3 };
 получить текстовое значение = var current: string = Season[2];
 */


// --- any ---
let someVar: any = "hello";
someVar = 20;
var someArray: any[] = [ 24, "Tom", false];


// --- null + undefined ---
let a: undefined = undefined;
let b: null = null;


// --- object ---
let person = {name:"Tom", age:23};


// --- unions --- объединения ---
let namesUnion : string[] | string | number;


// --- type --- псевдоним типа ---
type stringOrNumberType = number | string;


// --- type assertion ---
// преобразование значения переменной к определенному типу
let someAnyValue: any = "hello world!";
let strLength: number = (<string>someAnyValue).length;
let someUnionValue: string | number = "hello work";
strLength = (<string>someUnionValue).length;
let someAnyValue2: any = "hello world!";
let strLength2: number = (someAnyValue2 as string).length;
let someUnionValue2: string | number = "hello work";
strLength = (someUnionValue2 as string).length;


// --- functions ---
// В typescript при вызове в функцию должно передаваться ровно столько значений, сколько в ней определено параметров
// определение функции
function add(a: number, b: number): number {
    return a + b;
}
// вызов функции
let result1 = add(1, 2);
/*
 Чтобы иметь возможность передавать различное число значений в функцию, в TS некоторые параметры можно объявить как необязательные.
 Необязательные параметры должны быть помечены вопросительным знаком ?.
 Причем необязательные параметры должны идти после обязательных
 function getName(firstName: string, lastName?: string)
 */
/*
 Особым типом необязательных параметров являются параметры по умолчанию.
 function getName(firstName: string, lastName: string="Иванов")
 */
/*
 Но выше речь шла только о единичных необязательных параметрах. 
 Если же необходимо, чтобы функция принимала набор однотипных параметров, то используется знак многоточия, после которого идет массив
 function addNumbers(firstNumber: number, ...numberArray: number[]): number
 */


// --- Перегрузка функций ---
/*
 TypeScript поддерживает возможность перегрузки функций, то есть мы можем определить несколько версий функции,
 которые будут иметь одно и то же имя, но разные типы параметров или разное количество параметров или разные возвращаемые типы результатов.
 Для перегрузки вначале опеределяем все версии функции, которые не будут иметь никакой логики.
 А потом определяем версию функции с общей сигнатурой, которая подходит под все ранее определенные варианты.
 И в этой общей версии уже определяем конкретную логику функции.
 */
function addA(x: string, y: string): string;
function addA(x: number, y: number): number;
function addA(x: any, y: any): any {
    return x + y;
}

var result1A = addA(5, 4);
console.log(result1A);   // 9
var result2A = addA("5", "4");
console.log(result2A);   // 54


// --- определить переменную как функцию некоторого типа ---
let operation: (x: number, y: number) => number;
/*
 Однако на момент определения переменной неизвестно, какую именно функцию она будет представлять.
 И далее происходит собственно установка функции:
 */
operation = function (x: number, y: number): number {
    return x * y;
};


// --- лямбда ---
/*
 Лямбда-выражения представляют выражения типа (параметры) => тело функции.
 */
let sumq = (x: number, y: number) => x + y;
let resultq = sumq(15, 35); // 50
let sumw = (x: number, y: number) => {
    x *= 2;
    return x + y;
};

let resultw = sumw(15, 35);