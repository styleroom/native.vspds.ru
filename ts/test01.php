<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Test 01</title>
</head>
<body>

<h1 class="wow">Document.querySelector()</h1>

<p>Возвращает первый элемент внутри документа (используется предупорядоченный обход узлов в глубину <a href="http://shop.philips.ru">до первого найденного узла</a>), который совпадает с определенной группой селекторов.</p>

<h2>Примечания</h2>

<ol class="list">
    <li>Возвращается <code>null</code>, если нет найденых совпадений; в ином случае, нам вернется первый найденный элемент. </li>
    <li>Если селектор найден по ID и этот ID ошибочно используется несколько раз в документе, нам вернется первый совпавший элемент.</li>
    <li>Если селекторы не корректны, выбрасывается исключение <code>SYNTAX_ERR.</code></li>
    <li><code>querySelector()</code> был представлен в Selectors API.</li>
    <li>Строка передаваемых аргументов в <code>querySelector</code> должна соответствовать синтаксису CSS.</li>
    <li>CSS псевдо-классы никогда не вернут каких-либо элементов, как и определено в <a class="external external-icon" href="http://www.w3.org/TR/selectors-api/#grammar" style="color: rgb(56, 120, 148); margin: 0px; padding: 0px; border: 0px; text-decoration-line: none; white-space: pre-line;">Selectors API</a></li>
</ol>

<p>Для совпавшего ID или селектора, который не соответствует CSS синтаксу (к примеру, используя запятую или пробельный символ ненадёжным образом), вы должны экранировать символ обратной косой чертой (<code>\</code>). Так как обратная косая черта &mdash; это экранирующий символ в JavaScript, её нужно экранировать <em>дважды</em> (т.е. первый раз для экранирования строки JavaScript, второй раз для querySelector).</p>

<p><img alt="Описание картинки в теге alt" src="https://www.shop.philips.ru/media/up/evo/home-block-1.jpg" style="height:420px; width:612px" /></p>

<hr>
<div id="listItems"></div>
<hr>

<button id="alertButton">Жми</button>

<link rel="stylesheet" href="style.css" type="text/css" />
<script src="test01.js"></script>

</body>
</html>