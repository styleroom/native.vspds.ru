declare let aStorage: any; // глобальная переменная
declare let ajaxRequest: any; // глобальная переменная
declare let tingle: any; // глобальная переменная
declare let jq: any; // глобальная переменная

class SubscribeSecretSale {
    private container;
    private instance;

    constructor(selector: string) {
        this.container = document.querySelector(selector);
    }

    private _setContent(html: string) {
        this.instance.setContent(html);
    }

    private _onClose() {
        aStorage.set('isShowSubscribeSecretSale', false);
        this.instance.destroy();
    }

    private _onOpen() {
        let form = document.querySelector('#subscribe-secret-sale-form');
        if(form) {
            document.querySelector('.subscribe-finish__close').addEventListener('click', (e) : any => {
                this._onClose();
                e.preventDefault();
            });

            form.addEventListener('submit', (e): any => {
                ajaxRequest('/subscriber/subscribe/create/', jq(form).serializeArray(), (): any => {
                    jq(form).hide();
                    jq(form).next('.subscribe-finish').fadeIn('slow');

                    setTimeout(() : any => {
                        this._onClose();
                    }, 5000);
                }, {
                    'dataType': 'html',
                    'waitMessage': 'Оформляем подписку...',
                    'fadeOutMessage': false,
                    'errorMessage': 'Ошибка. Попробуйте позже.',
                    'statusObj': jq(form).find('.subscribe-form__status'),
                    'buttonObj': jq(form).find('.subscribe-form__button')
                });

                e.preventDefault();
            });
        }
    }

    public show(timeout: number) {
        this.instance = new tingle.modal({
            footer: false,
            stickyFooter: false,
            closeLabel: "Закрыть",
            cssClass: ['subscribe-secret-sale'],
            onOpen: (): any => { this._onOpen() },
            onClose: (): any => { this._onClose() }
        });

        this._setContent(this.container.innerHTML);
        setTimeout(() : any => {
            this.instance.open();
        }, timeout);
        this.container.remove();
    }
}