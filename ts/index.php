<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>TypeScript HTML App</title>
    <link rel="stylesheet" href="style.css" type="text/css" />
    <script src="/Scripts/jquery-3.2.1.min.js"></script>
    <script src="script.js"></script>
</head>
<body>
<h1>TypeScript HTML App</h1>

<div id="content"></div>
<button id="alertButton">Жми</button>
</body>
</html>