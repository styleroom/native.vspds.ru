<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 10:58
 */

//include 'AbstractFactory.php';
include_once 'JsonText.php';

class JsonFactory extends AbstractFactory
{
    public function createText($content)
    {
        return new JsonText($content);
    }
}