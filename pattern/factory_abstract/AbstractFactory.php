<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 10:58
 */

/**
 * Class AbstractFactory
 * Абстрактный класс нужен, когда нужно семейство классов.
 * При этом нужно реализовать некий метод, который должен быть у всех классов, но во всех он должен быть реализован по-своему.
 * http://www.quizful.net/interview/php/abstract-class-interface-difference
 */

abstract class AbstractFactory
{
    abstract public function createText($content);
}