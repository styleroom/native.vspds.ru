<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 16:08
 */

class Entity
{
    /**
     * @var \SplObjectStorage
     */
    private $values;
    /**
     * @var string
     */
    private $name;
    /**
     * @param string $name
     * @param Value[] $values
     */
    public function __construct($name, $values)
    {
        $this->values = new \SplObjectStorage();
        $this->name = $name;
        foreach ($values as $value) {
            $this->values->attach($value);
        }
    }
    public function __toString()
    {
        $text = [$this->name];
        foreach ($this->values as $value) {
            $text[] = (string) $value;
        }
        return join(', ', $text);
    }
}