<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 16:08
 */
class Value
{
    /**
     * @var Attribute
     */
    private $attribute;
    /**
     * @var string
     */
    private $name;
    public function __construct(Attribute $attribute, $name)
    {
        $this->name = $name;
        $this->attribute = $attribute;
        $attribute->addValue($this);
    }
    public function __toString()
    {
        return sprintf('%s: %s', $this->attribute, $this->name);
    }
}