<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 16:07
 */

class Attribute
{
    /**
     * @var \SplObjectStorage
     */
    private $values;
    /**
     * @var string
     */
    private $name;
    public function __construct($name)
    {
        $this->values = new \SplObjectStorage();
        $this->name = $name;
    }
    public function addValue(Value $value)
    {
        $this->values->attach($value);
    }
    /**
     * @return \SplObjectStorage
     */
    public function getValues()
    {
        return $this->values;
    }
    public function __toString()
    {
        return $this->name;
    }
}