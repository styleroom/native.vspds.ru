<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 14:51
 */
class Observer implements \SplObserver
{
    private $changedUsers = [];

    public function update(\SplSubject $subject)
    {
        $this->changedUsers[] = clone $subject;
        echo 'updated!<br>';
    }

    public function getChangedUsers()
    {
        return $this->changedUsers;
    }
}