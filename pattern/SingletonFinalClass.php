<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 13:48
 */

final class Singleton
{
    private static $instance;

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}

    public function getDateTime()
    {
        return date('c');
    }
}