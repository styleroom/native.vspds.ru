<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.07.17
 * Time: 12:29
 */

ini_set('display_errors', '1');

interface IObserver
{
    function onChanged( $sender, $args );
}
interface IObservable
{
    function addObserver( $observer );
}
class Comments implements IObservable
{
    private $_observers = array();
    public function addComment( $text )
    {
        foreach( $this->_observers as $obs )
            $obs->onChanged( $this, $text );
    }
    public function addObserver( $observer )
    {
        $this->_observers[]= $observer;
    }
}
class EMail implements IObserver
{
    /*

    ... прочие методы и свойства, где здесь метод отправки письма send ...

    */
    public function send($message)
    {
        echo "\n\n<hr>\n\n {$message} \n\n<hr>\n\n";
    }

    public function onChanged( $sender, $args )
    {
        $this->send( "Текст комментария: '$args'\n" );
    }
}
$comments = new Comments();                         // объект изменяющий свое состояние
$comments->addObserver( new EMail() );              // добавили наблюдателя
$comments->addComment( "It's my comment." );    // изменили состояние