<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 15:55
 */

/**
 * 1. класс обсервер выполняет функцию сохранения в себе объектов-клонов SplSubject
 */

class Observer implements \SplObserver
{
    private $subjectStorage = [];                           // массив-хранилище клонов-объеков SplSubject

    public function update(SplSubject $subject)             // добавление в массив клона объекта SplSubject
    {
        $this->subjectStorage[] = clone $subject;
    }

    public function getSubjectStorage()                     // получение массива-хранилища объектов
    {
        return $this->subjectStorage;
    }
}

class User implements \SplSubject
{
    private $email;
    private $observerStorage;                                     // переменная-хранилище для объекта SplObjectStorage

    public function __construct()                           // запись объекта SplObjectStorage в переменную-хранилище
    {
        $this->observerStorage = new \SplObjectStorage();
    }

    public function attach(SplObserver $observer)           // добавление в хранилище объекта SplObserver
    {
        $this->observerStorage->attach($observer);
    }

    public function detach(SplObserver $observer)           // удаление из хранилища объекта SplObserver
    {
        $this->observerStorage->detach($observer);
    }

    public function notify()                                // обновление всех обсерверов из хранилища
    {
        foreach ($this->observerStorage as $observer) {
            $observer->update($this); # посылаем оповещение
        }
    }

    public function changeEmail($email)                     // метод изменяющий что-то = в нем вызов notify
    {
        $this->email = $email;
        $this->notify(); # метод-оповещатель
    }
}

$observer = new Observer();                                 // объект Observer
$user = new User();                                         // объект Subject
$user->attach($observer);                                   // добавляем в объект Subject объект Observer
$user->changeEmail('foo1@bar.com');                         // изменяем что-то в Subject
$user->changeEmail('foo2@bar.com');

/**
 * Механизм класса, который позволяет получать экземпляру объекта этого класса
 * оповещения от других объектов об изменении их состояния, тем самым наблюдая за ними.
 * Определяет зависимость типа «один ко многим» между объектами таким образом,
 * что при изменении состояния одного объекта все зависящие от него (обсерверы) оповещаются об этом событии.
 */

/**
 * обсервер может быть фабрикой
 * которая в зависимости от сути изменения
 * вызывает тот или иной функционал обработки события
 */