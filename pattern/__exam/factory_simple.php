<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 14.07.17
 * Time: 14:12
 */

ini_set('display_errors', '1');

class Bicycle
{
    public function driveTo($country)
    {
        return "We are want to {$country}";
    }
}

class FactorySimple
{
    public function createBicycle()
    {
        return new Bicycle();
    }
}

$factory = new FactorySimple();
$bicycle = $factory->createBicycle();
echo $bicycle->driveTo('Russua');

