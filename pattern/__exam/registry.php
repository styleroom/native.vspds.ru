<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 14.07.17
 * Time: 15:17
 */

ini_set('display_errors', '1');

class Registry
{
    protected static $storage = [];

    public static function set($key,$item)
    {
        if (!array_key_exists($key, self::$storage)) {
            self::$storage[$key] = $item;
        }
    }

    public static function get($key)
    {
        if (array_key_exists($key, self::$storage)) {
            return self::$storage[$key];
        }
        return false;
    }

    public static function remove($key)
    {
        if (array_key_exists($key, self::$storage)) {
            unset(self::$storage[$key]);
        }
    }
}

Registry::set('key1','value1');
Registry::set('key2',['data'=>'value2']);

$key1 = Registry::get('key1');
echo "\n\n<pre>\n{$key1}\n</pre>\n\n";

$key2 = Registry::get('key2');
echo "\n<pre>\n";
print_r($key2);
echo "\n</pre>\n";