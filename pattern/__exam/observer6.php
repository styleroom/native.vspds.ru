<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 14.07.17
 * Time: 9:00
 */

ini_set('display_errors', '1');

/**
 * 1. Объект фабрики
 * 2. Объект сущности
 * 3. Вызов метода из объекта сущности
 */


class Handler
{
    public function event_01($data = [])
    {
        echo "<pre>";
        print_r($data);
        echo "</pre><hr>";
    }
    public function event_02($data = [])
    {
        echo "<pre>";
        print_r($data);
        echo "</pre><hr>";
    }
}

class Observer extends Handler implements SplObserver
{
    private $storage = [];
    public function getStorage()
    {
        return $this->storage;
    }
    public function update(SplSubject $subject)
    {
        $method = $subject->eventName;
        $this->$method($subject->eventData);
        $this->storage[] = clone $subject;
    }
}

class Subject implements SplSubject
{
    private $storage;
    public $eventName;
    public $eventData = [];
    public function __construct()
    {
        $this->storage = new SplObjectStorage();
    }
    public function attach(SplObserver $observer)
    {
        $this->storage->attach($observer);
    }
    public function detach(SplObserver $observer)
    {
        $this->storage->detach($observer);
    }
    public function notify()
    {
        foreach ($this->storage as $observer) {
            $observer->update($this);
        }
    }
    public function dispatchEvent($eventName, $eventData = [])
    {
        $this->eventName = $eventName;
        $this->eventData = $eventData;
        $this->notify();
    }
}

$observer = new Observer();
$subject = new Subject();
$subject->attach($observer);
$subject->dispatchEvent('event_01', ['key_01'=>'value_01']);
$subject->dispatchEvent('event_02', ['key_02'=>'value_02']);
var_dump($observer->getStorage());