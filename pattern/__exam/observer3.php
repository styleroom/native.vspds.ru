<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.07.17
 * Time: 10:40
 *
 * Наблюдатель оповещает другие части системы об изменениях в наблюдаемых объектах
 * чтобы это работало - к наблюдаемому нужно присоединить наблюдатель Observer -> Subject
 * Наблюдаемый объект, subject, должен предоставлять интерфейс для регистрации и дерегистрации наблюдателей.
 */

/**
 * В PHP осуществляется встроенная поддержка этого шаблона через входящее в поставку
 * расширение SPL (Standard PHP Library):
 * SplObserver - интерфейс для Observer (наблюдателя),
 * SplSubject - интерфейс Observable (наблюдаемого),
 * SplObjectStorage - вспомогательный класс (обеспечивает улучшенное сохранение и удаление
 * объектов, в частности, реализованы методы attach() и detach()).
 */

/**
 * внутри наблюдаемого вызывается update из наблюдателя
 */

ini_set('display_errors', '1');

class Observable implements SplSubject
{
    private $storage;
    function __construct()
    {
        $this->storage = new SplObjectStorage();
    }

    function attach(SplObserver $observer)
    {
        $this->storage->attach($observer);
    }

    function detach(SplObserver $observer)
    {
        $this->storage->detach($observer);
    }

    function notify()
    {
        foreach($this->storage as $obj)
        {
            $obj->update($this);
        }
    }
    //...
}

abstract class Observer implements SplObserver
{
    private $observable;

    function __construct(Observable $observable)
    {
        $this->observable = $observable;
        $observable->attach($this);
    }

    function update(SplSubject $subject)
    {
        if($subject === $this->observable)
        {
            $this->doUpdate($subject);
        }
    }

    abstract function doUpdate(Observable $observable);
}

class ConcreteObserver extends Observer
{
    function doUpdate(Observable $observable)
    {
        //echo 'Update Observable $observable here!';
    }
}

$observable = new Observable();                     // объект наблюдаемого
$concrete = new ConcreteObserver($observable);      // объект обсервера - странно
$concrete->doUpdate($observable);                   // изменили состояние

/**
 * Механизм класса, который позволяет получать экземпляру объекта этого класса
 * оповещения от других объектов об изменении их состояния, тем самым наблюдая за ними.
 * Определяет зависимость типа «один ко многим» между объектами таким образом,
 * что при изменении состояния одного объекта все зависящие от него (обсерверы) оповещаются об этом событии.
 *
 * обсервер узнает об изменениях в наблюдаемых классах
 */

/**
$observer = new Observer();                                 // объект Observer
$user = new User();                                         // объект Subject
$user->attach($observer);                                   // добавляем в объект Subject объект Observer
$user->changeEmail('foo1@bar.com');                         // изменяем что-то в Subject
$user->changeEmail('foo2@bar.com');                         // изменяем что-то в Subject
var_dump($observer->getChangedUsers());                     // проверяем наличие наблюдаемых объектов внутри наблюдателя Observer
*/
