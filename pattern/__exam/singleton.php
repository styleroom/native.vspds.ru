<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 14:40
 */

class MySingle
{
    private static $instance;
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }
    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}

    public function getDate()
    {
        return date('c');
    }
}

$qwe = MySingle::getInstance();
echo $qwe->getDate();