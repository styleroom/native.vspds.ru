<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 14.07.17
 * Time: 14:20
 */

ini_set('display_errors', '1');

class FormatString
{
    public function init()
    {
        $cl = __CLASS__;
        echo "\n{$cl}<br>\n";
    }
}

class FormatInteger
{
    public function init()
    {
        $cl = __CLASS__;
        echo "\n{$cl}<br>\n";
    }
}

class FactoryStatic
{
    public static function build($name)
    {
        $method = false;
        try {
            switch ($name) {
                case 'string':
                    $method = new FormatString();
                    break;
                case 'integer':
                    $method = new FormatInteger();
                    break;
            }
        } catch (Exception $ex) {
            var_dump($ex);
        }
        return $method;
    }
}

$fact1 = FactoryStatic::build('string');
$fact2 = FactoryStatic::build('integer');
$fact3 = FactoryStatic::build('object');
$fact1->init();
$fact2->init();
$fact3->init();