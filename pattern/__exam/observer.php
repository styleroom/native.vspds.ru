<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 15:08
 */

class Observer implements \SplObserver
{
    private $changedUsers = [];

    public function update(SplSubject $subject)
    {
        /**
         * пишем в массив склонированные объекты класса SplSubject
         */
        $this->changedUsers[] = clone $subject;
    }

    public function getChangedUsers()
    {
        return $this->changedUsers;
    }
}

class User implements \SplSubject
{
    private $email;
    private $observers;

    public function __construct()
    {
        /**
         * пишем в свойство объект класса SplObjectStorage
         */
        $this->observers = new \SplObjectStorage();
    }

    public function attach(SplObserver $observer)
    {
        /**
         * добавляем обсервер в массив
         */
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        /**
         * удаляем обсервер из массива
         */
        $this->observers->detach($observer);
    }

    public function notify()
    {
        /**
         * перебираем массив и обновляем каждый обсервер
         */
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function changeEmail($email)
    {
        /**
         * при изменении адреса
         * обновляем все обсерверы
         */
        $this->email = $email;
        $this->notify();
    }
}

$observer = new Observer();
$user = new User();
$user->attach($observer);
$user->changeEmail('foo1@bar.com');
$user->changeEmail('foo2@bar.com');
var_dump($observer->getChangedUsers());