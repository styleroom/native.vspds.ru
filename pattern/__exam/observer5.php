<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 13.07.17
 * Time: 12:35
 */

class Observer implements SplObserver
{
    private $storage = [];
    public function update(SplSubject $subject)
    {
        $this->storage[] = clone $subject;
    }
    public function getStorage()
    {
        return $this->storage;
    }
    public function printStorage()
    {
        echo "\n\n<br>\n<pre>\n";
        print_r($this->getStorage());
        echo "\n</pre>\n<br>\n\n";
    }
}

class Comments implements SplSubject
{
    private $storage;
    private $comment;
    private $eventName;
    private $eventData;
    public function __construct()
    {
        $this->storage = new SplObjectStorage();
    }

    public function attach(SplObserver $observer)
    {
        $this->storage->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        $this->storage->detach($observer);
    }

    public function notify()
    {
        foreach ($this->storage as $item) {
            $item->update($this);
        }
    }

    public function addComment($comment)
    {
        echo "Комментарий: {$comment}";
        $this->comment = $comment;
        $this->notify();
    }

    public function dispatchEvent($eventName, $eventData = [])
    {
        $this->eventData = $eventData;
        $this->eventName = $eventName;
        $this->notify();
    }
}


$comments = new Comments();                         // SplSubject
$observer = new Observer();                         // SplObsrver
$comments->attach($observer);                       // добавили наблюдателя
$comments->addComment( "It's my comment." );    // изменили состояние
$comments->dispatchEvent('my_new_event', ['collection'=>[111,222,333]]);
echo "\n\n<hr>\n\n";
$observer->printStorage();