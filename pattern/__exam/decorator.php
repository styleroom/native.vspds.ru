<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 14.07.17
 * Time: 15:47
 */

ini_set('display_errors', '1');

interface RenderableInterface
{
    public function renderData();
}

class Webservice implements RenderableInterface
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function renderData()
    {
        return $this->data;
    }
}

abstract class Decorator implements RenderableInterface
{
    protected $wrapped;

    public function __construct(RenderableInterface $renderer)
    {
        $this->wrapped = $renderer;
    }
}

class XmlRenderer extends Decorator
{
    public function renderData()
    {
        $doc = new \DOMDocument();
        $data = $this->wrapped->renderData();
        $doc->appendChild($doc->createElement('content', $data));

        return $doc->saveXML();
    }
}

class JsonRenderer extends Decorator
{
    public function renderData()
    {
        return json_encode($this->wrapped->renderData());
    }
}

$service = new Webservice('foobar');
$service1 = new JsonRenderer($service);
$service2 = new XmlRenderer($service);