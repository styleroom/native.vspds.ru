<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 10:36
 */

/**
 * класс с функциональными членами
 * Class Bicycle
 */
class Bicycle
{
    public function driveTo($destination)
    {
        return "Hurray! We are drive to $destination";
    }
}