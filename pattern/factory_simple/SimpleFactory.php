<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 10:35
 */

/**
 * класс с методами возвращающими объект другого класса
 * Class SimpleFactory
 */
class SimpleFactory
{
    public function createBicycle()
    {
        return new Bicycle();
    }
}