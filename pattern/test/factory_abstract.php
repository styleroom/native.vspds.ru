<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 16:09
 */

/**
 * Фабричный метод - порождающий шаблон проектирования, предоставляющий подклассам интерфейс для создания экземпляров некоторого класса.
 * В момент создания наследники могут определить, какой класс создавать.
 * Иными словами, данный шаблон делегирует создание объектов наследникам родительского класса.
 * Это позволяет использовать в коде программы не специфические классы, а манипулировать абстрактными объектами на более высоком уровне.
 */

ini_set('display_errors', '1');
include_once "../factory_abstract/HtmlFactory.php";
include_once "../factory_abstract/JsonFactory.php";

$factoryHtml = new HtmlFactory();
$textHtml = $factoryHtml->createText('html factory foobar');
var_dump($textHtml);
echo "\n<br>".$textHtml->getText();

echo "\n\n<hr>\n\n";

$factoryJson = new JsonFactory();
$textJson = $factoryJson->createText('json factory foobar');
var_dump($textJson);
echo "\n<br>".$textJson->getText();
