<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 13:13
 */

ini_set('display_errors', '1');

include('../SingletonNew.php');

$Object = Singleton::getInstance();  // Получение объекта
//Вывод будет одинаковым, так как существует только один экземпляр
$Object -> test();
Singleton::getInstance() -> test();

$Object2 = Singleton::getInstance();
$Object2->test();
//// Попытка создать дополнительный экземпляр приведет к ошибке
//$Object2 = new Singleton(); // Fatal error: Call to private Singleton::__construct() from invalid context
//$Object3 = clone $Object; // Fatal error: Call to private Singleton::__clone() from context ''