<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 14:52
 */
ini_set('display_errors', '1');
include '../observer/User.php';
include '../observer/Observer.php';

$observer = new Observer();
$user = new User();
$user->attach($observer);
$user->changeEmail('foo1@bar.com');
$user->changeEmail('foo2@bar.com');
var_dump($observer->getChangedUsers());