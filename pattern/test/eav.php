<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 05.07.17
 * Time: 16:09
 */
ini_set('display_errors', '1');
include "../eav/Entity.php";
include "../eav/Attribute.php";
include "../eav/Value.php";

$colorAttribute = new Attribute('color');
$colorSilver = new Value($colorAttribute, 'silver');
$colorBlack = new Value($colorAttribute, 'black');

$memoryAttribute = new Attribute('memory');
$memory8Gb = new Value($memoryAttribute, '8GB');
$entity = new Entity('MacBook Pro', [$colorSilver, $colorBlack, $memory8Gb]);

//$this->assertEquals('MacBook Pro, color: silver, color: black, memory: 8GB', (string) $entity);

echo $entity;