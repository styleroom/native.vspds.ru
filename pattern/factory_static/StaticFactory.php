<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 14:15
 */

/**
 * Note1: Remember, static means global state which is evil because it can't be mocked for tests
 * Note2: Cannot be subclassed or mock-upped or have multiple different instances.
 */

include_once 'FormatNumber.php';
include_once 'FormatString.php';

final class StaticFactory
{
    /**
     * @param string $type
     *
     * @return FormatterInterface
     */
    public static function factory($type)
    {
        if ($type == 'number') {
            return new FormatNumber();
        }

        if ($type == 'string') {
            return new FormatString();
        }

        echo 'error occured';
    }
}