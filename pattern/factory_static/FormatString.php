<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 14:16
 */

include_once 'FormatterInterface.php';

class FormatString implements FormatterInterface
{
    public static function getDateTime(){
        return date('c') . ' = ' . __METHOD__;
    }
}