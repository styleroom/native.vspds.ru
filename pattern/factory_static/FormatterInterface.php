<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 14:16
 */

interface FormatterInterface
{
    public static function getDateTime();
}