<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 12.07.17
 * Time: 14:17
 */

include_once 'FormatterInterface.php';

class FormatNumber implements FormatterInterface
{
    public static function getDateTime(){
        return date('c') . ' = ' . __METHOD__;
    }
}