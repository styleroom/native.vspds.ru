/**
 * Created by p.vasin on 28.08.17.
 */

class ObjectPosition {

    // Element object
    private _elem: any;
    // Element info: width, height, let, top, e.g.
    private _elemAbout: any;
    // window width
    private _windowWidth: number;
    // window height
    private _windowHeight: number;
    // body padding
    public padding = 20;

    constructor(selector: string) {
        if (!(this._elem = document.querySelector(selector))) {
            throw new Error(`Error! The object via ${selector} does not exist!`);
        }
        this.setElementAbout(this._elem);
    }

    // --- main methods ------

    /**
     * Set this._elemAbout inside constructor
     * @param Element value
     */
    private setElementAbout(value: any): any {
        this._elemAbout = (function(value: any): any {
            let rect = value.getBoundingClientRect();
            let width: number = Math.ceil(rect.right - rect.left);
            let height: number = Math.ceil(rect.bottom - rect.top);
            return {
                left: rect.left,
                top: rect.top,
                right: rect.right,
                bottom: rect.bottom,
                width: width,
                height: height,
                midW: Math.ceil(rect.left + width/2),
                midH: Math.ceil(rect.top + height/2)
            }
        })(value);
    }

    /**
     * Check element inside viewport
     * @returns {boolean}
     */
    public checkObjectInViewport(): boolean {
        let result = true;
        let windowWidth = this.windowWidth;
        let windowHeight = this.windowHeight;
        let fromLeft: number = Math.ceil(windowWidth - this._elemAbout.midW - this.padding);
        let fromTop: number = Math.ceil(windowHeight - this._elemAbout.midH - this.padding);
        if (fromLeft <= 0 && fromTop <= 0) {
            result = false;
        }
        return result;
    }


    // --- getters ------

    /**
     * Get element info
     * @returns {any}
     */
    get elemAbout(): any {
        return this._elemAbout;
    }

    /**
     * Get need element
     * @returns {any}
     */
    get elem(): any {
        return this._elem;
    }

    /**
     * Get cross-browser window width
     * @returns {number}
     */
    get windowWidth(): number {
        return window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
    }

    /**
     * Get cross-browser window height
     * @returns {number}
     */
    get windowHeight(): number {
        return window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;
    }

}

let objpos = new ObjectPosition('.myclass');
window.onresize = function(event) {
    console.log(objpos.checkObjectInViewport());
};