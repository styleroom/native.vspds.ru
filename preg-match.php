<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<?php
$url = $_GET['url'];
echo <<< HTML
<script>
var sitesAll = [
        "philips",
        "pfse",
        "olympus",
        "castrol",
        "castrol-original",
        "grohe",
        "grundfos",
        "tefal",
        "rowetna",
        "moulinex",
        "krups",
        "zte",
        "myzte",
        "imtoy",
        "beastmode",
        "purina",
        "philipsaudio",
        "onkyo",
        "pen"    
];
var sitesAllRegString = sitesAll.join('|');
var regex = new RegExp(sitesAllRegString, "g");
var url = "$url";
var result = url.match(regex);
console.log('url result = ',result);
</script>
HTML;

?>
</body>
</html>