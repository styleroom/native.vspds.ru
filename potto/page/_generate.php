<?php

$xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<page>
    <title>---</title>
    <description>---</description>
    <img src="---"/>
    <sidebar file="---"/>
    <content><![CDATA[---]]></content>
</page>
XML;

$tome = 1;
$pages = 10;

$pages_1 = 52;
$pages_2 = 45;
$pages_3 = 41;
$pages_4 = 39;
$pages_5 = 31;

for ($i=1;$i<=$pages_1;$i++) {
    $index = ($i < 10) ? '0'.$i : $i;
    $filename = "{$tome}{$index}.xml";
    // $filename = __DIR__."/{$tome}{$index}.xml";
    file_put_contents($filename,$xml);
}