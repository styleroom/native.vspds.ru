<?php

$page = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<page>
    <title>#TITLE#</title>
    <description>---</description>
    <img src="---"/>
    <sidebar file="---"/>
    <content><![CDATA[---]]></content>
</page>
XML;

$xml = simplexml_load_file('../site/db.xml');
foreach ($xml as $val) {
    //print_r($val);
    echo '<hr>';
    echo $tome = $val->attributes();
    echo '<hr><hr>';
    $i=1;
    foreach ($val as $item) {
        $index = ($i < 10) ? '0'.$i : $i;
        echo $tome.$index.'<br>';
        echo $item->title;
        echo '<hr>';
        $i++;
        $page2 = str_replace('#TITLE#',$item->title,$page);
        $filename = "{$tome}{$index}.xml";
        file_put_contents($filename,$page2);
        $page2 = '';
    }
}