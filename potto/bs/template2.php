<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">
    <title>Potto</title>
    <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/3.3/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/3.3/examples/starter-template/starter-template.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="https://getbootstrap.com/docs/3.3/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="https://getbootstrap.com/docs/3.3/assets/js/ie-emulation-modes-warning.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Ledger" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed" rel="stylesheet">
    <link href="mystyle2.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Кавказская Война</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Главная</a></li>
                <li class="hidden-xs"><a>|</a></li>
                <li><a href="/tome1" title="--">Том 1</a></li>
                <li><a href="/tome2" title="--">Том 2</a></li>
                <li><a href="/tome3" title="--">Том 3</a></li>
                <li><a href="/tome4" title="--">Том 4</a></li>
                <li><a href="/tome5" title="--">Том 5</a></li>
                <li class="hidden-xs"><a>|</a></li>
                <li class="hidden-sm"><a href="#about">О проекте</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div id="page" class="container">

<div class="row">
    <div class="col-md-10">
        <h1>Длинное название статьи</h1>
        <p><strong>Министр культуры Владимир Мединский считает, что любые попытки давления &laquo;инициативщиков&raquo; на кинотеатры с требованием запретить показ фильма Алексея Учителя &laquo;Матильда&raquo; являются беззаконием.</strong></p>

        <p>&laquo;Любые намерения &laquo;инициативщиков&raquo; на местах &laquo;запретить показ&raquo;, любые попытки давления на частные или муниципальные кинотеатры &ndash; это чистой воды беззаконие и цензура&raquo;, &ndash; передает его слова&nbsp;<a href="http://tass.ru/" style="color: rgb(0, 0, 0);" target="_blank">ТАСС</a>.</p>

        <p>Он напомнил, что цензура напрямую запрещена Конституцией.</p>

        <p>Он отметил, что Минкультуры &laquo;до последнего старалось воздерживаться&raquo; от вмешательства в скандал вокруг фильма, однако нынешние события вынуждают высказываться.</p>

        <p>При этом он отметил, что не знает о мотивах депутата Госдумы Натальи Поклонской, которая затеяла и поддерживает &laquo;этот гвалт&raquo;.</p>

        <p>&laquo;Может, и от чистого сердца. Тем более не готов разгадывать мотивации разнокалиберных &laquo;активистов&raquo; &ndash; поджигателей, нагло именующих себя &laquo;православными&raquo;, &ndash; признался Мединский.</p>

        <p>Он отметил, что самозваные &laquo;активисты&raquo; &laquo;дискредитируют и государственную культурную политику, и церковь&raquo;, и сравнил их с фанатиками других конфессий или сект &laquo;в худшем их проявлении&raquo;.</p>

        <p>В итоге, считает министр, &laquo;истерия достигла невиданного накала&raquo; и дошла до &laquo;публичных угроз&raquo; и &laquo;поджогов&raquo;.</p>

        <blockquote>
            <p>&laquo;Любая полемика о фильме сейчас беспредметна, вдвойне абсурдна позиция граждан, которые &laquo;фильм не видели, но гневно осуждают&raquo; &ndash; и даже бравируют этим.</p>
        </blockquote>

        <p>Лично я фильм видел. Не буду обсуждать его содержание &ndash; это просто некорректно, пока его не увидят зрители, но свидетельствую: в нем нет ничего оскорбительного ни для памяти Николая II, ни для истории российской монархии&raquo;, &ndash; уверен Мединский.</p>

        <p>По его словам, это очевидные вещи.</p>

        <p>Он добавил, что в случае с &laquo;Матильдой&raquo; нет никаких законных оснований для отказа в выдаче прокатного удостоверения.</p>

        <p>&laquo;Фильму присвоена категория 16+. Это единственное предусмотренное законом ограничение на условия кинопроката&raquo;, &ndash; отметил он.</p>

        <p>Министр заявил, что в России здоровое гражданское общество, которое имеет &laquo;достаточно законных и эффективных механизмов общественного диалога&raquo;.</p>

        <p>&laquo;Идя на поводу у отрицающих закон &laquo;активистов&raquo;, мы создаем опасный прецедент потакания безответственному кликушеству&raquo;, &ndash; считает он.</p>

        <p>При этом работникам киноотрасли он заявил, что их работу оценит зритель, а &laquo;сектанты-поджигатели&raquo; &ndash; &laquo;клиенты других специализированных учреждений&raquo;.</p>

        <p>Также он попросил правоохранителей обеспечить законность и жестко пресечь давление на кинобизнес, дав принципиальную и справедливую оценку их действиям.</p>

        <p>Во вторник вечером после&nbsp;<a href="https://vz.ru/news/2017/9/12/886774.html" style="color: rgb(0, 0, 0);" target="_blank">решения</a>&nbsp;объединенной сети &laquo;Синема Парк&raquo; и &laquo;Формула кино&raquo; не показывать фильм &laquo;Матильда&raquo; Мединский заявил, что&nbsp;<a href="https://vz.ru/news/2017/9/12/886815.html" style="color: rgb(0, 0, 0);" target="_blank">его чаша терпения переполнена</a>.</p>

        <p>Позже министр пояснил, что его возмущает не отказ кинотеатров, а&nbsp;<a href="https://vz.ru/news/2017/9/13/886898.html" style="color: rgb(0, 0, 0);" target="_blank">умышленно нагнетаемая истерия</a>&nbsp;вокруг обычной художественной картины.</p>

        <p>Текст: Сергей Гурьянов</p>

    </div>
    <div class="col-md-2">
        <p><img class="img-thumbnail" src="cover1.jpg"></p>
        <p><img class="img-thumbnail" src="cover2.jpg"></p>
        <p><img class="img-thumbnail" src="cover3.jpg"></p>
        <p><img class="img-thumbnail" src="cover4.jpg"></p>
        <p><img class="img-thumbnail" src="cover5.jpg"></p>
    </div>
</div>

</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://getbootstrap.com/docs/3.3/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
