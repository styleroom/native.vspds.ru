/**
 * Created by p.vasin on 23.08.17.
 */
export default class Class01 {
    public qwe: string;
    public asd = 'hello from Class01';
    public zxc(arg1:string, arg2 = 'second arg'): void {
        let result = arg1 + ' ' + arg2;
        alert(result);
    }
}