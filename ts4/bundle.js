(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by p.vasin on 23.08.17.
 */
var Class01 = (function () {
    function Class01() {
        this.asd = 'hello from Class01';
    }
    Class01.prototype.zxc = function (arg1, arg2) {
        if (arg2 === void 0) { arg2 = 'second arg'; }
        var result = arg1 + ' ' + arg2;
        alert(result);
    };
    return Class01;
}());
exports.default = Class01;

},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by p.vasin on 23.08.17.
 */
var Class02 = (function () {
    function Class02() {
    }
    Class02.nowConfirm = function () {
        confirm('Hi, dude!');
    };
    return Class02;
}());
exports.default = Class02;

},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by p.vasin on 23.08.17.
 */
var class01_1 = require("./class01");
var class02_1 = require("./class02");
var class01 = new class01_1.default;
class01.zxc(class01.asd);
class02_1.default.nowConfirm();
alert('ну вот и ффсе!');

},{"./class01":1,"./class02":2}]},{},[1,2,3]);
