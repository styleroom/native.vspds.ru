<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>О русских людях. Рассказ второй. 1865 год</title>
    <meta name="author" content="rusemp.info">
    <link rel="icon" href="http://rusemp.vpvd.ru/favicon.ico">
    <link rel="shortcut icon" href="http://rusemp.vpvd.ru/favicon.ico" />
    <link rel="icon" href="http://rusemp.vpvd.ru/favicon.ico" type="image/x-icon" />
    <link href="jscss/lightbox.css" rel="stylesheet">
    <link href="jscss/custom.css" rel="stylesheet">
    <link href="jscss/responsive.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="jscss/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="jscss/ie-emulation-modes-warning.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="page">
    <div class="content">
        <article>
            <div class="mobile-rusemp">
                <img style="" src="/images/mobile-rusemp-exit.png">
                <div class="break-light"></div>
            </div>
            <header>
                <h1>Коротко о проекте «Российская Империя.info»</h1>
            </header>
            <section>
                <p>
                    Сайт <a target="_blank" href="/">rusemp.info</a> создан для публикации информационных материалов из различных изданий выпущенных во времена Имперской России.
                    Это могут быть:
                </p>
                <ul>
                    <li>периодические издания: газеты, журналы, отчеты</li>
                    <li>иллюстративные материалы: портреты, рисунки, карты, схемы, атласы</li>
                    <li>книги по различным тематикам</li>
                    <li>адрес-календари, памятные и справочные книги</li>
                    <li>библиографии</li>
                    <li>генеалогические и биографические справочники</li>
                    <li>законодательство Российской Империи</li>
                    <li>газеты русского зарубежья</li>
                    <li>официальные источники Российской Империи</li>
                    <li>материалы губернских ученых архивных комиссий</li>
                    <li>и т.д.</li>
                </ul>
                <p>
                    <a data-lightbox="image-1" data-title="пример оригинального имперского документа" href="https://img-fotki.yandex.ru/get/16121/13223519.32/0_9cf70_426403bb_XL">
                        <img class="img-left img-thumbnail" alt="пример оригинального имперского документа" src="https://img-fotki.yandex.ru/get/16121/13223519.32/0_9cf70_426403bb_S">
                    </a>
                    Материалы для этого сайта будут перепечатываться вручную с отсканированных ориигналов.
                    Одновременно с этим, будут даваться ссылки на исходники для возможности сличения и подтверждения подлинности материалов этого сайта.
                    Работа по созданию текстовых копий очень трудоемкая, по этому наполнение проекта <a target="_blank" href="/">rusemp.info</a> будет
                    не очень быстрое.
                    Если Вы желаете помочь ресурсу в наполнении, то напишите письмо на странице <a target="_blank" href="/contact.html">обратной связи</a>.
                    Высказать пожелания или что-то подсказать можно в <a target="_blank" href="/guestbook.html">гостевой книге</a>.
                </p>
                <p>
                    Просьба, если Вы знаете где можно в он-лайн режиме ознакомиться с материалами
                    изданными в период Российской Империи, сообщите пожалуйста об этом на странице <a target="_blank" href="/contact.html">обратной связи</a>.
                </p>

                <blockquote>
                    <strong>Техническая информация:</strong> сайт <a target="_blank" href="/">rusemp.info</a> работает на фреймворке
                    <a href="http://www.yiiframework.com/download/" target="_blank">Yii 1.15</a>,
                    в качестве гостевой книги используется веб-сервис <a target="_blank" href="https://disqus.com/">Disqus</a>,
                    для просмотра изображений используется лайтбокс <a target="_blank" href="http://lokeshdhakar.com/projects/lightbox2/">lightbox2</a>.
                    Сайт адаптирован к просмотру на мобильных устройствах и мониторах с различным разрешением экранов.
                    <div class="break-light"></div>
                    Название домена <a target="_blank" href="/">rusemp.info</a> является сокращением <br>от английского словосочетания <strong>Russian Empire Information</strong>,
                    <br>что переводится как <strong>Информация Российской Империи</strong>.
                </blockquote>

                <div class="table-responsive" style="display: none;">
                    <iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/donate.xml?account=4100174723687&amp;quickpay=donate&amp;payment-type-choice=on&amp;default-sum=15&amp;targets=на+перепечатку+исторических+оригиналов+для+сайта+rusemp.info&amp;target-visibility=on&amp;project-name=Информация+Российской+Империи&amp;project-site=http%3A%2F%2Frusemp.info&amp;button-text=05&amp;successURL=http%3A%2F%2Frusemp.info%2Fthanks.html" width="507" height="133">
                    </iframe>
                </div>                </section>
        </article>
        <div class="clear"></div>
    </div>
    <div class="sidebar">
        <aside>
            <ul class="menu" id="yw1">
                <li title="На главную страницу" class="index_item"><a href="/">Главная</a></li>
                <li title="Перечень материалов"><a href="/publications.html">Материалы</a></li>
                <li title="О сайте rusemp.info"><a href="/about.html">Проект rusemp</a></li>
                <li title="Оставить отзыв или пожелание"><a href="/guestbook.html">Гостевая книга</a></li>
                <li title="Написать письмо"><a href="/contact.html">Обратная связь</a></li>
                <li title="это интересно"><a target="_blank" href="http://radioheart.ru/iamruss">Радио "Я-русский"</a></li>
            </ul><div class='break-light'></div>                <div class='menu_sect'>
                <ul class="menu" id="yw2">
                    <li class="header"><span>Меню книги</span></li>
                    <li class="active"><a href="/russians.html">Обложка</a></li>
                    <li><a href="/russians/part01.html">Часть №1</a></li>
                    <li><a href="/russians/part02.html">Часть №2</a></li>
                    <li><a href="/russians/part03.html">Часть №3</a></li>
                    <li><a href="/russians/part04.html">Часть №4</a></li>
                    <li><a href="/russians/part05.html">Часть №5</a></li>
                </ul></div>
            <!--                <div class="partner-link">
                                <div class='break-light'></div>
                                <ul class="menu">
                                    <li>Партнерские ссылки:</li>
                                    <li><a title="Я-русский" target="_blank" href="http://iamruss.ru">iamruss.ru</a></li>
                                    <li><a title="Старые русские фотографии" target="_blank" href="http://old-rus-photo.vpvlab.ru">old-rus-photo.vpvlab.ru</a></li>
                                </ul>
                            </div> -->
            <div class="copy">
                <div class='break-light'></div>
                <a title="Russian Empire Information" href="/">&copy; rusemp.info | 2014</a>
            </div>
        </aside>
    </div>
    <div class="clear"></div>
</div>
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="jscss/lightbox.js"></script>
<script src="jscss/custom.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="jscss/ie10-viewport-bug-workaround.js"></script>

</body>
</html>
