<?php
/**
 * Created by PhpStorm.
 * User: p.vasin
 * Date: 11.09.17
 * Time: 11:07
 */
$pathPage = (!isset($_GET['path'])) ? false : $_GET['path'];

if ($pathPage) {
    // anybody here

    $xml = simplexml_load_file('page/index.xml');
    $pageTitle = $xml->title;
    $pageDescription = $xml->description;
    $sidebar = $xml->sidebar;
    $content = $xml->content;
    $contentClear = preg_replace([
        '/\n/',
        '/\s{2,}/',
    ],[
        '',
        ' ',
    ],$content);
    $contentClear = trim($contentClear);
    $pageContent = $contentClear;

    ob_start();
    //echo $contentClear;
    require_once 'layout/main.php';
    ob_end_flush();

} else {
    echo __FILE__;
}