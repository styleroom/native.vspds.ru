<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Теплый пол</title>
    <meta name="description" content="Default Description" />
    <meta name="keywords" content="Magento, Varien, E-commerce" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <link rel="icon" href="http://shop-rehau.ru/media/favicon/stores/21/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="http://shop-rehau.ru/media/favicon/stores/21/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery/jquery-migrate-3.0.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/7767d8d39ac1554d0ba861880a4ae5d6.css" />
    <link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/8faaa3518eb7fb1b8f919e6e508cc727.css" media="all" />
    <script type="text/javascript" src="http://shop-rehau.ru/media/js/8625f07986b20aaf8327f9c53c639032.js"></script>
    <link rel="canonical" href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau.html" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic" />
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Pacifico" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Serif" />

    <script type="text/javascript">
        //<![CDATA[
        optionalZipCountries = ["RU"];
        //]]>
    </script>

    <script>var baseMessages={please_wait: "Пожалуйста, ждите",successfully: "Успешно",request_error: "Ошибка запроса"}</script><script type="text/javascript">
        etCurrencyManagerJsConfig={"precision":0,"position":8,"display":2,"locale":"ru_RU","symbol":"\u20bd","input_admin":0,"excludecheckout":"0","cutzerodecimal":0,"cutzerodecimal_suffix":""}</script>

    <script>var evoMessages={next: "Вперед",prev: "Назад"}</script></head>

<!-- home -->
<link rel="stylesheet" href="style.css">

<body class=" catalog-category-view categorypath-obogrev-ohlazhdenie-poverhnostei-rehau category-obogrev-ohlazhdenie-poverhnostei-rehau">
<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div><div class="page-wrapper">

    <header class="pv_header_fullwidth">
        <div class="container header__container">
            <div class="row">
                <div class="col-xs-12" id="header-mobile">
                    <span class="header-mobile__shop-name"></span>
                    <a class="header-mobile__phone-number" href=""></a>
                </div>
                <div class="col-md-2 col-xs-7">
                    <div class="header__logo">
                        <a href="http://shop-rehau.ru/">
                            <img src="http://shop-rehau.ru/skin/frontend/evo/rehau/images/logo.png" alt="интернет-магазин Rehau">
                        </a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-5">
                    <div class="header__top">
                        <span class="header__shop-name" style="font-size: 0.875rem">Официальный интернет-магазин</span>
                        <a style="margin-left:20px; margin-right:7px; font-size: 1rem;" class="header__phone" href="tel:+7 (495) 118-25-87">+7 (495) 118-25-87</a>
                        <a style="margin-left:0; margin-right:0; font-size: 1rem;" class="header__phone" href="tel:8 (800) 350-09-61">8 (800) 350-09-61</a>
                        <span class="header__work-time">Пн-Пт: 09:00 - 21:00, Сб-Вс: 09:00 - 19:00</span>                </div>
                    <div class="header__region">

                        <div id="regionPopover">
                            <div class="btn-group dropdown region">
                                <button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown">
                                    <span id="icon"><i class="material-icons pr-10">&#xE569;</i></span> <span id="header_region_city">г. Москва</span>
                                </button>

                                <div class="popover-content dropdown-menu dropdown-menu-right dropdown-animation">
                                    <div class="region">


                                        <p class="header-place-descr">Возможно, мы неправильно определили ваше месторасположение, Вы можете
                                            указать его самостоятельно:</p>

                                        <div class="dropdown">
                                            <input class="header-region" type="text" placeholder="Ваше месторасположение" title="Ваше месторасположение" name="header_region_input" id="header_region_input" />
                                            <input type="hidden" name="header_region_input_kladr"/>
                                        </div>

                                        <ul class="defaultCity">
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7700000000000">г. Москва</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7800000000000">г. Санкт-Петербург</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6600000100000">г. Екатеринбург</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=5400000100000">г. Новосибирск</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=2300000100000">г. Краснодар</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6100000100000">г. Ростов-на-Дону</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6300000100000">г. Самара</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7400000100000">г. Челябинск</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=2500000100000">г. Владивосток</a>
                                            </li>
                                        </ul>


                                        <ul class="defaultCity">
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=5200000100000">г. Нижний Новгород</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=2700000100000">г. Хабаровск</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=0200000100000">г. Уфа</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=1600000100000">г. Казань</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=2400000100000">г. Красноярск</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=5900000100000">г. Пермь</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=3600000100000">г. Воронеж</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=3400000100000">г. Волгоград</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=7200000100000">г. Тюмень</a>
                                            </li>
                                        </ul>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <script type="text/javascript">

                            function AristosVisitor_getSetRegionUrl(kladr, name) {
                                urlTemplate = "/visitor_region/region/setRegion?kladr=$1";
                                return urlTemplate.replace("$1", kladr).replace("$2", name);
                            }

                            function AristosVisitor_setRegionByUrl(url) {
                                var btn = jQuery("#regionPopover .popover-btn");
                                btn.trigger('click');
                                jQuery.ajax({
                                    type: "GET",
                                    url: url,
                                    beforeSend: function() {
                                        btn.addClass('loading');
                                    }
                                }).done(function (data) {
                                        btn.removeClass('loading');
                                        btn.html(data);
                                        setTimeout(function () {
                                            location.reload();
                                        }, 500);
                                    }
                                );
                            }

                        </script>                </div>
                    <div class="header__links">
                        <a href="/dostavka" class="header__links__a" title="">Доставка</a>
                        <a href="/rehau-oplata" class="header__links__a" title="">Оплата</a>
                        <a href="/samovivoz" class="header__links__a" title="">Самовывоз</a>
                        <a href="/contacts.html" class="header__links__a" title="">Контакты</a>
                        <a href="/product/specifications" class="header__links__a pink-color" title="">Заказать по артикулам</a>
                    </div>                <div class="header__menu">
                        <div class="navbar-btn">
                            <a href="#" class="open-panel">
                                <span class="mobile-menu__button" aria-hidden="true"></span>
                            </a>
                        </div>
                        <nav class="moving navbar navbar-default">
                            <div class="menu" id="navbar-collapse-2">

                                <!--        --><!--            <div class="register-link">-->
                                <!--                <a href="--><!--">-->
                                <!--                    --><!--</a>-->
                                <!--            </div>-->
                                <!--        -->
                                <ul class="nav navbar-nav">
                                    <li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan.html" class="dropdown-toggle">RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan.html" class="dropdown">Трубопроводная система RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan.html" >Универсальные трубы RAUTITAN </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/prinadlezhnosti-dlja-trub.html" >Принадлежности для труб </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan.html" class="dropdown">Универсальные фитинги RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/nadvizhnye-gil-zy.html" >Надвижные гильзы </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/trojniki.html" >Тройники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/soedinitel-nye-mufty-i-perehodniki.html" >Соединительные муфты и переходники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/ugol-niki.html" >Угольники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/varianty-podkljuchenija-iz-steny.html" >Варианты подключения из стены. Настенные угольники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/kronshtei-ny.html" >Кронштейны </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/montazhnye-bloki.html" >Монтажные блоки </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/sistema-nastennyh-flancevyh-ugol-nikov.html" >Система настенных фланцевых угольников </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/raspredeliteli.html" >Распределители </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/komplektujuschie-dlja-fasonnyh-chastei.html" >Комплектующие для фасонных частей </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan.html" class="dropdown">Система радиаторной разводки RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/fitingi-i-komplektujuschie-dlja-prisoedinenija-k-otopitel-nym-priboram.html" >Фитинги и комплектующие для присоединения к отопительным приборам </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/rez-bozazhimnye-soedinenija-sharovye-krany.html" >Резьбозажимные соединения / шаровые краны </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/odnoploskostnaja-krestovina.html" >Одноплоскостная крестовина </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/raspredelitel-nye-kollektory-komplektujuschie.html" >Распределительные коллекторы / комплектующие </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/prisoedinitel-nye-garnitury-sl.html" >Присоединительные гарнитуры SL </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/sl-nabor-fitingov-rautitan-s-gil-zami.html" >SL – Набор фитингов RAUTITAN с гильзами </a></li></ul></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/plintusnye-kanaly-rehau.html" >Плинтусные каналы REHAU </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus.html" class="dropdown-toggle">RAUPIANO Plus <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/kanalizacionnaja-truba-raupiano-plus.html" >Канализационная труба RAUPIANO Plus </a></li><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/fasonnye-chasti-dlja-kanalizacii.html" >Фасонные части для канализации </a></li><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/tehnika-soedinenija-dlja-sistemy-raupiano-plus.html" >Техника соединения для системы RAUPIANO Plus </a></li><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/prinadlezhnosti.html" >Принадлежности </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec.html" class="dropdown-toggle">SOLELEC <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec/jelektricheskie-grejuschie-maty.html" >Электрические греющие маты </a></li><li class=" "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec/jelektricheskie-grejuschie-kabeli.html" >Электрические греющие кабели </a></li><li class=" "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec/termoreguljatory.html" >Терморегуляторы </a></li></ul></li><li class="dropdown-submenu"><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau.html" class="dropdown-toggle">Теплый пол <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby.html" class="dropdown">Трубы и гофротрубы <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby/truba-rautherm-s.html" >Труба RAUTHERM S </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby/gofrotruby.html" >Гофротрубы </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi.html" class="dropdown">Надвижные гильзы и фитинги <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi/nadvizhnye-gil-zy.html" >Надвижные гильзы </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi/fitingi.html" >Фитинги </a></li></ul></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sposoby-kreplenija-trub-k-polu.html" >Крепления труб к полу </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-nastennogo-obogreva-ohlazhdenija.html" >Системы укладки для настенного обогрева/охлаждения </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-potolochnogo-obogreva-ohlazhdenija.html" >Системы укладки для потолочного обогрева/охлаждения </a></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory.html" class="dropdown">Коллекторы <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory/raspredelitel-nye-kollektory-i-komplektujuschie.html" >Распределительные коллекторы и комплектующие </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory/raspredelitel-nye-shkafy.html" >Распределительные шкафы </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea.html" class="dropdown">Системы автоматического регулирования  <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-avtomaticheskogo-regulirovanija-ns-bus.html" >Система автоматического регулирования НС BUS </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-avtomaticheskogo-regulirovanija-nea.html" >Система автоматического регулирования Nea </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-regulirovanija.html" >Монтаж системы регулирования </a></li></ul></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-podogreva-i-ohlazhdenija-jadra-betonnyh-perekrytii.html" >Система подогрева и охлаждения ядра бетонных перекрытий </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/napol-noe-otoplenie-rehau-dlja-promyshlennyh-zdanij.html" >Напольное отопление REHAU для промышленных зданий </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-otoplenija-amortizirujuschih-polov-v-sportzalah.html" >Системы для отопления амортизирующих полов в спортзалах </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-obogreva-otkrytyh-ploschadok.html" >Системы для обогрева открытых площадок </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/promyshlennyi-kollektor-rehau.html" >Промышленный коллектор REHAU </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool.html" class="dropdown-toggle">RAUTOOL <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5.html" >Механический монтажный инструмент для надвижки гильз на диаметр 10 x 1,1/14 x 1,5 </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40.html" >Монтажный инструмент для надвижки гильз на диаметр 16-40 </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapchasti-k-instrumentu.html" >Комплектующие и запчасти к инструменту </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument.html" >Дополнительный инструмент </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40.html" >Комплектующие и запасные части на диаметр 16-40 </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110.html" >Гидравлический монтажный инструмент и комплектующие к нему на диаметр 40-110 </a></li></ul></li>                            </ul>
                                <div class="header__links-mobile"></div>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12">
                    <div class="header__login">
                        <div class="btn-group login">
                            <a class="btn dropdown-toggle btn-default btn-sm" href="http://shop-rehau.ru/customer/account/create/is_head_login_form/1/">
                                Вход    </a>
                        </div>                                            <div class="btn-group">
                            <a href="http://shop-rehau.ru/customer/account/create/" class="btn btn-default btn-sm user-link__desctop"> Регистрация</a>
                            <a href="http://shop-rehau.ru/customer/account/create/" class="user-icon__mobile"></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="header__cart">
                        <div  id="header-shopping-cart" class="header-cart" data-pjax-container="" data-pjax-timeout="7000" data-pjax-overlay-disable="true">                    <div class="btn-group">
                                <a href="http://shop-rehau.ru/checkout/cart/" class="btn">
                                    <span class="cart-icon"></span>
                                    <!--        <i class="fa fa-shopping-cart"></i>-->
                                    <span class="rehau-cart-count">Корзина: 0 шт.</span>
                                </a>
                            </div>
                        </div>                </div>
                    <div class="clearfix"></div>
                    <div class="header__search">
                        <form method="get" action="http://shop-rehau.ru/catalogsearch/result/" role="search" class="search-box margin-clear">
                            <div class="form-group has-feedback">
                                <input id="search" type="text" name="q" value="" class="form-control" placeholder="Поиск" required>
                                <button type="submit" title="Поиск" class="button"><span class="search__icon"></span><span class="mobile-search__text">Найти</span></button>
                                <div id="search_autocomplete" class="search-autocomplete"></div>
                            </div>
                        </form>                </div>
                </div>
                <div id="header-mobile__bottom" class="col-xs-12">
                    <div class="header-mobile__menu"></div>
                    <div class="header-mobile__search-button"></div>
                    <div class="header-mobile__search"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </header>    <div id="page-start"></div>
    <div class="container">
        <noscript>
            <div class="global-site-notice noscript">
                <div class="notice-inner">
                    <p>
                        <strong>Скорее всего в вашем браузере отключён JavaScript.</strong><br />
                        Вы должны включить JavaScript в вашем браузере, чтобы использовать все возможности этого сайта.                </p>
                </div>
            </div>
        </noscript>
    </div>
    <section class="main-container">
        <div class="breadcrumb-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="home ">
                                <i class="fa fa-home pr-10"></i>
                                <a class="link-dark" href="http://shop-rehau.ru/"
                                   title="Перейти на главную страницу">Главная</a>
                            </li>
                            <li class="category745 active">
                                Теплый пол                                                            </li>
                        </ol>
                        <h2 class="breadcrumbs__page-title">Теплый пол</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="main col-md-12" role="main">




                    <!-- Modal rehauAlertSaved -->
                    <div class="modal fade alert-saved" id="rehauAlertSaved" tabindex="-1" role="dialog" aria-labelledby="rehauAlertSavedLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>Поздравляем!</strong> Подписка на оповещение сохранена!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h1 class="page-title">Теплый пол</h1>



                    <!-- div class="row-fluid category-items-list masonry-grid-fitrows" -->
                    <div class="row subcategory category-items-list masonry-grid-fitrows clearfix">


                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby.html" title="Трубы и гофротрубы">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-398272_14_1.jpg" alt="Трубы и гофротрубы" title="Трубы и гофротрубы" />                                                    </span>
                                    <span class="text">
                              Трубы и гофротрубы                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi.html" title="Надвижные гильзы и фитинги">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-5101_3.jpg" alt="Надвижные гильзы и фитинги" title="Надвижные гильзы и фитинги" />                                                    </span>
                                    <span class="text">
                              Надвижные гильзы и фитинги                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sposoby-kreplenija-trub-k-polu.html" title="Крепления труб к полу">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-2612.jpg" alt="Крепления труб к полу" title="Крепления труб к полу" />                                                    </span>
                                    <span class="text">
                              Крепления труб к полу                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-nastennogo-obogreva-ohlazhdenija.html" title="Системы укладки для настенного обогрева/охлаждения">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-2612_1.jpg" alt="Системы укладки для настенного обогрева/охлаждения" title="Системы укладки для настенного обогрева/охлаждения" />                                                    </span>
                                    <span class="text">
                              Системы укладки для настенного обогрева/охлаждения                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-potolochnogo-obogreva-ohlazhdenija.html" title="Системы укладки для потолочного обогрева/охлаждения">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-1848_5.jpg" alt="Системы укладки для потолочного обогрева/охлаждения" title="Системы укладки для потолочного обогрева/охлаждения" />                                                    </span>
                                    <span class="text">
                              Системы укладки для потолочного обогрева/охлаждения                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory.html" title="Коллекторы">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-401978_14.jpg" alt="Коллекторы" title="Коллекторы" />                                                    </span>
                                    <span class="text">
                              Коллекторы                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea.html" title="Системы автоматического регулирования ">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-363410.jpg" alt="Системы автоматического регулирования " title="Системы автоматического регулирования " />                                                    </span>
                                    <span class="text">
                              Системы автоматического регулирования                         </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-podogreva-i-ohlazhdenija-jadra-betonnyh-perekrytii.html" title="Система подогрева и охлаждения ядра бетонных перекрытий">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-4463_2.jpg" alt="Система подогрева и охлаждения ядра бетонных перекрытий" title="Система подогрева и охлаждения ядра бетонных перекрытий" />                                                    </span>
                                    <span class="text">
                              Система подогрева и охлаждения ядра бетонных перекрытий                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/napol-noe-otoplenie-rehau-dlja-promyshlennyh-zdanij.html" title="Напольное отопление REHAU для промышленных зданий">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-2631_7.jpg" alt="Напольное отопление REHAU для промышленных зданий" title="Напольное отопление REHAU для промышленных зданий" />                                                    </span>
                                    <span class="text">
                              Напольное отопление REHAU для промышленных зданий                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-otoplenija-amortizirujuschih-polov-v-sportzalah.html" title="Системы для отопления амортизирующих полов в спортзалах">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-398272_14_2.jpg" alt="Системы для отопления амортизирующих полов в спортзалах" title="Системы для отопления амортизирующих полов в спортзалах" />                                                    </span>
                                    <span class="text">
                              Системы для отопления амортизирующих полов в спортзалах                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-obogreva-otkrytyh-ploschadok.html" title="Системы для обогрева открытых площадок">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-398272_14_3.jpg" alt="Системы для обогрева открытых площадок" title="Системы для обогрева открытых площадок" />                                                    </span>
                                    <span class="text">
                              Системы для обогрева открытых площадок                        </span>
                                </a>
                            </div>
                        </div>



                        <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                            <div class="listing-item white-bg bordered">
                                <a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/promyshlennyi-kollektor-rehau.html" title="Промышленный коллектор REHAU">
                        <span class="img">
                                                            <img src="http://shop-rehau.ru/media/catalog/category/M-1842_54.jpg" alt="Промышленный коллектор REHAU" title="Промышленный коллектор REHAU" />                                                    </span>
                                    <span class="text">
                              Промышленный коллектор REHAU                        </span>
                                </a>
                            </div>
                        </div>



                    </div>
                    <div class="clearfix"></div>

                    <script>
                        jQuery(document).on('ready', function () {
                            jQuery('.nav-pills a').on('click',function () {
                                var tabFilter = jQuery(this).data('filter');
                                jQuery('.tab-content ').isotope({ filter: tabFilter });
                            });
                        });
                    </script>

                    <!--<p class="note-msg">--><!--</p>-->
                </div>
            </div>
        </div>
    </section>
    <div class="container">
    </div>
    <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
    <!-- ================ -->
    <footer id="footer" class="clearfix ">

        <!-- .footer start -->
        <!-- ================ -->
        <div class="footer">
            <div class="container">
                <div class="footer-inner">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Продукция</li>
                                <li class="footer__ul-menu_link">
                                    <a href="/obogrev-ohlazhdenie-poverhnostei-rehau.html" title="">Тёплый пол</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/truboprovodnaja-sistema-rautitan.html" title="">Отопление и Водопровод</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus.html" title="">Канализация</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/montazhnyi-instrument-rautool.html" title="">Всё для монтажа</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Информация для покупателей</li>
                                <li class="footer__ul-menu_link">
                                    <a href="/dostavka" title="">Доставка</a>
                                <li class="footer__ul-menu_link">
                                    <a href="/samovivoz" title="">Самовывоз</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/rehau-oplata" title="">Условия оплаты</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/vozvrat-tovara" title="">Гарантии и возврат</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Спецпредложения</li>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Специальные предложения</a>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Акции и скидки</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Подарочные сертификаты</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 pull-right col-xs-12">
                            <div class="mobile-position">
                                <a href="tel:+7 (495) 118-25-87" class="footer__phone">+7 (495) 118-25-87</a>
                                <div><a href="tel:8 (800) 350-09-61" class="footer__phone">8 (800) 350-09-61</a></div>
                            </div>
                            <div class="footer__work-time">
                                Пн-Пт: 09:00 - 21:00<br>
                                Сб-Вс: 09:00 - 19:00
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="footer__bottom">
                                <a href="/pravila-raboti" title="">Правила использования</a>
                                <a href="/o-magazine" title="">О магазине</a>
                                <a href="/contacts.html" title="">Контакты</a>
                                <span>© 2017 Официальный интернет-магазин REHAU</span>
                                <span class="footer__social">
            <a href="//vk.com/rehau_ru" target="_blank" title="" class="footer__social_vk"></a>
            <a href="//www.facebook.com/rehau.ru" target="_blank" title="" class="footer__social_fb"></a>
            <a href="//www.youtube.com/user/rehauea" target="_blank" title="" class="footer__social_youtube"></a>
            <a href="//www.instagram.com/rehaurussia/" target="_blank" title="" class="footer__social_instagram"></a>
        </span>
                            </div>
                        </div>                                    </div>
                </div>
            </div>
        </div>
        <!-- .footer end -->

    </footer>
    <!-- footer end --></div>




<div id="advancednewsletter-overlay" style="display:none"></div>
<div id="subscribe-please-wait" style="display:none;">
    <img src="http://shop-rehau.ru/skin/frontend/base/default/images/opc-ajax-loader.gif" />&nbsp;Загружается...</div>
<div id="an-content" style="display:none"></div>
<script>var cartMessages={processing: "Обрабатывается",in_cart: "В корзину"}</script>
<script>var compareMessages={hide: "Скрыть",show: "Показать",_or_: " или "}</script>    <section id="compare-toolbar" class="compare-toolbar">
    <div class="compare-wrap">
        <div class="bar">
            <div class="title">
                <span class="groups"></span>
            </div>
        </div>
        <div class="content">
            <div class="compare-items">
                <p>У вас нет товаров для сравнения.</p>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/jquery.validate-1.17.0.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/localization/messages_ru.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/bootstrap/bootstrap-3.3.7.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/f7e6ef9e5c935838a27b221c80aaf41a.css" />
<link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/a09b88010e959de4f0a025bb826a8d86.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/45cf0c0049726c85908a930f13cf3c7e.css" media="print" />
<script type="text/javascript" src="http://shop-rehau.ru/media/js/454728669a5244e8a3b08183770e38db.js"></script>

<!-- home -->
<script type="text/javascript" src="script.js"></script>

<link rel="stylesheet" href="//cdn.aristosgroup.ru/fonts/helios-cond/font.css" />
<script type="text/javascript">
    var product = {
        category : []
    };

    product.category.push('Теплый пол');
</script>
</body>
</html>
