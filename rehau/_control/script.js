/**
 * Created by papasha on 05.11.2017.
 */
jQuery(document).on('ready', function () {

    jQuery('[data-toggle="popover"]').popover();

    var tabFilterLocalStorage = localStorage.getItem('tabFilter');
    if (tabFilterLocalStorage) {
        jQuery('a[data-filter="'+tabFilterLocalStorage+'"]').parents('li').addClass('active');
        jQuery('.tab-content').isotope({ filter: tabFilterLocalStorage });
    }

    jQuery('.nav-pills a').on('click',function () {
        var tabFilter = jQuery(this).data('filter');
        localStorage.setItem('tabFilter',tabFilter);
        jQuery('.tab-content').isotope({ filter: tabFilter });
    });

});