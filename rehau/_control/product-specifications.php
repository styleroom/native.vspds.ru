<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>интернет-магазин Rehau</title>
    <meta name="description" content="Default Description" />
    <meta name="keywords" content="Magento, Varien, E-commerce" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <link rel="icon" href="http://pv.rehau.tesla.aristos.pw/media/favicon/stores/21/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="http://pv.rehau.tesla.aristos.pw/media/favicon/stores/21/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery/jquery-migrate-3.0.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/aristos/base.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/fontawesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/fancybox/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/fancybox/helpers/jquery.fancybox-thumbs.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/fancybox/helpers/jquery.fancybox-buttons.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/aristos/subscriber/css/subscriber.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/bxslider/jquery.bxslider.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/rs-plugin/css/settings.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/owl-carousel/owl.transitions.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/hover/hover.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/owl-carousel/owl.theme.default.min.css" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/aw_blog/css/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/css/amshopby.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/css/bundle/toolbar.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/css/bundle/styles.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/css/aristos_bonus.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/aristos/animations.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/aristos/form/form.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/css/aristos/popover.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/css/aristos/customer.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/aristos/form/buttons.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/aristos/debug/panel.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/css/et_salesrulesgift.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/css/mirasvit_custom.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/fonts/raleway/css/raleway.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/default/fonts/fontello/css/fontello.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/default/fonts/fontello/css/animation.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/default/css/bootstrap/evo/animations.css" media="all" />
    <link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/css/bootstrap/style.css" media="all" />
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.blockui.min.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/base.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.storageapi.min.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/bundle.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/bonus.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery-ui-1.11.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/form.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/popover.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/customer.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/kladr.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/fancybox/helpers/jquery.fancybox-thumbs.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/fancybox/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/subscriber/js/subscriber.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/mirasvit_personal_stats.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/evo/layout.js"></script>
    <script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/et_currencymanager/et_currencymanager_round.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic" />
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Pacifico" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Serif" />

    <script type="text/javascript">
        //<![CDATA[
        optionalZipCountries = ["RU"];
        //]]>
    </script>

    <script>var baseMessages={please_wait: "Пожалуйста, ждите",successfully: "Успешно",request_error: "Ошибка запроса"}</script><script type="text/javascript">
        etCurrencyManagerJsConfig={"precision":0,"position":8,"display":2,"locale":"ru_RU","symbol":"\u20bd","input_admin":0,"excludecheckout":"0","cutzerodecimal":0,"cutzerodecimal_suffix":""}</script>

    <script>var evoMessages={next: "Вперед",prev: "Назад"}</script></head>

<!-- home -->
<link rel="stylesheet" href="style.css">


<body class=" aristos-product-specifications-index">
<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div><div class="page-wrapper">

    <header class="pv_header_fullwidth">
        <div class="container header__container">
            <div class="row">
                <div class="col-xs-12" id="header-mobile">
                    <span class="header-mobile__shop-name"></span>
                    <a class="header-mobile__phone-number" href=""></a>
                </div>
                <div class="col-md-2 col-xs-7">
                    <div class="header__logo">
                        <a href="http://pv.rehau.tesla.aristos.pw/">
                            <img src="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/images/logo.png" alt="интернет-магазин Rehau">
                        </a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-5">
                    <div class="header__top">
                        <span class="header__shop-name" style="font-size: 0.875rem">Официальный интернет-магазин</span>
                        <a style="margin-left:20px; margin-right:7px; font-size: 1rem;" class="header__phone" href="tel:+7 (495) 118-25-87">+7 (495) 118-25-87</a>
                        <a style="margin-left:0; margin-right:0; font-size: 1rem;" class="header__phone" href="tel:8 (800) 350-09-61">8 (800) 350-09-61</a>
                        <span class="header__work-time">Пн-Пт: 09:00 - 21:00, Сб-Вс: 09:00 - 19:00</span>                </div>
                    <div class="header__region">
                        <!--{HEADERREGION_7e6145cd3574a92970858bb6c10c9dfb}-->
                        <div id="regionPopover">
                            <div class="btn-group dropdown region">
                                <button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown">
                                    <span id="icon"><i class="material-icons pr-10">&#xE569;</i></span> <span id="header_region_city">г. Москва</span>
                                </button>

                                <div class="popover-content dropdown-menu dropdown-menu-right dropdown-animation">
                                    <div class="region">


                                        <p class="header-place-descr">Возможно, мы неправильно определили ваше месторасположение, Вы можете
                                            указать его самостоятельно:</p>

                                        <div class="dropdown">
                                            <input class="header-region" type="text" placeholder="Ваше месторасположение" title="Ваше месторасположение" name="header_region_input" id="header_region_input" />
                                            <input type="hidden" name="header_region_input_kladr"/>
                                        </div>

                                        <ul class="defaultCity">
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7700000000000">г. Москва</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7800000000000">г. Санкт-Петербург</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6600000100000">г. Екатеринбург</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=5400000100000">г. Новосибирск</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=2300000100000">г. Краснодар</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6100000100000">г. Ростов-на-Дону</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6300000100000">г. Самара</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7400000100000">г. Челябинск</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=2500000100000">г. Владивосток</a>
                                            </li>
                                        </ul>


                                        <ul class="defaultCity">
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=5200000100000">г. Нижний Новгород</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=2700000100000">г. Хабаровск</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=0200000100000">г. Уфа</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=1600000100000">г. Казань</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=2400000100000">г. Красноярск</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=5900000100000">г. Пермь</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=3600000100000">г. Воронеж</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=3400000100000">г. Волгоград</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=7200000100000">г. Тюмень</a>
                                            </li>
                                        </ul>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <script type="text/javascript">

                            function AristosVisitor_getSetRegionUrl(kladr, name) {
                                urlTemplate = "/visitor_region/region/setRegion?kladr=$1";
                                return urlTemplate.replace("$1", kladr).replace("$2", name);
                            }

                            function AristosVisitor_setRegionByUrl(url) {
                                var btn = jQuery("#regionPopover .popover-btn");
                                btn.trigger('click');
                                jQuery.ajax({
                                    type: "GET",
                                    url: url,
                                    beforeSend: function() {
                                        btn.addClass('loading');
                                    }
                                }).done(function (data) {
                                        btn.removeClass('loading');
                                        btn.html(data);
                                        setTimeout(function () {
                                            location.reload();
                                        }, 500);
                                    }
                                );
                            }

                        </script><!--/{HEADERREGION_7e6145cd3574a92970858bb6c10c9dfb}-->                </div>
                    <div class="header__links">
                        <a href="/dostavka" class="header__links__a" title="">Доставка</a>
                        <a href="/rehau-oplata" class="header__links__a" title="">Оплата</a>
                        <a href="/samovivoz" class="header__links__a" title="">Самовывоз</a>
                        <a href="/contacts.html" class="header__links__a" title="">Контакты</a>
                        <a href="/product/specifications" class="header__links__a pink-color" title="">Заказать по артикулам</a>
                    </div>                <div class="header__menu">
                        <!--{TOPMENU_f991f2903db667fd0b48582dbf650b89}--><div class="navbar-btn">
                            <a href="#" class="open-panel">
                                <span class="mobile-menu__button" aria-hidden="true"></span>
                            </a>
                        </div>
                        <nav class="moving navbar navbar-default">
                            <div class="menu" id="navbar-collapse-2">
                                <div class="btn-group login">
                                    <a class="btn dropdown-toggle btn-default btn-sm" href="http://pv.rehau.tesla.aristos.pw/customer/account/create/is_head_login_form/1/">
                                        Вход    </a>
                                </div>
                                <!--        --><!--            <div class="register-link">-->
                                <!--                <a href="--><!--">-->
                                <!--                    --><!--</a>-->
                                <!--            </div>-->
                                <!--        -->
                                <ul class="nav navbar-nav">
                                    <li class="level0 nav-1 first parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan.html" ><span>RAUTITAN</span></a><ul class="dropdown-menu level0"><li class="level1 nav-1-1 first parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan.html" ><span>Трубопроводная система RAUTITAN</span></a><ul class="dropdown-menu level1"><li class="level2 nav-1-1-1 first"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan.html" ><span>Универсальные трубы RAUTITAN</span></a></li><li class="level2 nav-1-1-2 last"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/prinadlezhnosti-dlja-trub.html" ><span>Принадлежности для труб</span></a></li></ul><div class="hover-mark"></div></li><li class="level1 nav-1-2 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan.html" ><span>Универсальные фитинги RAUTITAN</span></a><ul class="dropdown-menu level1"><li class="level2 nav-1-2-1 first"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/nadvizhnye-gil-zy.html" ><span>Надвижные гильзы</span></a></li><li class="level2 nav-1-2-2"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/trojniki.html" ><span>Тройники</span></a></li><li class="level2 nav-1-2-3"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/soedinitel-nye-mufty-i-perehodniki.html" ><span>Соединительные муфты и переходники</span></a></li><li class="level2 nav-1-2-4"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/ugol-niki.html" ><span>Угольники</span></a></li><li class="level2 nav-1-2-5"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/varianty-podkljuchenija-iz-steny.html" ><span>Варианты подключения из стены. Настенные угольники</span></a></li><li class="level2 nav-1-2-6"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/kronshtei-ny.html" ><span>Кронштейны</span></a></li><li class="level2 nav-1-2-7"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/montazhnye-bloki.html" ><span>Монтажные блоки</span></a></li><li class="level2 nav-1-2-8"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/sistema-nastennyh-flancevyh-ugol-nikov.html" ><span>Система настенных фланцевых угольников</span></a></li><li class="level2 nav-1-2-9"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/raspredeliteli.html" ><span>Распределители</span></a></li><li class="level2 nav-1-2-10 last"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/komplektujuschie-dlja-fasonnyh-chastei.html" ><span>Комплектующие для фасонных частей</span></a></li></ul><div class="hover-mark"></div></li><li class="level1 nav-1-3 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan.html" ><span>Система радиаторной разводки RAUTITAN</span></a><ul class="dropdown-menu level1"><li class="level2 nav-1-3-1 first"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/fitingi-i-komplektujuschie-dlja-prisoedinenija-k-otopitel-nym-priboram.html" ><span>Фитинги и комплектующие для присоединения к отопительным приборам</span></a></li><li class="level2 nav-1-3-2"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/rez-bozazhimnye-soedinenija-sharovye-krany.html" ><span>Резьбозажимные соединения / шаровые краны</span></a></li><li class="level2 nav-1-3-3"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/odnoploskostnaja-krestovina.html" ><span>Одноплоскостная крестовина</span></a></li><li class="level2 nav-1-3-4"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/raspredelitel-nye-kollektory-komplektujuschie.html" ><span>Распределительные коллекторы / комплектующие</span></a></li><li class="level2 nav-1-3-5"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/prisoedinitel-nye-garnitury-sl.html" ><span>Присоединительные гарнитуры SL</span></a></li><li class="level2 nav-1-3-6 last"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/sl-nabor-fitingov-rautitan-s-gil-zami.html" ><span>SL – Набор фитингов RAUTITAN с гильзами</span></a></li></ul><div class="hover-mark"></div></li><li class="level1 nav-1-4 last"><a href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/plintusnye-kanaly-rehau.html" ><span>Плинтусные каналы REHAU</span></a></li></ul><div class="hover-mark"></div></li><li class="level0 nav-2 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus.html" ><span>RAUPIANO Plus</span></a><ul class="dropdown-menu level0"><li class="level1 nav-2-1 first"><a href="http://pv.rehau.tesla.aristos.pw/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/kanalizacionnaja-truba-raupiano-plus.html" ><span>Канализационная труба RAUPIANO Plus</span></a></li><li class="level1 nav-2-2"><a href="http://pv.rehau.tesla.aristos.pw/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/fasonnye-chasti-dlja-kanalizacii.html" ><span>Фасонные части для канализации</span></a></li><li class="level1 nav-2-3"><a href="http://pv.rehau.tesla.aristos.pw/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/tehnika-soedinenija-dlja-sistemy-raupiano-plus.html" ><span>Техника соединения для системы RAUPIANO Plus</span></a></li><li class="level1 nav-2-4 last"><a href="http://pv.rehau.tesla.aristos.pw/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/prinadlezhnosti.html" ><span>Принадлежности</span></a></li></ul><div class="hover-mark"></div></li><li class="level0 nav-3 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/jelektricheskii-teplyi-pol-solelec.html" ><span>SOLELEC</span></a><ul class="dropdown-menu level0"><li class="level1 nav-3-1 first"><a href="http://pv.rehau.tesla.aristos.pw/jelektricheskii-teplyi-pol-solelec/jelektricheskie-grejuschie-maty.html" ><span>Электрические греющие маты</span></a></li><li class="level1 nav-3-2"><a href="http://pv.rehau.tesla.aristos.pw/jelektricheskii-teplyi-pol-solelec/jelektricheskie-grejuschie-kabeli.html" ><span>Электрические греющие кабели</span></a></li><li class="level1 nav-3-3 last"><a href="http://pv.rehau.tesla.aristos.pw/jelektricheskii-teplyi-pol-solelec/termoreguljatory.html" ><span>Терморегуляторы</span></a></li></ul><div class="hover-mark"></div></li><li class="level0 nav-4 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau.html" ><span>Теплый пол</span></a><ul class="dropdown-menu level0"><li class="level1 nav-4-1 first parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby.html" ><span>Трубы и гофротрубы</span></a><ul class="dropdown-menu level1"><li class="level2 nav-4-1-1 first"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby/truba-rautherm-s.html" ><span>Труба RAUTHERM S</span></a></li><li class="level2 nav-4-1-2 last"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby/gofrotruby.html" ><span>Гофротрубы</span></a></li></ul><div class="hover-mark"></div></li><li class="level1 nav-4-2 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi.html" ><span>Надвижные гильзы и фитинги</span></a><ul class="dropdown-menu level1"><li class="level2 nav-4-2-1 first"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi/nadvizhnye-gil-zy.html" ><span>Надвижные гильзы</span></a></li><li class="level2 nav-4-2-2 last"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi/fitingi.html" ><span>Фитинги</span></a></li></ul><div class="hover-mark"></div></li><li class="level1 nav-4-3"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sposoby-kreplenija-trub-k-polu.html" ><span>Крепления труб к полу</span></a></li><li class="level1 nav-4-4"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-nastennogo-obogreva-ohlazhdenija.html" ><span>Системы укладки для настенного обогрева/охлаждения</span></a></li><li class="level1 nav-4-5"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-potolochnogo-obogreva-ohlazhdenija.html" ><span>Системы укладки для потолочного обогрева/охлаждения</span></a></li><li class="level1 nav-4-6 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory.html" ><span>Коллекторы</span></a><ul class="dropdown-menu level1"><li class="level2 nav-4-6-1 first"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory/raspredelitel-nye-kollektory-i-komplektujuschie.html" ><span>Распределительные коллекторы и комплектующие</span></a></li><li class="level2 nav-4-6-2 last"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory/raspredelitel-nye-shkafy.html" ><span>Распределительные шкафы</span></a></li></ul><div class="hover-mark"></div></li><li class="level1 nav-4-7 parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea.html" ><span>Системы автоматического регулирования </span></a><ul class="dropdown-menu level1"><li class="level2 nav-4-7-1 first"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-avtomaticheskogo-regulirovanija-ns-bus.html" ><span>Система автоматического регулирования НС BUS</span></a></li><li class="level2 nav-4-7-2"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-avtomaticheskogo-regulirovanija-nea.html" ><span>Система автоматического регулирования Nea</span></a></li><li class="level2 nav-4-7-3 last"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-regulirovanija.html" ><span>Монтаж системы регулирования</span></a></li></ul><div class="hover-mark"></div></li><li class="level1 nav-4-8"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-podogreva-i-ohlazhdenija-jadra-betonnyh-perekrytii.html" ><span>Система подогрева и охлаждения ядра бетонных перекрытий</span></a></li><li class="level1 nav-4-9"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/napol-noe-otoplenie-rehau-dlja-promyshlennyh-zdanij.html" ><span>Напольное отопление REHAU для промышленных зданий</span></a></li><li class="level1 nav-4-10"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-otoplenija-amortizirujuschih-polov-v-sportzalah.html" ><span>Системы для отопления амортизирующих полов в спортзалах</span></a></li><li class="level1 nav-4-11"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-obogreva-otkrytyh-ploschadok.html" ><span>Системы для обогрева открытых площадок</span></a></li><li class="level1 nav-4-12 last"><a href="http://pv.rehau.tesla.aristos.pw/obogrev-ohlazhdenie-poverhnostei-rehau/promyshlennyi-kollektor-rehau.html" ><span>Промышленный коллектор REHAU</span></a></li></ul><div class="hover-mark"></div></li><li class="level0 nav-5 last parent dropdown"><a href="http://pv.rehau.tesla.aristos.pw/montazhnyi-instrument-rautool.html" ><span>RAUTOOL</span></a><ul class="dropdown-menu level0"><li class="level1 nav-5-1 first"><a href="http://pv.rehau.tesla.aristos.pw/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5.html" ><span>Механический монтажный инструмент для надвижки гильз на диаметр 10 x 1,1/14 x 1,5</span></a></li><li class="level1 nav-5-2"><a href="http://pv.rehau.tesla.aristos.pw/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40.html" ><span>Монтажный инструмент для надвижки гильз на диаметр 16-40</span></a></li><li class="level1 nav-5-3"><a href="http://pv.rehau.tesla.aristos.pw/montazhnyi-instrument-rautool/komplektujuschie-i-zapchasti-k-instrumentu.html" ><span>Комплектующие и запчасти к инструменту</span></a></li><li class="level1 nav-5-4"><a href="http://pv.rehau.tesla.aristos.pw/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument.html" ><span>Дополнительный инструмент</span></a></li><li class="level1 nav-5-5"><a href="http://pv.rehau.tesla.aristos.pw/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40.html" ><span>Комплектующие и запасные части на диаметр 16-40</span></a></li><li class="level1 nav-5-6 last"><a href="http://pv.rehau.tesla.aristos.pw/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110.html" ><span>Гидравлический монтажный инструмент и комплектующие к нему на диаметр 40-110</span></a></li></ul><div class="hover-mark"></div></li>                            </ul>
                                <div class="header__links-mobile"></div>
                            </div>
                        </nav>
                        <!--/{TOPMENU_f991f2903db667fd0b48582dbf650b89}-->                </div>
                </div>
                <div class="col-md-2 col-xs-12">
                    <div class="header__login">
                        <div class="btn-group login">
                            <a class="btn dropdown-toggle btn-default btn-sm" href="http://pv.rehau.tesla.aristos.pw/customer/account/create/is_head_login_form/1/">
                                Вход    </a>
                        </div>                                            <div class="btn-group">
                            <a href="http://pv.rehau.tesla.aristos.pw/customer/account/create/" class="btn btn-default btn-sm user-link__desctop"> Регистрация</a>
                            <a href="http://pv.rehau.tesla.aristos.pw/customer/account/create/" class="user-icon__mobile"></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="header__cart">
                        <div  id="header-shopping-cart" class="header-cart" data-pjax-container="" data-pjax-timeout="700000" data-pjax-overlay-disable="true">                    <!--{CART_SIDEBAR_26a4aac46e7011c821315a40be796bf1}--><div class="btn-group">
                                <a href="http://pv.rehau.tesla.aristos.pw/checkout/cart/" class="btn">
                                    <span class="cart-icon"></span>
                                    <!--        <i class="fa fa-shopping-cart"></i>-->
                                    <span class="rehau-cart-count">Корзина: 0 шт.</span>
                                </a>
                            </div>
                            <!--/{CART_SIDEBAR_26a4aac46e7011c821315a40be796bf1}-->                    </div>                </div>
                    <div class="clearfix"></div>
                    <div class="header__search">
                        <form method="get" action="http://pv.rehau.tesla.aristos.pw/catalogsearch/result/" role="search" class="search-box margin-clear">
                            <div class="form-group has-feedback">
                                <input id="search" type="text" name="q" value="" class="form-control" placeholder="Поиск" required>
                                <button type="submit" title="Поиск" class="button"><span class="search__icon"></span><span class="mobile-search__text">Найти</span></button>
                                <div id="search_autocomplete" class="search-autocomplete"></div>
                            </div>
                        </form>                </div>
                </div>
                <div id="header-mobile__bottom" class="col-xs-12">
                    <div class="header-mobile__menu"></div>
                    <div class="header-mobile__search-button"></div>
                    <div class="header-mobile__search"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </header>    <div id="page-start"></div>
    <div class="container">
        <noscript>
            <div class="global-site-notice noscript">
                <div class="notice-inner">
                    <p>
                        <strong>Скорее всего в вашем браузере отключён JavaScript.</strong><br />
                        Вы должны включить JavaScript в вашем браузере, чтобы использовать все возможности этого сайта.                </p>
                </div>
            </div>
        </noscript>
        <!--{GLOBAL_MESSAGES_56d48a7e36b917874fd9de55488dca6e}--><!--/{GLOBAL_MESSAGES_56d48a7e36b917874fd9de55488dca6e}-->    </div>
    <section class="main-container">
        <div class="container">
            <div class="row">
                <div class="main col-md-12" role="main">

                    <div  id="specifications-container" data-pjax-container="" data-pjax-push-state data-pjax-timeout="700000">
                        <!--<script>
                            $(function () {
                                $('[data-toggle="popover"]').popover()
                            })
                        </script>-->
                        <div class="pv-hint-search">
                            <div class="row">
                                <div class="col-md-11 description">
                                    Если у вас есть список артикулов товаров (спецификация), которые необходимо заказать – скопируйте их и вставьте в поле под этим текстом
                                    с любым разделителем (например, через пробел или запятую) и нажмите ""Поиск"". Все товары будут выведены готовым к размещению заказа списком с
                                    возможностью изменения количества каждого из артикулов
                                </div>
                                <div class="col-md-1 text-center symbol">
                                    <button type="button"
                                            data-placement="top"
                                            class="btn btn-info"
                                            data-toggle="popover"
                                            title="Обратите внимание!"
                                            data-content="Вы можете скопировать список артикулов из таблицы MS Excel и вставить их в поле поиска. ">
                                        ?
                                    </button>
                                </div>
                            </div>
                        </div>

                        <form id="specifications-form" action="http://pv.rehau.tesla.aristos.pw/product/specifications/index/">
                            <label style="width: 100%">
                                <input class="form-control specifications-form__input" name="search" placeholder="Введите список артикулов через запятую" value="" required />
                                <button class="specifications-form__button" type="submit" name="search">Поиск</button>
                            </label>
                        </form>

                        <div id="specifications-result">


                        </div>

                    </div>                </div>
            </div>
        </div>
    </section>
    <div class="container">
    </div>
    <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
    <!-- ================ -->
    <footer id="footer" class="clearfix ">

        <!-- .footer start -->
        <!-- ================ -->
        <div class="footer">
            <div class="container">
                <div class="footer-inner">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Продукция</li>
                                <li class="footer__ul-menu_link">
                                    <a href="/obogrev-ohlazhdenie-poverhnostei-rehau.html" title="">Тёплый пол</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/truboprovodnaja-sistema-rautitan.html" title="">Отопление и Водопровод</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus.html" title="">Канализация</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/montazhnyi-instrument-rautool.html" title="">Всё для монтажа</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Информация для покупателей</li>
                                <li class="footer__ul-menu_link">
                                    <a href="/dostavka" title="">Доставка</a>
                                <li class="footer__ul-menu_link">
                                    <a href="/samovivoz" title="">Самовывоз</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/rehau-oplata" title="">Условия оплаты</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/vozvrat-tovara" title="">Гарантии и возврат</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Спецпредложения</li>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Специальные предложения</a>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Акции и скидки</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Подарочные сертификаты</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 pull-right col-xs-12">
                            <div class="mobile-position">
                                <a href="tel:+7 (495) 118-25-87" class="footer__phone">+7 (495) 118-25-87</a>
                                <div><a href="tel:8 (800) 350-09-61" class="footer__phone">8 (800) 350-09-61</a></div>
                            </div>
                            <div class="footer__work-time">
                                Пн-Пт: 09:00 - 21:00<br>
                                Сб-Вс: 09:00 - 19:00
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="footer__bottom">
                                <a href="/pravila-raboti" title="">Правила использования</a>
                                <a href="/o-magazine" title="">О магазине</a>
                                <a href="/contacts.html" title="">Контакты</a>
                                <span>© 2017 Официальный интернет-магазин REHAU</span>
                                <span class="footer__social">
            <a href="//vk.com/rehau_ru" target="_blank" title="" class="footer__social_vk"></a>
            <a href="//www.facebook.com/rehau.ru" target="_blank" title="" class="footer__social_fb"></a>
            <a href="//www.youtube.com/user/rehauea" target="_blank" title="" class="footer__social_youtube"></a>
            <a href="//www.instagram.com/rehaurussia/" target="_blank" title="" class="footer__social_instagram"></a>
        </span>
                            </div>
                        </div>                                    </div>
                </div>
            </div>
        </div>
        <!-- .footer end -->

    </footer>
    <!-- footer end --></div>




<div id="advancednewsletter-overlay" style="display:none"></div>
<div id="subscribe-please-wait" style="display:none;">
    <img src="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/images/opc-ajax-loader.gif" />&nbsp;Загружается...</div>
<div id="an-content" style="display:none"></div>
<script>var cartMessages={processing: "Обрабатывается",in_cart: "В корзину"}</script>
<script>var compareMessages={hide: "Скрыть",show: "Показать",_or_: " или "}</script><!--{COMPARETOOLBAR_50d2a66df7dba2232d863bf15354129f}-->    <section id="compare-toolbar" class="compare-toolbar">
    <div class="compare-wrap">
        <div class="bar">
            <div class="title">
                <span class="groups"></span>
            </div>
        </div>
        <div class="content">
            <div class="compare-items">
                <p>У вас нет товаров для сравнения.</p>
            </div>
        </div>
    </div>
</section>
<!--/{COMPARETOOLBAR_50d2a66df7dba2232d863bf15354129f}--><script type="text/javascript">
    var debugConfig;
    try {
        debugConfig = {
            "cache": {
                "config": {
                    "id": "config",
                    "cache_type": "Конфигурация",
                    "description": "System(config.xml, local.xml) and modules configuration files(config.xml).",
                    "tags": "CONFIG",
                    "status": 1
                },
                "layout": {
                    "id": "layout",
                    "cache_type": "Layouts",
                    "description": "Layout building instructions.",
                    "tags": "LAYOUT_GENERAL_CACHE_TAG",
                    "status": 1
                },
                "block_html": {
                    "id": "block_html",
                    "cache_type": "Blocks HTML output",
                    "description": "Page blocks HTML.",
                    "tags": "BLOCK_HTML",
                    "status": 1
                },
                "translate": {
                    "id": "translate",
                    "cache_type": "Переводы",
                    "description": "Translation files.",
                    "tags": "TRANSLATE",
                    "status": 1
                },
                "collections": {
                    "id": "collections",
                    "cache_type": "Collections Data",
                    "description": "Collection data files.",
                    "tags": "COLLECTION_DATA",
                    "status": 1
                },
                "eav": {
                    "id": "eav",
                    "cache_type": "EAV types and attributes",
                    "description": "Entity types declaration cache.",
                    "tags": "EAV",
                    "status": 1
                },
                "config_api": {
                    "id": "config_api",
                    "cache_type": "Web Services Configuration",
                    "description": "Web Services definition files (api.xml).",
                    "tags": "CONFIG_API",
                    "status": 1
                },
                "config_api2": {
                    "id": "config_api2",
                    "cache_type": "Web Services Configuration",
                    "description": "Web Services definition files (api2.xml).",
                    "tags": "CONFIG_API2",
                    "status": 1
                },
                "amshopby": {
                    "id": "amshopby",
                    "cache_type": "Amasty Improved Navigation",
                    "description": "Indexed data for filters and their options",
                    "tags": "AMSHOPBY",
                    "status": 1
                },
                "full_page": {
                    "id": "full_page",
                    "cache_type": "Page Cache",
                    "description": "Full page caching.",
                    "tags": "FPC",
                    "status": 1
                }
            }
        };
    } catch(e) {
        console.error('Ошибка получения конфигурации для панели отладки');
    }
</script>
<i id="aristos-debug-box-trigger" class="debug-box-trigger fa fa-2x fa-bug hvr-grow" title="Aristos Debug Panel"></i>
<nav id="aristos-debug-box" draggable="false">
    <header draggable="true">Aristos Debug Panel</header>
    <i id="debug-box-close" class="debug-box-trigger fa fa-times fa-2x hvr-grow" title="Закрыть панель"></i>
    <section class="content">
        <div class="panel-group" id="debug-panels" role="tablist" aria-multiselectable="true">
            <!--<div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Demo
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb2" type="checkbox"/>
                            <label class="tgl-btn" for="cb2"></label>
                            <span class="tgl-info">Config Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox" checked/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox"/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox"/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox"/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <section class="blocks"></section>
    </section>
    <i class="resize-handle fa fa-signal" aria-hidden="true"></i>
</nav>
<script type="text/javascript" >;(function ($, window, document, undefined) {$(document).ready(function () {jQuery(document).on('submit', true, function (event) {jQuery.pjax.submit(event, '#specifications-container', {"push":true,"replace":false,"pushRedirect":false,"replaceRedirect":true,"timeout":700000,"scrollTo":false,"skipOuterContainers":false,"fragment":"#specifications-container"});});});})(jQuery, window, document);</script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/jquery.validate-1.17.0.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/localization/messages_ru.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/bootstrap/bootstrap-3.3.7.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/fontawesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/awesomplete/awesomplete.css" />
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/css/compare.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/css/compare.print.css" media="print" />
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/compare.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/clipboard/clipboard.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.move.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.swipe.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/awesomplete/awesomplete.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/sprintf/sprintf.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/modernizr.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/waypoints/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/jquery.countTo.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/vide/jquery.vide.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/jquery.browser.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/SmoothScroll.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/template.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/region.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/owl-carousel/owl.carousel2.thumbs.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/evo/rehau/libraries/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/evo/rehau/layout.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/evo/rehau/addtocart-input.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/specifications.js"></script>

<!-- home -->
<script type="text/javascript" src="script.js"></script>

<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/debug.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.pjax.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/pjax.js"></script>
<link rel="stylesheet" href="//cdn.aristosgroup.ru/fonts/helios-cond/font.css" />
<style type="text/css">@media print { #djDebug {display:none;}}</style>
<script type="text/javascript">
    // <![CDATA[
    var DEBUG_TOOLBAR_MEDIA_URL = "http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/debug/";
    // ]]>
</script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/debug/js/toolbar.js"></script>
</body>
</html>
