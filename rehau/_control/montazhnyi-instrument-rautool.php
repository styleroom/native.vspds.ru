<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RAUTOOL</title>
    <meta name="description" content="Default Description" />
    <meta name="keywords" content="Magento, Varien, E-commerce" />
    <meta name="robots" content="INDEX,FOLLOW" />
    <link rel="icon" href="http://shop-rehau.ru/media/favicon/stores/21/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="http://shop-rehau.ru/media/favicon/stores/21/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery/jquery-migrate-3.0.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/7767d8d39ac1554d0ba861880a4ae5d6.css" />
    <link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/c8d288b2fdf3eda9dbad00c3a5c8da10.css" media="all" />
    <script type="text/javascript" src="http://shop-rehau.ru/media/js/05f05bee6fd727f82bac4dddcacf84cc.js"></script>
    <link rel="canonical" href="http://shop-rehau.ru/montazhnyi-instrument-rautool.html" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic" />
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Pacifico" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Serif" />

    <script type="text/javascript">
        //<![CDATA[
        optionalZipCountries = ["RU"];
        //]]>
    </script>

    <script>var baseMessages={please_wait: "Пожалуйста, ждите",successfully: "Успешно",request_error: "Ошибка запроса"}</script><script type="text/javascript">
        etCurrencyManagerJsConfig={"precision":0,"position":8,"display":2,"locale":"ru_RU","symbol":"\u20bd","input_admin":0,"excludecheckout":"0","cutzerodecimal":0,"cutzerodecimal_suffix":""}</script>

    <script>var evoMessages={next: "Вперед",prev: "Назад"}</script></head>

<!-- home -->
<link rel="stylesheet" href="style.css">

<body class=" catalog-category-view categorypath-montazhnyi-instrument-rautool category-montazhnyi-instrument-rautool">
<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div><div class="page-wrapper">

    <header class="pv_header_fullwidth">
        <div class="container header__container">
            <div class="row">
                <div class="col-xs-12" id="header-mobile">
                    <span class="header-mobile__shop-name"></span>
                    <a class="header-mobile__phone-number" href=""></a>
                </div>
                <div class="col-md-2 col-xs-7">
                    <div class="header__logo">
                        <a href="http://shop-rehau.ru/">
                            <img src="http://shop-rehau.ru/skin/frontend/evo/rehau/images/logo.png" alt="интернет-магазин Rehau">
                        </a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-5">
                    <div class="header__top">
                        <span class="header__shop-name" style="font-size: 0.875rem">Официальный интернет-магазин</span>
                        <a style="margin-left:20px; margin-right:7px; font-size: 1rem;" class="header__phone" href="tel:+7 (495) 118-25-87">+7 (495) 118-25-87</a>
                        <a style="margin-left:0; margin-right:0; font-size: 1rem;" class="header__phone" href="tel:8 (800) 350-09-61">8 (800) 350-09-61</a>
                        <span class="header__work-time">Пн-Пт: 09:00 - 21:00, Сб-Вс: 09:00 - 19:00</span>                </div>
                    <div class="header__region">

                        <div id="regionPopover">
                            <div class="btn-group dropdown region">
                                <button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown">
                                    <span id="icon"><i class="material-icons pr-10">&#xE569;</i></span> <span id="header_region_city">г. Москва</span>
                                </button>

                                <div class="popover-content dropdown-menu dropdown-menu-right dropdown-animation">
                                    <div class="region">


                                        <p class="header-place-descr">Возможно, мы неправильно определили ваше месторасположение, Вы можете
                                            указать его самостоятельно:</p>

                                        <div class="dropdown">
                                            <input class="header-region" type="text" placeholder="Ваше месторасположение" title="Ваше месторасположение" name="header_region_input" id="header_region_input" />
                                            <input type="hidden" name="header_region_input_kladr"/>
                                        </div>

                                        <ul class="defaultCity">
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7700000000000">г. Москва</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7800000000000">г. Санкт-Петербург</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6600000100000">г. Екатеринбург</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=5400000100000">г. Новосибирск</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=2300000100000">г. Краснодар</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6100000100000">г. Ростов-на-Дону</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=6300000100000">г. Самара</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=7400000100000">г. Челябинск</a>
                                            </li>
                                            <!--                -->                        <li><a class="selectedCity" rel="nofollow"
                                                                                                  href="/visitor_region/region/setRegion?kladr=2500000100000">г. Владивосток</a>
                                            </li>
                                        </ul>


                                        <ul class="defaultCity">
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=5200000100000">г. Нижний Новгород</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=2700000100000">г. Хабаровск</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=0200000100000">г. Уфа</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=1600000100000">г. Казань</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=2400000100000">г. Красноярск</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=5900000100000">г. Пермь</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=3600000100000">г. Воронеж</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=3400000100000">г. Волгоград</a>
                                            </li>
                                            <li><a class="selectedCity" rel="nofollow"
                                                   href="/visitor_region/region/setRegion?kladr=7200000100000">г. Тюмень</a>
                                            </li>
                                        </ul>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <script type="text/javascript">

                            function AristosVisitor_getSetRegionUrl(kladr, name) {
                                urlTemplate = "/visitor_region/region/setRegion?kladr=$1";
                                return urlTemplate.replace("$1", kladr).replace("$2", name);
                            }

                            function AristosVisitor_setRegionByUrl(url) {
                                var btn = jQuery("#regionPopover .popover-btn");
                                btn.trigger('click');
                                jQuery.ajax({
                                    type: "GET",
                                    url: url,
                                    beforeSend: function() {
                                        btn.addClass('loading');
                                    }
                                }).done(function (data) {
                                        btn.removeClass('loading');
                                        btn.html(data);
                                        setTimeout(function () {
                                            location.reload();
                                        }, 500);
                                    }
                                );
                            }

                        </script>                </div>
                    <div class="header__links">
                        <a href="/dostavka" class="header__links__a" title="">Доставка</a>
                        <a href="/rehau-oplata" class="header__links__a" title="">Оплата</a>
                        <a href="/samovivoz" class="header__links__a" title="">Самовывоз</a>
                        <a href="/contacts.html" class="header__links__a" title="">Контакты</a>
                        <a href="/product/specifications" class="header__links__a pink-color" title="">Заказать по артикулам</a>
                    </div>                <div class="header__menu">
                        <div class="navbar-btn">
                            <a href="#" class="open-panel">
                                <span class="mobile-menu__button" aria-hidden="true"></span>
                            </a>
                        </div>
                        <nav class="moving navbar navbar-default">
                            <div class="menu" id="navbar-collapse-2">
                                <div class="btn-group login">
                                    <a class="btn dropdown-toggle btn-default btn-sm" href="http://shop-rehau.ru/customer/account/create/is_head_login_form/1/">
                                        Вход    </a>
                                </div>
                                <!--        --><!--            <div class="register-link">-->
                                <!--                <a href="--><!--">-->
                                <!--                    --><!--</a>-->
                                <!--            </div>-->
                                <!--        -->
                                <ul class="nav navbar-nav">
                                    <li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan.html" class="dropdown-toggle">RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan.html" class="dropdown">Трубопроводная система RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan.html" >Универсальные трубы RAUTITAN </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/prinadlezhnosti-dlja-trub.html" >Принадлежности для труб </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan.html" class="dropdown">Универсальные фитинги RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/nadvizhnye-gil-zy.html" >Надвижные гильзы </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/trojniki.html" >Тройники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/soedinitel-nye-mufty-i-perehodniki.html" >Соединительные муфты и переходники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/ugol-niki.html" >Угольники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/varianty-podkljuchenija-iz-steny.html" >Варианты подключения из стены. Настенные угольники </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/kronshtei-ny.html" >Кронштейны </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/montazhnye-bloki.html" >Монтажные блоки </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/sistema-nastennyh-flancevyh-ugol-nikov.html" >Система настенных фланцевых угольников </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/raspredeliteli.html" >Распределители </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/universal-nye-fitingi-rautitan/komplektujuschie-dlja-fasonnyh-chastei.html" >Комплектующие для фасонных частей </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan.html" class="dropdown">Система радиаторной разводки RAUTITAN <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/fitingi-i-komplektujuschie-dlja-prisoedinenija-k-otopitel-nym-priboram.html" >Фитинги и комплектующие для присоединения к отопительным приборам </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/rez-bozazhimnye-soedinenija-sharovye-krany.html" >Резьбозажимные соединения / шаровые краны </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/odnoploskostnaja-krestovina.html" >Одноплоскостная крестовина </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/raspredelitel-nye-kollektory-komplektujuschie.html" >Распределительные коллекторы / комплектующие </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/prisoedinitel-nye-garnitury-sl.html" >Присоединительные гарнитуры SL </a></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/sistema-radiatornoi-razvodki-rautitan/sl-nabor-fitingov-rautitan-s-gil-zami.html" >SL – Набор фитингов RAUTITAN с гильзами </a></li></ul></li><li class=" "><a href="http://shop-rehau.ru/truboprovodnaja-sistema-rautitan/plintusnye-kanaly-rehau.html" >Плинтусные каналы REHAU </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus.html" class="dropdown-toggle">RAUPIANO Plus <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/kanalizacionnaja-truba-raupiano-plus.html" >Канализационная труба RAUPIANO Plus </a></li><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/fasonnye-chasti-dlja-kanalizacii.html" >Фасонные части для канализации </a></li><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/tehnika-soedinenija-dlja-sistemy-raupiano-plus.html" >Техника соединения для системы RAUPIANO Plus </a></li><li class=" "><a href="http://shop-rehau.ru/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus/prinadlezhnosti.html" >Принадлежности </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec.html" class="dropdown-toggle">SOLELEC <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec/jelektricheskie-grejuschie-maty.html" >Электрические греющие маты </a></li><li class=" "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec/jelektricheskie-grejuschie-kabeli.html" >Электрические греющие кабели </a></li><li class=" "><a href="http://shop-rehau.ru/jelektricheskii-teplyi-pol-solelec/termoreguljatory.html" >Терморегуляторы </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau.html" class="dropdown-toggle">Теплый пол <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby.html" class="dropdown">Трубы и гофротрубы <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby/truba-rautherm-s.html" >Труба RAUTHERM S </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/truby-i-gofrotruby/gofrotruby.html" >Гофротрубы </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi.html" class="dropdown">Надвижные гильзы и фитинги <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi/nadvizhnye-gil-zy.html" >Надвижные гильзы </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/nadvizhnye-gil-zy-i-fitingi/fitingi.html" >Фитинги </a></li></ul></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sposoby-kreplenija-trub-k-polu.html" >Крепления труб к полу </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-nastennogo-obogreva-ohlazhdenija.html" >Системы укладки для настенного обогрева/охлаждения </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-ukladki-dlja-potolochnogo-obogreva-ohlazhdenija.html" >Системы укладки для потолочного обогрева/охлаждения </a></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory.html" class="dropdown">Коллекторы <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory/raspredelitel-nye-kollektory-i-komplektujuschie.html" >Распределительные коллекторы и комплектующие </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/kollektory/raspredelitel-nye-shkafy.html" >Распределительные шкафы </a></li></ul></li><li class="dropdown-submenu "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea.html" class="dropdown">Системы автоматического регулирования  <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-avtomaticheskogo-regulirovanija-ns-bus.html" >Система автоматического регулирования НС BUS </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-avtomaticheskogo-regulirovanija-nea.html" >Система автоматического регулирования Nea </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-avtomaticheskogo-regulirovanija-nea/sistema-regulirovanija.html" >Монтаж системы регулирования </a></li></ul></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistema-podogreva-i-ohlazhdenija-jadra-betonnyh-perekrytii.html" >Система подогрева и охлаждения ядра бетонных перекрытий </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/napol-noe-otoplenie-rehau-dlja-promyshlennyh-zdanij.html" >Напольное отопление REHAU для промышленных зданий </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-otoplenija-amortizirujuschih-polov-v-sportzalah.html" >Системы для отопления амортизирующих полов в спортзалах </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/sistemy-dlja-obogreva-otkrytyh-ploschadok.html" >Системы для обогрева открытых площадок </a></li><li class=" "><a href="http://shop-rehau.ru/obogrev-ohlazhdenie-poverhnostei-rehau/promyshlennyi-kollektor-rehau.html" >Промышленный коллектор REHAU </a></li></ul></li><li class="dropdown-submenu"><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool.html" class="dropdown-toggle">RAUTOOL <span class="toggle"><i class="material-icons plus">&#xE313;</i><i class="material-icons minus">&#xE316;</i></span></a><ul class="dropdown-menu"><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5.html" >Механический монтажный инструмент для надвижки гильз на диаметр 10 x 1,1/14 x 1,5 </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40.html" >Монтажный инструмент для надвижки гильз на диаметр 16-40 </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapchasti-k-instrumentu.html" >Комплектующие и запчасти к инструменту </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument.html" >Дополнительный инструмент </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40.html" >Комплектующие и запасные части на диаметр 16-40 </a></li><li class=" "><a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110.html" >Гидравлический монтажный инструмент и комплектующие к нему на диаметр 40-110 </a></li></ul></li>                            </ul>
                                <div class="header__links-mobile"></div>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12">
                    <div class="header__login">
                        <div class="btn-group login">
                            <a class="btn dropdown-toggle btn-default btn-sm" href="http://shop-rehau.ru/customer/account/create/is_head_login_form/1/">
                                Вход    </a>
                        </div>                                            <div class="btn-group">
                            <a href="http://shop-rehau.ru/customer/account/create/" class="btn btn-default btn-sm user-link__desctop"> Регистрация</a>
                            <a href="http://shop-rehau.ru/customer/account/create/" class="user-icon__mobile"></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="header__cart">
                        <div  id="header-shopping-cart" class="header-cart" data-pjax-container="" data-pjax-timeout="7000" data-pjax-overlay-disable="true">                    <div class="btn-group">
                                <a href="http://shop-rehau.ru/checkout/cart/" class="btn">
                                    <span class="cart-icon"></span>
                                    <!--        <i class="fa fa-shopping-cart"></i>-->
                                    <span class="rehau-cart-count">Корзина: 0 шт.</span>
                                </a>
                            </div>
                        </div>                </div>
                    <div class="clearfix"></div>
                    <div class="header__search">
                        <form method="get" action="http://shop-rehau.ru/catalogsearch/result/" role="search" class="search-box margin-clear">
                            <div class="form-group has-feedback">
                                <input id="search" type="text" name="q" value="" class="form-control" placeholder="Поиск" required>
                                <button type="submit" title="Поиск" class="button"><span class="search__icon"></span><span class="mobile-search__text">Найти</span></button>
                                <div id="search_autocomplete" class="search-autocomplete"></div>
                            </div>
                        </form>                </div>
                </div>
                <div id="header-mobile__bottom" class="col-xs-12">
                    <div class="header-mobile__menu"></div>
                    <div class="header-mobile__search-button"></div>
                    <div class="header-mobile__search"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </header>    <div id="page-start"></div>
    <div  id="catalog" data-pjax-container="" data-pjax-push-state data-pjax-timeout="7000" fragment="false" only=".filters, .toolbar">    <div class="container">
            <noscript>
                <div class="global-site-notice noscript">
                    <div class="notice-inner">
                        <p>
                            <strong>Скорее всего в вашем браузере отключён JavaScript.</strong><br />
                            Вы должны включить JavaScript в вашем браузере, чтобы использовать все возможности этого сайта.                </p>
                    </div>
                </div>
            </noscript>
        </div>
        <section class="main-container">
            <div class="breadcrumb-container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li class="home ">
                                    <i class="fa fa-home pr-10"></i>
                                    <a class="link-dark" href="http://shop-rehau.ru/"
                                       title="Перейти на главную страницу">Главная</a>
                                </li>
                                <li class="category743 active">
                                    RAUTOOL                                                            </li>
                            </ol>
                            <h2 class="breadcrumbs__page-title">RAUTOOL</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="main col-md-12" role="main">




                        <!-- Modal rehauAlertSaved -->
                        <div class="modal fade alert-saved" id="rehauAlertSaved" tabindex="-1" role="dialog" aria-labelledby="rehauAlertSavedLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Поздравляем!</strong> Подписка на оповещение сохранена!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h1 class="page-title">RAUTOOL</h1>



                        <script>
                            jQuery(document).on('ready', function () {
                                jQuery('.nav-pills a').on('click',function () {
                                    var tabFilter = jQuery(this).data('filter');
                                    jQuery('.tab-content ').isotope({ filter: tabFilter });
                                });
                            });
                        </script>


                        <div class="toolbar">
                            <!-- Табы подкатегорий -->
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="mobile-subcategories-button">Выбрать подкатегорию <span class="subcategories-button__icon"></span></span>
                                    <ul class="nav nav-pills subcategory-tabs" role="tablist">
                                        <li>
                                            <a href="#" class="show-all" role="tab" data-toggle="tab" data-filter="*">Все</a>
                                        </li>
                                        <li>
                                            <a class="" href="#pill--0" role="tab" data-toggle="tab" data-filter=".tab-num-0">Гидравлический монтажный инструмент и комплектующие к нему на диаметр 40-110</a>
                                        </li>
                                        <li>
                                            <a class="" href="#pill--1" role="tab" data-toggle="tab" data-filter=".tab-num-1">Монтажный инструмент для надвижки гильз на диаметр 16-40</a>
                                        </li>
                                        <li>
                                            <a class="" href="#pill--2" role="tab" data-toggle="tab" data-filter=".tab-num-2">Комплектующие и запасные части на диаметр 16-40</a>
                                        </li>
                                        <li>
                                            <a class="" href="#pill--3" role="tab" data-toggle="tab" data-filter=".tab-num-3">Механический монтажный инструмент для надвижки гильз на диаметр 10 x 1,1/14 x 1,5</a>
                                        </li>
                                        <li>
                                            <a class="" href="#pill--4" role="tab" data-toggle="tab" data-filter=".tab-num-4">Комплектующие и запчасти к инструменту</a>
                                        </li>
                                        <li>
                                            <a class="" href="#pill--5" role="tab" data-toggle="tab" data-filter=".tab-num-5">Дополнительный инструмент</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Тулбар -->
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- Сортировка продуктов -->
                                    <div class="product-sort__menu">
                                        <span class="product-sort__text">Тип сортировки:</span>
                                        <span class="product-sort__item default">
                    По умолчанию                    <span class="product-sort__button"></span>
                </span>
                                        <ul class="product-sort__list">
                                            <li class="product-sort__item">
                                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool.html?dir=asc&order=position" class="product-sort__item-link">Позиция</a>
                                            </li>
                                            <li class="product-sort__item">
                                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool.html?dir=asc&order=name" class="product-sort__item-link">Название</a>
                                            </li>
                                            <li class="product-sort__item">
                                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool.html?dir=asc&order=price" class="product-sort__item-link">Цена</a>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <div class="col-md-6 pull-right toolbar-view">
                                    <!-- Переключение вида отображения продуктов -->
                                    <!--                    <label>--><!--:</label>-->
                                    <span class="view__icon_active" title="Сетка">
                                <i class="view__icon-tile"></i>
                            </span>
                                    <span>
                                <a class="view__icon" href="http://shop-rehau.ru/montazhnyi-instrument-rautool.html?mode=list" title="Список">
                                    <i class="view__icon-list"></i>
                                </a>
                            </span>
                                </div>
                            </div>
                        </div>


                        <div class="row rehau-grid-mode">
                            <!-- Tab panes Grid Mode -->
                            <div class="tab-content clear-style category-products">
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10874"
                                                        data-category-id="784"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10874/"
                                                        id="compare-10874"
                                                    />
                                                    <label for="compare-10874">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 13149851001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-g2-jelektrogidravlicheskij-na-diametry-50-63-i-40-110.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-151643_1.jpg" alt="RAUTOOL G2, электрогидравлический на диаметры 50-63 и 40-110">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-151643_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-g2-jelektrogidravlicheskij-na-diametry-50-63-i-40-110.html">RAUTOOL G2, электрогидравлический на диаметры 50-63 и 40-110</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10874/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10874/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10480"
                                                        data-category-id="784"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10480/"
                                                        id="compare-10480"
                                                    />
                                                    <label for="compare-10480">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 11378251001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/smennyj-komplekt-75-110-dlja-gidravlicheskogo-instrumenta-g1-h-g1.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495714.jpg" alt="Сменный комплект 75-110 для гидравлического инструмента G1, H/G1">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495714.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/smennyj-komplekt-75-110-dlja-gidravlicheskogo-instrumenta-g1-h-g1.html">Сменный комплект 75-110 для гидравлического инструмента G1, H/G1</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10480/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10480/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10875"
                                                        data-category-id="784"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10875/"
                                                        id="compare-10875"
                                                    />
                                                    <label for="compare-10875">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 13149871001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-h-g1-mehaniko-gidravlicheskij-na-diametry-50-63.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/placeholder/stores/21/no_photo_1.jpg" alt="RAUTOOL H/G1, механико-гидравлический на диаметры 50-63">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/placeholder/stores/21/no_photo_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-h-g1-mehaniko-gidravlicheskij-na-diametry-50-63.html">RAUTOOL H/G1, механико-гидравлический на диаметры 50-63</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10875/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10875/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9957"
                                                        data-category-id="784"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9957/"
                                                        id="compare-9957"
                                                    />
                                                    <label for="compare-9957">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12018021001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/zapressovochnye-tiski-g1-na-diametry-40-110-dlja-g2-h-g1-5.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-3998_1.jpg" alt="Запрессовочные тиски G1 на диаметры 40-110 для G2, H/G1">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-3998_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/zapressovochnye-tiski-g1-na-diametry-40-110-dlja-g2-h-g1-5.html">Запрессовочные тиски G1 на диаметры 40-110 для G2, H/G1</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9957/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9957/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10444"
                                                        data-category-id="784"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10444/"
                                                        id="compare-10444"
                                                    />
                                                    <label for="compare-10444">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 11399011001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rasshiritel-nye-nasadki-g1-dlja-rautool-g2-h-g1-8.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4117_13.jpg" alt="Расширительные насадки G1 для RAUTOOL G2, H/G1">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4117_13.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rasshiritel-nye-nasadki-g1-dlja-rautool-g2-h-g1-8.html">Расширительные насадки G1 для RAUTOOL G2, H/G1</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10444/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10444/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10119"
                                                        data-category-id="784"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10119/"
                                                        id="compare-10119"
                                                    />
                                                    <label for="compare-10119">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 13152391001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/nozhnicy-truboreznye-do-dh-125-2.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-537347_1.jpg" alt="Ножницы труборезные до DH 125">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-537347_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/nozhnicy-truboreznye-do-dh-125-2.html">Ножницы труборезные до DH 125</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10119/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10119/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10872"
                                                        data-category-id="780"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10872/"
                                                        id="compare-10872"
                                                    />
                                                    <label for="compare-10872">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12174781001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-a-light2-kombi-kombinirovannyj-akkumuljatornyj.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-526910_1.jpg" alt="RAUTOOL A-light2 Kombi, комбинированный аккумуляторный">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-526910_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-a-light2-kombi-kombinirovannyj-akkumuljatornyj.html">RAUTOOL A-light2 Kombi, комбинированный аккумуляторный</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10872/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10872/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10876"
                                                        data-category-id="780"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10876/"
                                                        id="compare-10876"
                                                    />
                                                    <label for="compare-10876">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12024841001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-h2-mehaniko-gidravlicheskij.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-567475.jpg" alt="RAUTOOL H2, механико-гидравлический">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-567475.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-h2-mehaniko-gidravlicheskij.html">RAUTOOL H2, механико-гидравлический</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10876/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10876/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10879"
                                                        data-category-id="780"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10879/"
                                                        id="compare-10879"
                                                    />
                                                    <label for="compare-10879">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 11377641005</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-m1-mehanicheskij.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4007_1.jpg" alt="RAUTOOL M1, механический">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4007_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-m1-mehanicheskij.html">RAUTOOL M1, механический</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10879/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10879/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10881"
                                                        data-category-id="780"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10881/"
                                                        id="compare-10881"
                                                    />
                                                    <label for="compare-10881">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12168201001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-xpand-akkumuljatornyj-gidravlicheskij-jekspander-16-40-sistemy-qc-1.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-493902_1.jpg" alt="RAUTOOL Xpand, аккумуляторный гидравлический экспандер 16-40 системы QC ">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-493902_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-xpand-akkumuljatornyj-gidravlicheskij-jekspander-16-40-sistemy-qc-1.html">RAUTOOL Xpand, аккумуляторный гидравлический экспандер 16-40 системы QC </a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10881/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10881/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10034"
                                                        data-category-id="780"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10034/"
                                                        id="compare-10034"
                                                    />
                                                    <label for="compare-10034">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12141761001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/mehanicheskij-jekspander-16-32-sistemy-qc.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-443809_1.jpg" alt="Механический экспандер 16-32 системы QC">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-443809_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/mehanicheskij-jekspander-16-32-sistemy-qc.html">Механический экспандер 16-32 системы QC</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10034/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10034/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9938"
                                                        data-category-id="780"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9938/"
                                                        id="compare-9938"
                                                    />
                                                    <label for="compare-9938">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12036191001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/zapasnoj-jelektroakkumuljator-dlja-a-light2-az-ez-g2-xpand-kombi.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84471_1.jpg" alt="RAUTOOL A-light2, аккумуляторный гидравлический">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84471_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/zapasnoj-jelektroakkumuljator-dlja-a-light2-az-ez-g2-xpand-kombi.html">RAUTOOL A-light2, аккумуляторный гидравлический</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9938/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9938/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10430"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10430/"
                                                        id="compare-10430"
                                                    />
                                                    <label for="compare-10430">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 11373441001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-m1-1.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-98863856.jpg" alt="Расширительные насадки 40 для М1">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-98863856.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-m1-1.html">Расширительные насадки 40 для М1</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10430/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10430/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10413"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10413/"
                                                        id="compare-10413"
                                                    />
                                                    <label for="compare-10413">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12476441001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-40-h-6-0-stabil-dlja-n2-a-light-a-light2-az.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4105_1.jpg" alt="Расширительная насадка 40 х 6,0 stabil для Н2, А-light, A-light2, АЗ">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4105_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-40-h-6-0-stabil-dlja-n2-a-light-a-light2-az.html">Расширительная насадка 40 х 6,0 stabil для Н2, А-light, A-light2, АЗ</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10413/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10413/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10435"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10435/"
                                                        id="compare-10435"
                                                    />
                                                    <label for="compare-10435">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12093941001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-xpand-2.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-341666.jpg" alt="Расширительные насадки 40 для Xpand">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-341666.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-xpand-2.html">Расширительные насадки 40 для Xpand</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10435/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10435/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10432"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10432/"
                                                        id="compare-10432"
                                                    />
                                                    <label for="compare-10432">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12446111001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-n2-a-light2-az-1.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4106.jpg" alt="Расширительные насадки 40 для Н2, A-light2, АЗ">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4106.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-n2-a-light2-az-1.html">Расширительные насадки 40 для Н2, A-light2, АЗ</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10432/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10432/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9951"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9951/"
                                                        id="compare-9951"
                                                    />
                                                    <label for="compare-9951">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12051331001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3-na-3-diametra.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-276305_1.jpg" alt="Запрессовочные тиски для Н2, A-light2, Kombi, A3 на 3 диаметра">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-276305_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3-na-3-diametra.html">Запрессовочные тиски для Н2, A-light2, Kombi, A3 на 3 диаметра</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9951/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9951/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9950"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9950/"
                                                        id="compare-9950"
                                                    />
                                                    <label for="compare-9950">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12018011001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4013.jpg" alt="Запрессовочные тиски для Н2, A-light2, Kombi, A3">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4013.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3.html">Запрессовочные тиски для Н2, A-light2, Kombi, A3</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9950/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9950/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10416"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10416/"
                                                        id="compare-10416"
                                                    />
                                                    <label for="compare-10416">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12484111001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-20-dlja-n2-a-light2-az-1.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-361051.jpg" alt="Расширительные насадки 16-20 для Н2, A-light2, АЗ">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-361051.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-20-dlja-n2-a-light2-az-1.html">Расширительные насадки 16-20 для Н2, A-light2, АЗ</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10416/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10416/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9946"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9946/"
                                                        id="compare-9946"
                                                    />
                                                    <label for="compare-9946">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12017981001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-m1-3.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4002.jpg" alt="Запрессовочные тиски для М1">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4002.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-m1-3.html">Запрессовочные тиски для М1</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9946/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9946/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10446"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10446/"
                                                        id="compare-10446"
                                                    />
                                                    <label for="compare-10446">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12174691001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nyj-nakonechnik-25-32-sistemy-qc-dlja-gidravlicheskogo-instrumenta-n2-a-light2-az.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495698.jpg" alt="Расширительный наконечник 25/32 системы QC для гидравлического инструмента Н2, A-light2, АЗ">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495698.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nyj-nakonechnik-25-32-sistemy-qc-dlja-gidravlicheskogo-instrumenta-n2-a-light2-az.html">Расширительный наконечник 25/32 системы QC для гидравлического инструмента Н2, A-light2, АЗ</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10446/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10446/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9949"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9949/"
                                                        id="compare-9949"
                                                    />
                                                    <label for="compare-9949">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12590491002</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light-a-light2-a2-az-kombi-2.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4000_4.jpg" alt="Запрессовочные тиски для Н2, A-light, A-light2, А2, АЗ, Kombi">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4000_4.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light-a-light2-a2-az-kombi-2.html">Запрессовочные тиски для Н2, A-light, A-light2, А2, АЗ, Kombi</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9949/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9949/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10428"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10428/"
                                                        id="compare-10428"
                                                    />
                                                    <label for="compare-10428">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12446011001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-32-sistemy-ro-11.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4110_5.jpg" alt="Расширительные насадки 16-32 системы RO">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4110_5.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-32-sistemy-ro-11.html">Расширительные насадки 16-32 системы RO</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10428/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10428/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9854"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9854/"
                                                        id="compare-9854"
                                                    />
                                                    <label for="compare-9854">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12141751001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/bystros-emnye-rasshiritel-nye-nasadki-16-32-sistemy-qc-11.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-442307_4.jpg" alt="Быстросъемные расширительные насадки 16-32 системы QC">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-442307_4.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/bystros-emnye-rasshiritel-nye-nasadki-16-32-sistemy-qc-11.html">Быстросъемные расширительные насадки 16-32 системы QC</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9854/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9854/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10414"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10414/"
                                                        id="compare-10414"
                                                    />
                                                    <label for="compare-10414">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12687641001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-dlja-razval-covki-prisoedinitel-nyh-trubok-iz-medi-i-nerzhavejuschej-stali-15-h-1-0-mm.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4111_1.jpg" alt="Расширительная насадка RO для медных и стальных трубок">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4111_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-dlja-razval-covki-prisoedinitel-nyh-trubok-iz-medi-i-nerzhavejuschej-stali-15-h-1-0-mm.html">Расширительная насадка RO для медных и стальных трубок</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10414/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10414/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10368"
                                                        data-category-id="783"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10368/"
                                                        id="compare-10368"
                                                    />
                                                    <label for="compare-10368">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12474941001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/pruzhinnyj-vkladysh-dlja-vygiba-trub-rautitan-stabil-1.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-271722_2.jpg" alt="Пружинный вкладыш для выгиба труб RAUTITAN stabil">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-271722_2.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/pruzhinnyj-vkladysh-dlja-vygiba-trub-rautitan-stabil-1.html">Пружинный вкладыш для выгиба труб RAUTITAN stabil</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10368/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10368/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-3">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10878"
                                                        data-category-id="779"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10878/"
                                                        id="compare-10878"
                                                    />
                                                    <label for="compare-10878">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12446211001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k14-x-1-5-mehanicheskij.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_2.jpg" alt="RAUTOOL K14 x 1,5, механический">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_2.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k14-x-1-5-mehanicheskij.html">RAUTOOL K14 x 1,5, механический</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10878/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10878/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-3">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10877"
                                                        data-category-id="779"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10877/"
                                                        id="compare-10877"
                                                    />
                                                    <label for="compare-10877">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12283961001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k10-x-1-1-mehanicheskij.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_1.jpg" alt="RAUTOOL K10 x 1,1, механический">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_1.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k10-x-1-1-mehanicheskij.html">RAUTOOL K10 x 1,1, механический</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10877/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10877/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-4">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="9958"
                                                        data-category-id="781"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9958/"
                                                        id="compare-9958"
                                                    />
                                                    <label for="compare-9958">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 12036091001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapchasti-k-instrumentu/zarjadnoe-ustrojstvo-k-instrumentu-a-light2-az-ez-g2-xpand-kombi.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84470.jpg" alt="Зарядное устройство к инструменту A-light2 / АЗ /ЕЗ /G2 /Xpand / Kombi">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84470.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapchasti-k-instrumentu/zarjadnoe-ustrojstvo-k-instrumentu-a-light2-az-ez-g2-xpand-kombi.html">Зарядное устройство к инструменту A-light2 / АЗ /ЕЗ /G2 /Xpand / Kombi</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9958/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9958/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-5">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10116"
                                                        data-category-id="782"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10116/"
                                                        id="compare-10116"
                                                    />
                                                    <label for="compare-10116">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 13152431001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/nozhnicy-truboreznye-dlja-rezki-polimernyh-metallopolimernyh-trub-do-dn-40-mm-4.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-425435_2.jpg" alt="Ножницы труборезные для резки полимерных/металлополимерных труб до Dн 40 мм">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-425435_2.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/nozhnicy-truboreznye-dlja-rezki-polimernyh-metallopolimernyh-trub-do-dn-40-mm-4.html">Ножницы труборезные для резки полимерных/металлополимерных труб до Dн 40 мм</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10116/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10116/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-5">
                                    <div class="listing-item white-bg bordered">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-12">
                                                <div class="compare-link"><input
                                                        type="checkbox"
                                                        data-product-id="10473"
                                                        data-category-id="782"
                                                        name="compare"
                                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10473/"
                                                        id="compare-10473"
                                                    />
                                                    <label for="compare-10473">
                                                        <span>Сравнить</span>
                                                    </label></div>
                                                <div class="sku">Арт. 11372341001</div>
                                                <div class="overlay-container">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/ruchnoj-fiksatornyj-zazhim-dlja-trub-16-17-20.html">
                                                        <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-147806.jpg" alt="Ручной фиксаторный зажим для труб 16/17/20">
                                                    </a>
                                                    <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-147806.jpg"><i class="fa fa-search-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-md-12 flex-align">
                                                <h3 class="card__product-title">
                                                    <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/ruchnoj-fiksatornyj-zazhim-dlja-trub-16-17-20.html">Ручной фиксаторный зажим для труб 16/17/20</a>
                                                    <!--                                            -->                                        <span class="short-descr"></span>
                                                    <!--                                            -->                                    </h3>

                                                <div class="body">
                                                    <div class="status">
                                                        <div class="out-of-stock">
                                                            <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                                    </div>
                                                    <div class="rating"></div>
                                                    <div class="elements-list clearfix">

                                                        <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10473/form_key/H2XZoSKY4QfYAa9d/" method="post"
                                                              id="product_addtocart_form">
                                                            <input name="form_key" type="hidden" value="H2XZoSKY4QfYAa9d" />
                                                            <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10473/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                                Нет в наличии        </a>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </section>
    </div>    <div class="container">
        <style>
            .home-info .col-sm-3 {width: 33.333%;}
            @media (max-width: 767px) {.home-info .col-sm-3 {width: 100% !important; }}
        </style>
        <div class="home-info row">
            <div class="block-title">
                <h2>Преимущества официального магазина</h2>
            </div>
            <div class="col-sm-3">
                <a href="/o-magazine">
                    <img src="http://shop-rehau.ru/media/wysiwyg/rehau/home/rh_icon_1_garant.png" alt="" /><!-- home_features-icon.png -->
                    <span>
                <h3>Фирменная гарантия</h3>
                <p>На всю продукцию в течение 10 лет!</p>
            </span>
                </a>
            </div>
            <!--
        <div class="col-sm-3">
                <a href="/dostavka">
                    <img src="http://shop-rehau.ru/media/wysiwyg/rehau/home/home_features-icon.png" alt="" />
                    <span>
                        <h3>бесплатная доставка</h3>
                        <p>При сумме заказа от 10000 рублей.</p>
                    </span>
                </a>
            </div> -->
            <div class="col-sm-3">
                <a href="/vozvrat-tovara">
                    <img src="http://shop-rehau.ru/media/wysiwyg/rehau/home/rh_icon_2_problem.png" alt="" />
                    <span>
                <h3>возврат без проблем</h3>
                <p>По любой причине в течение 7 дней.</p>
            </span>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="/samovivoz">
                    <img src="http://shop-rehau.ru/media/wysiwyg/rehau/home/rh_icon_3_vyvoz.png" alt="" />
                    <span>
                <h3>самовывоз</h3>
                <p>Пункты самовывоза в Москве</p>
            </span>
                </a>
            </div>
        </div></div>
    <!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
    <!-- ================ -->
    <footer id="footer" class="clearfix ">

        <!-- .footer start -->
        <!-- ================ -->
        <div class="footer">
            <div class="container">
                <div class="footer-inner">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Продукция</li>
                                <li class="footer__ul-menu_link">
                                    <a href="/obogrev-ohlazhdenie-poverhnostei-rehau.html" title="">Тёплый пол</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/truboprovodnaja-sistema-rautitan.html" title="">Отопление и Водопровод</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus.html" title="">Канализация</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/montazhnyi-instrument-rautool.html" title="">Всё для монтажа</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Информация для покупателей</li>
                                <li class="footer__ul-menu_link">
                                    <a href="/dostavka" title="">Доставка</a>
                                <li class="footer__ul-menu_link">
                                    <a href="/samovivoz" title="">Самовывоз</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/rehau-oplata" title="">Условия оплаты</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="/vozvrat-tovara" title="">Гарантии и возврат</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <ul class="footer__ul-menu">
                                <li class="footer__ul-menu_name">Спецпредложения</li>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Специальные предложения</a>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Акции и скидки</a>
                                </li>
                                <li class="footer__ul-menu_link">
                                    <a href="#" title="">Подарочные сертификаты</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 pull-right col-xs-12">
                            <div class="mobile-position">
                                <a href="tel:+7 (495) 118-25-87" class="footer__phone">+7 (495) 118-25-87</a>
                                <div><a href="tel:8 (800) 350-09-61" class="footer__phone">8 (800) 350-09-61</a></div>
                            </div>
                            <div class="footer__work-time">
                                Пн-Пт: 09:00 - 21:00<br>
                                Сб-Вс: 09:00 - 19:00
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="footer__bottom">
                                <a href="/pravila-raboti" title="">Правила использования</a>
                                <a href="/o-magazine" title="">О магазине</a>
                                <a href="/contacts.html" title="">Контакты</a>
                                <span>© 2017 Официальный интернет-магазин REHAU</span>
                                <span class="footer__social">
            <a href="//vk.com/rehau_ru" target="_blank" title="" class="footer__social_vk"></a>
            <a href="//www.facebook.com/rehau.ru" target="_blank" title="" class="footer__social_fb"></a>
            <a href="//www.youtube.com/user/rehauea" target="_blank" title="" class="footer__social_youtube"></a>
            <a href="//www.instagram.com/rehaurussia/" target="_blank" title="" class="footer__social_instagram"></a>
        </span>
                            </div>
                        </div>                                    </div>
                </div>
            </div>
        </div>
        <!-- .footer end -->

    </footer>
    <!-- footer end --></div>




<div id="advancednewsletter-overlay" style="display:none"></div>
<div id="subscribe-please-wait" style="display:none;">
    <img src="http://shop-rehau.ru/skin/frontend/base/default/images/opc-ajax-loader.gif" />&nbsp;Загружается...</div>
<div id="an-content" style="display:none"></div>
<script>var cartMessages={processing: "Обрабатывается",in_cart: "В корзину"}</script>
<script>var compareMessages={hide: "Скрыть",show: "Показать",_or_: " или "}</script>    <section id="compare-toolbar" class="compare-toolbar">
    <div class="compare-wrap">
        <div class="bar">
            <div class="title">
                <span class="groups"></span>
            </div>
        </div>
        <div class="content">
            <div class="compare-items">
                <p>У вас нет товаров для сравнения.</p>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" >;(function ($, window, document, undefined) {$(document).ready(function () {jQuery(document).pjax("#catalog .filters a, #catalog .toolbar a", '#catalog', {"push":true,"replace":false,"pushRedirect":false,"replaceRedirect":true,"timeout":7000,"scrollTo":false,"skipOuterContainers":false});jQuery(document).on('submit', "#catalog .filters form, #catalog .toolbar form", function (event) {jQuery.pjax.submit(event, '#catalog', {"push":true,"replace":false,"pushRedirect":false,"replaceRedirect":true,"timeout":7000,"scrollTo":false,"skipOuterContainers":false});});});})(jQuery, window, document);</script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/jquery.validate-1.17.0.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/localization/messages_ru.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/bootstrap/bootstrap-3.3.7.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/d6260fca0e9256f91ddeabde812ff4dc.css" />
<link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/a09b88010e959de4f0a025bb826a8d86.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://shop-rehau.ru/media/css/45cf0c0049726c85908a930f13cf3c7e.css" media="print" />
<script type="text/javascript" src="http://shop-rehau.ru/media/js/a2a25ad8d541a5cf0542469530b495ff.js"></script>

<!-- home -->
<script type="text/javascript" src="script.js"></script>

<link rel="stylesheet" href="//cdn.aristosgroup.ru/fonts/helios-cond/font.css" />
<script type="text/javascript">
    var product = {
        category : []
    };

    product.category.push('RAUTOOL');
</script>
</body>
</html>
