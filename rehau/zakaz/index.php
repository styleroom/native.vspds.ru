<?php require_once '_top.php'; ?>

<style>
    .pv-hint-search .symbol button {
        background-color: #bbb;
    }
    .wrap-search {}
    .wrap-search .col-md-11 {
        padding-right: 0;
    }
    .wrap-search .col-md-1 {
        padding-left: 0;
    }
    .wrap-search input[type="text"] {
        width: 100%;
        padding-right: 15px;
    }
    .wrap-search button[type="submit"] {
        width: 100% !important;
    }
    @media (max-width: 762px) {
        .wrap-search .col-md-11 {
            padding-right: 15px;
        }
        .wrap-search .col-md-1 {
            padding-left: 15px;
        }
    }
</style>

<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="main col-md-12" role="main">

                <div  id="specifications-container" data-pjax-container="" data-pjax-push-state data-pjax-timeout="700000"><script>
                        $(function () {
                            $('[data-toggle="popover"]').popover()
                        })
                    </script>
                    <div class="pv-hint-search">
                        <div class="row">
                            <div class="col-md-11 description">
                                Если у вас есть список артикулов товаров (спецификация), которые необходимо заказать – скопируйте их и вставьте в поле под этим текстом
                                с любым разделителем (например, через пробел или запятую) и нажмите ""Поиск"". Все товары будут выведены готовым к размещению заказа списком с
                                возможностью изменения количества каждого из артикулов
                            </div>
                            <div class="col-md-1 text-center symbol">
                                <button type="button"
                                        data-placement="top"
                                        class="btn btn-info"
                                        data-toggle="popover"
                                        title="Обратите внимание!"
                                        data-content="Вы можете скопировать список артикулов из таблицы MS Excel и вставить их в поле поиска. ">
                                    ?
                                </button>
                            </div>
                        </div>
                    </div>

                    <form id="specifications-form" action="http://pv.rehau.tesla.aristos.pw/product/specifications/index/">
                        <!--<div class="row wrap-search">
                            <div class="col-md-11">
                                <input type="text" class="form-control specifications-form__input" name="search" placeholder="Введите список артикулов через запятую" value="" required />
                            </div>
                            <div class="col-md-1">
                                <button class="specifications-form__button" type="submit" name="search">Поиск</button>
                            </div>
                        </div>-->
                        <label style="width: 100%">
                            <input class="form-control specifications-form__input" name="search" placeholder="Введите список артикулов через запятую" value="" required />
                            <button class="specifications-form__button" type="submit" name="search">Поиск</button>
                        </label>
                    </form>

                    <div id="specifications-result">


                    </div>

                </div>                </div>
        </div>
    </div>
</section>

<?php require_once '_bottom.php'; ?>