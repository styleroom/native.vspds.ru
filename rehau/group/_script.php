<script>
    jQuery(document).on('ready', function () {

        var tabFilterLocalStorage = localStorage.getItem('tabFilter');
        if (tabFilterLocalStorage) {
            jQuery('a[data-filter="'+tabFilterLocalStorage+'"]').parents('li').addClass('active');
            jQuery('.tab-content ').isotope({ filter: tabFilterLocalStorage });
        }

        jQuery('.nav-pills a').on('click',function () {
            var tabFilter = jQuery(this).data('filter');
            localStorage.setItem('tabFilter',tabFilter);
            jQuery('.tab-content ').isotope({ filter: tabFilter });
        });

//        jQuery('.tab-content').isotope().on( 'layoutComplete', function( event, laidOutItems ) {
//            jQuery('.masonry-grid-item:visible').each(function (index) {
//                var element = jQuery(this);
//                if ((index + 1) % 4 === 0) {
//                    element.css('margin-right','0px');
//                }
//            });
//        });

    });
</script>