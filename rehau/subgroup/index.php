<?php require_once '_top.php'; ?>
<?php
// row rehau-grid-mode subcategories category-items-list masonry-grid-fitrows clearfix
?>
<style>
    .pv_header_fullwidth {
        width: 100% !important;
        background-color: white;
        position: static !important;
        transform: none !important;
        top: 0 !important;
        right: 0 !important;
    }
    .header__container {
        box-shadow: none !important;
    }
    .subcategory {
        margin-left: 0 !important;
        overflow: hidden;
        width: 102% !important;
    }
    .subcategory .masonry-grid-item,
    .subcategory .masonry-grid-item:nth-child(4n) {
        margin-right: 20px !important;
    }
    @media (max-width: 762px) {
        .subcategory {
            width: 100% !important;
        }
    }
</style>
    <div class="row subcategory category-items-list masonry-grid-fitrows clearfix">

        <?php for ($i=0;$i<7;$i++) : ?>
            <div class="item masonry-grid-item col-md-4 col-sm-4 col-xs-6">
                <div class="listing-item white-bg bordered">
                    <a href="" title="Трубопроводная система RAUTITAN">
                        <span class="img">
                            <img src="http://pv.rehau.tesla.aristos.pw/media/catalog/category/M-401408_12_1.jpg"
                                 alt="Трубопроводная система RAUTITAN"
                                 title="Трубопроводная система RAUTITAN" />
                        </span>
                        <span class="text">
                            Трубопроводная система RAUTITAN
                        </span>
                    </a>
                </div>
            </div>
        <?php endfor; ?>

    </div>

<?php require_once '_bottom.php'; ?>