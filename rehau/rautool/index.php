<?php require_once '_top.php'; ?>

<style>
    .rehau-grid-mode {
        margin-left: 0 !important;
        overflow: hidden;
        width: 102% !important;
    }
    .rehau-grid-mode .tab-content {
        padding-top: 1px;
    }
    .rehau-grid-mode .masonry-grid-item,
    .rehau-grid-mode .masonry-grid-item:nth-child(4n) {
        margin-right: 20px !important;
    }
    @media (max-width: 762px) {
        .rehau-grid-mode {
            width: 100% !important;
        }
    }
</style>

<div class="row rehau-grid-mode">
        <!-- Tab panes Grid Mode -->
        <div class="tab-content clear-style category-products">
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10874"
                                        data-category-id="784"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10874/"
                                        id="compare-10874"
                                />
                                <label for="compare-10874">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 13149851001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-g2-jelektrogidravlicheskij-na-diametry-50-63-i-40-110.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-151643_1.jpg" alt="RAUTOOL G2, электрогидравлический на диаметры 50-63 и 40-110">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-151643_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-g2-jelektrogidravlicheskij-na-diametry-50-63-i-40-110.html">RAUTOOL G2, электрогидравлический на диаметры 50-63 и 40-110</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10874/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10874/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10480"
                                        data-category-id="784"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10480/"
                                        id="compare-10480"
                                />
                                <label for="compare-10480">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 11378251001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/smennyj-komplekt-75-110-dlja-gidravlicheskogo-instrumenta-g1-h-g1.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495714.jpg" alt="Сменный комплект 75-110 для гидравлического инструмента G1, H/G1">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495714.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/smennyj-komplekt-75-110-dlja-gidravlicheskogo-instrumenta-g1-h-g1.html">Сменный комплект 75-110 для гидравлического инструмента G1, H/G1</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10480/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10480/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10875"
                                        data-category-id="784"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10875/"
                                        id="compare-10875"
                                />
                                <label for="compare-10875">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 13149871001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-h-g1-mehaniko-gidravlicheskij-na-diametry-50-63.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/placeholder/stores/21/no_photo_1.jpg" alt="RAUTOOL H/G1, механико-гидравлический на диаметры 50-63">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/placeholder/stores/21/no_photo_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rautool-h-g1-mehaniko-gidravlicheskij-na-diametry-50-63.html">RAUTOOL H/G1, механико-гидравлический на диаметры 50-63</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10875/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10875/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9957"
                                        data-category-id="784"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9957/"
                                        id="compare-9957"
                                />
                                <label for="compare-9957">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12018021001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/zapressovochnye-tiski-g1-na-diametry-40-110-dlja-g2-h-g1-5.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-3998_1.jpg" alt="Запрессовочные тиски G1 на диаметры 40-110 для G2, H/G1">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-3998_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/zapressovochnye-tiski-g1-na-diametry-40-110-dlja-g2-h-g1-5.html">Запрессовочные тиски G1 на диаметры 40-110 для G2, H/G1</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9957/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9957/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10444"
                                        data-category-id="784"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10444/"
                                        id="compare-10444"
                                />
                                <label for="compare-10444">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 11399011001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rasshiritel-nye-nasadki-g1-dlja-rautool-g2-h-g1-8.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4117_13.jpg" alt="Расширительные насадки G1 для RAUTOOL G2, H/G1">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4117_13.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/rasshiritel-nye-nasadki-g1-dlja-rautool-g2-h-g1-8.html">Расширительные насадки G1 для RAUTOOL G2, H/G1</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10444/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10444/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-0">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10119"
                                        data-category-id="784"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10119/"
                                        id="compare-10119"
                                />
                                <label for="compare-10119">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 13152391001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/nozhnicy-truboreznye-do-dh-125-2.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-537347_1.jpg" alt="Ножницы труборезные до DH 125">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-537347_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/gidravlicheskii-montazhnyi-instrument-i-komplektujuschie-k-nemu-na-diametr-40-110/nozhnicy-truboreznye-do-dh-125-2.html">Ножницы труборезные до DH 125</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10119/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10119/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10872"
                                        data-category-id="780"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10872/"
                                        id="compare-10872"
                                />
                                <label for="compare-10872">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12174781001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-a-light2-kombi-kombinirovannyj-akkumuljatornyj.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-526910_1.jpg" alt="RAUTOOL A-light2 Kombi, комбинированный аккумуляторный">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-526910_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-a-light2-kombi-kombinirovannyj-akkumuljatornyj.html">RAUTOOL A-light2 Kombi, комбинированный аккумуляторный</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10872/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10872/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10876"
                                        data-category-id="780"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10876/"
                                        id="compare-10876"
                                />
                                <label for="compare-10876">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12024841001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-h2-mehaniko-gidravlicheskij.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-567475.jpg" alt="RAUTOOL H2, механико-гидравлический">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-567475.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-h2-mehaniko-gidravlicheskij.html">RAUTOOL H2, механико-гидравлический</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10876/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10876/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10879"
                                        data-category-id="780"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10879/"
                                        id="compare-10879"
                                />
                                <label for="compare-10879">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 11377641005</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-m1-mehanicheskij.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4007_1.jpg" alt="RAUTOOL M1, механический">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4007_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-m1-mehanicheskij.html">RAUTOOL M1, механический</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10879/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10879/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10881"
                                        data-category-id="780"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10881/"
                                        id="compare-10881"
                                />
                                <label for="compare-10881">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12168201001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-xpand-akkumuljatornyj-gidravlicheskij-jekspander-16-40-sistemy-qc-1.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-493902_1.jpg" alt="RAUTOOL Xpand, аккумуляторный гидравлический экспандер 16-40 системы QC ">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-493902_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/rautool-xpand-akkumuljatornyj-gidravlicheskij-jekspander-16-40-sistemy-qc-1.html">RAUTOOL Xpand, аккумуляторный гидравлический экспандер 16-40 системы QC </a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10881/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10881/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10034"
                                        data-category-id="780"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10034/"
                                        id="compare-10034"
                                />
                                <label for="compare-10034">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12141761001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/mehanicheskij-jekspander-16-32-sistemy-qc.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-443809_1.jpg" alt="Механический экспандер 16-32 системы QC">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-443809_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/mehanicheskij-jekspander-16-32-sistemy-qc.html">Механический экспандер 16-32 системы QC</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10034/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10034/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-1">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9938"
                                        data-category-id="780"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9938/"
                                        id="compare-9938"
                                />
                                <label for="compare-9938">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12036191001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/zapasnoj-jelektroakkumuljator-dlja-a-light2-az-ez-g2-xpand-kombi.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84471_1.jpg" alt="RAUTOOL A-light2, аккумуляторный гидравлический">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84471_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-16-40/zapasnoj-jelektroakkumuljator-dlja-a-light2-az-ez-g2-xpand-kombi.html">RAUTOOL A-light2, аккумуляторный гидравлический</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9938/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9938/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10430"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10430/"
                                        id="compare-10430"
                                />
                                <label for="compare-10430">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 11373441001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-m1-1.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-98863856.jpg" alt="Расширительные насадки 40 для М1">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-98863856.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-m1-1.html">Расширительные насадки 40 для М1</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10430/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10430/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10413"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10413/"
                                        id="compare-10413"
                                />
                                <label for="compare-10413">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12476441001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-40-h-6-0-stabil-dlja-n2-a-light-a-light2-az.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4105_1.jpg" alt="Расширительная насадка 40 х 6,0 stabil для Н2, А-light, A-light2, АЗ">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4105_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-40-h-6-0-stabil-dlja-n2-a-light-a-light2-az.html">Расширительная насадка 40 х 6,0 stabil для Н2, А-light, A-light2, АЗ</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10413/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10413/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10435"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10435/"
                                        id="compare-10435"
                                />
                                <label for="compare-10435">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12093941001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-xpand-2.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-341666.jpg" alt="Расширительные насадки 40 для Xpand">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-341666.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-xpand-2.html">Расширительные насадки 40 для Xpand</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10435/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10435/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10432"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10432/"
                                        id="compare-10432"
                                />
                                <label for="compare-10432">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12446111001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-n2-a-light2-az-1.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4106.jpg" alt="Расширительные насадки 40 для Н2, A-light2, АЗ">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4106.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-40-dlja-n2-a-light2-az-1.html">Расширительные насадки 40 для Н2, A-light2, АЗ</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10432/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10432/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9951"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9951/"
                                        id="compare-9951"
                                />
                                <label for="compare-9951">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12051331001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3-na-3-diametra.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-276305_1.jpg" alt="Запрессовочные тиски для Н2, A-light2, Kombi, A3 на 3 диаметра">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-276305_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3-na-3-diametra.html">Запрессовочные тиски для Н2, A-light2, Kombi, A3 на 3 диаметра</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9951/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9951/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9950"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9950/"
                                        id="compare-9950"
                                />
                                <label for="compare-9950">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12018011001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4013.jpg" alt="Запрессовочные тиски для Н2, A-light2, Kombi, A3">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4013.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light2-kombi-a3.html">Запрессовочные тиски для Н2, A-light2, Kombi, A3</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9950/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9950/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10416"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10416/"
                                        id="compare-10416"
                                />
                                <label for="compare-10416">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12484111001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-20-dlja-n2-a-light2-az-1.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-361051.jpg" alt="Расширительные насадки 16-20 для Н2, A-light2, АЗ">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-361051.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-20-dlja-n2-a-light2-az-1.html">Расширительные насадки 16-20 для Н2, A-light2, АЗ</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10416/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10416/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9946"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9946/"
                                        id="compare-9946"
                                />
                                <label for="compare-9946">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12017981001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-m1-3.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4002.jpg" alt="Запрессовочные тиски для М1">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4002.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-m1-3.html">Запрессовочные тиски для М1</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9946/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9946/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10446"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10446/"
                                        id="compare-10446"
                                />
                                <label for="compare-10446">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12174691001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nyj-nakonechnik-25-32-sistemy-qc-dlja-gidravlicheskogo-instrumenta-n2-a-light2-az.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495698.jpg" alt="Расширительный наконечник 25/32 системы QC для гидравлического инструмента Н2, A-light2, АЗ">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-495698.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nyj-nakonechnik-25-32-sistemy-qc-dlja-gidravlicheskogo-instrumenta-n2-a-light2-az.html">Расширительный наконечник 25/32 системы QC для гидравлического инструмента Н2, A-light2, АЗ</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10446/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10446/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9949"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9949/"
                                        id="compare-9949"
                                />
                                <label for="compare-9949">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12590491002</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light-a-light2-a2-az-kombi-2.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4000_4.jpg" alt="Запрессовочные тиски для Н2, A-light, A-light2, А2, АЗ, Kombi">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4000_4.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/zapressovochnye-tiski-dlja-n2-a-light-a-light2-a2-az-kombi-2.html">Запрессовочные тиски для Н2, A-light, A-light2, А2, АЗ, Kombi</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9949/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9949/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10428"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10428/"
                                        id="compare-10428"
                                />
                                <label for="compare-10428">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12446011001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-32-sistemy-ro-11.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4110_5.jpg" alt="Расширительные насадки 16-32 системы RO">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4110_5.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-nye-nasadki-16-32-sistemy-ro-11.html">Расширительные насадки 16-32 системы RO</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10428/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10428/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9854"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9854/"
                                        id="compare-9854"
                                />
                                <label for="compare-9854">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12141751001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/bystros-emnye-rasshiritel-nye-nasadki-16-32-sistemy-qc-11.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-442307_4.jpg" alt="Быстросъемные расширительные насадки 16-32 системы QC">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-442307_4.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/bystros-emnye-rasshiritel-nye-nasadki-16-32-sistemy-qc-11.html">Быстросъемные расширительные насадки 16-32 системы QC</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9854/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9854/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10414"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10414/"
                                        id="compare-10414"
                                />
                                <label for="compare-10414">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12687641001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-dlja-razval-covki-prisoedinitel-nyh-trubok-iz-medi-i-nerzhavejuschej-stali-15-h-1-0-mm.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4111_1.jpg" alt="Расширительная насадка RO для медных и стальных трубок">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4111_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/rasshiritel-naja-nasadka-dlja-razval-covki-prisoedinitel-nyh-trubok-iz-medi-i-nerzhavejuschej-stali-15-h-1-0-mm.html">Расширительная насадка RO для медных и стальных трубок</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10414/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10414/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-2">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10368"
                                        data-category-id="783"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10368/"
                                        id="compare-10368"
                                />
                                <label for="compare-10368">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12474941001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/pruzhinnyj-vkladysh-dlja-vygiba-trub-rautitan-stabil-1.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-271722_2.jpg" alt="Пружинный вкладыш для выгиба труб RAUTITAN stabil">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-271722_2.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapasnye-chasti-na-diametr-16-40/pruzhinnyj-vkladysh-dlja-vygiba-trub-rautitan-stabil-1.html">Пружинный вкладыш для выгиба труб RAUTITAN stabil</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10368/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10368/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-3">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10878"
                                        data-category-id="779"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10878/"
                                        id="compare-10878"
                                />
                                <label for="compare-10878">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12446211001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k14-x-1-5-mehanicheskij.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_2.jpg" alt="RAUTOOL K14 x 1,5, механический">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_2.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k14-x-1-5-mehanicheskij.html">RAUTOOL K14 x 1,5, механический</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10878/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10878/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-3">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10877"
                                        data-category-id="779"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10877/"
                                        id="compare-10877"
                                />
                                <label for="compare-10877">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12283961001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k10-x-1-1-mehanicheskij.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_1.jpg" alt="RAUTOOL K10 x 1,1, механический">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4198_1.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/mehanicheskii-montazhnyi-instrument-dlja-nadvizhki-gil-z-na-diametr-10-x-1-1-14-x-1-5/rautool-k10-x-1-1-mehanicheskij.html">RAUTOOL K10 x 1,1, механический</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10877/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10877/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-4">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="9958"
                                        data-category-id="781"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/9958/"
                                        id="compare-9958"
                                />
                                <label for="compare-9958">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 12036091001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapchasti-k-instrumentu/zarjadnoe-ustrojstvo-k-instrumentu-a-light2-az-ez-g2-xpand-kombi.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84470.jpg" alt="Зарядное устройство к инструменту A-light2 / АЗ /ЕЗ /G2 /Xpand / Kombi">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-84470.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/komplektujuschie-i-zapchasti-k-instrumentu/zarjadnoe-ustrojstvo-k-instrumentu-a-light2-az-ez-g2-xpand-kombi.html">Зарядное устройство к инструменту A-light2 / АЗ /ЕЗ /G2 /Xpand / Kombi</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/9958/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/9958/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-5">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10116"
                                        data-category-id="782"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10116/"
                                        id="compare-10116"
                                />
                                <label for="compare-10116">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 13152431001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/nozhnicy-truboreznye-dlja-rezki-polimernyh-metallopolimernyh-trub-do-dn-40-mm-4.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-425435_2.jpg" alt="Ножницы труборезные для резки полимерных/металлополимерных труб до Dн 40 мм">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-425435_2.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/nozhnicy-truboreznye-dlja-rezki-polimernyh-metallopolimernyh-trub-do-dn-40-mm-4.html">Ножницы труборезные для резки полимерных/металлополимерных труб до Dн 40 мм</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10116/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10116/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-4 masonry-grid-item tab-num-5">
                <div class="listing-item white-bg bordered">
                    <div class="row">
                        <div class="col-xs-4 col-md-12">
                            <div class="compare-link"><input
                                        type="checkbox"
                                        data-product-id="10473"
                                        data-category-id="782"
                                        name="compare"
                                        value="http://shop-rehau.ru/catalog/product_compare/add/product/10473/"
                                        id="compare-10473"
                                />
                                <label for="compare-10473">
                                    <span>Сравнить</span>
                                </label></div>
                            <div class="sku">Арт. 11372341001</div>
                            <div class="overlay-container">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/ruchnoj-fiksatornyj-zazhim-dlja-trub-16-17-20.html">
                                    <img src="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/300x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-147806.jpg" alt="Ручной фиксаторный зажим для труб 16/17/20">
                                </a>
                                <a class="overlay-link popup-img-single" href="http://shop-rehau.ru/media/catalog/product/cache/21/small_image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-147806.jpg"><i class="fa fa-search-plus"></i></a>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-12 flex-align">
                            <h3 class="card__product-title">
                                <a href="http://shop-rehau.ru/montazhnyi-instrument-rautool/dopolnitel-nyi-instrument/ruchnoj-fiksatornyj-zazhim-dlja-trub-16-17-20.html">Ручной фиксаторный зажим для труб 16/17/20</a>
                                <!--                                            -->                                        <span class="short-descr"></span>
                                <!--                                            -->                                    </h3>

                            <div class="body">
                                <div class="status">
                                    <div class="out-of-stock">
                                        <i class="material-icons pull-left">&#xE5CD;</i> Нет в наличии    </div>
                                </div>
                                <div class="rating"></div>
                                <div class="elements-list clearfix">

                                    <form class="card__buyButton" action="http://shop-rehau.ru/checkout/cart/add/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/product/10473/form_key/9MOBHUhoDWRG3CwA/" method="post"
                                          id="product_addtocart_form">
                                        <input name="form_key" type="hidden" value="9MOBHUhoDWRG3CwA" />
                                        <a href="http://shop-rehau.ru/productalert/add/stock/product_id/10473/uenc/aHR0cDovL3Nob3AtcmVoYXUucnUvbW9udGF6aG55aS1pbnN0cnVtZW50LXJhdXRvb2wuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                            Нет в наличии        </a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once '_bottom.php'; ?>