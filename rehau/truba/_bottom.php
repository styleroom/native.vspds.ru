</section>
<div class="container">
    <style>
        .home-info .col-sm-3 {width: 33.333%;}
        @media (max-width: 767px) {.home-info .col-sm-3 {width: 100% !important; }}
    </style>
    <div class="home-info row">
        <div class="block-title">
            <h2>Преимущества официального магазина</h2>
        </div>
        <div class="col-sm-3">
            <a href="/o-magazine">
                <img src="http://pv.rehau.tesla.aristos.pw/media/wysiwyg/rehau/home/rh_icon_1_garant.png" alt="" /><!-- home_features-icon.png -->
                <span>
                <h3>Фирменная гарантия</h3>
                <p>На всю продукцию в течение 10 лет!</p>
            </span>
            </a>
        </div>
        <!--
<div class="col-sm-3">
    <a href="/dostavka">
        <img src="http://pv.rehau.tesla.aristos.pw/media/wysiwyg/rehau/home/home_features-icon.png" alt="" />
        <span>
            <h3>бесплатная доставка</h3>
            <p>При сумме заказа от 10000 рублей.</p>
        </span>
    </a>
</div> -->
        <div class="col-sm-3">
            <a href="/vozvrat-tovara">
                <img src="http://pv.rehau.tesla.aristos.pw/media/wysiwyg/rehau/home/rh_icon_2_problem.png" alt="" />
                <span>
                <h3>возврат без проблем</h3>
                <p>По любой причине в течение 7 дней.</p>
            </span>
            </a>
        </div>
        <div class="col-sm-3">
            <a href="/samovivoz">
                <img src="http://pv.rehau.tesla.aristos.pw/media/wysiwyg/rehau/home/rh_icon_3_vyvoz.png" alt="" />
                <span>
                <h3>самовывоз</h3>
                <p>Пункты самовывоза в Москве</p>
            </span>
            </a>
        </div>
    </div></div>
<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
<!-- ================ -->
<footer id="footer" class="clearfix ">

    <!-- .footer start -->
    <!-- ================ -->
    <div class="footer">
        <div class="container">
            <div class="footer-inner">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="footer__ul-menu">
                            <li class="footer__ul-menu_name">Продукция</li>
                            <li class="footer__ul-menu_link">
                                <a href="/obogrev-ohlazhdenie-poverhnostei-rehau.html" title="">Тёплый пол</a>
                            </li>
                            <li class="footer__ul-menu_link">
                                <a href="/truboprovodnaja-sistema-rautitan.html" title="">Отопление и Водопровод</a>
                            </li>
                            <li class="footer__ul-menu_link">
                                <a href="/sistema-shumopogloschajuschei-kanalizacii-raupiano-plus.html" title="">Канализация</a>
                            </li>
                            <li class="footer__ul-menu_link">
                                <a href="/montazhnyi-instrument-rautool.html" title="">Всё для монтажа</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="footer__ul-menu">
                            <li class="footer__ul-menu_name">Информация для покупателей</li>
                            <li class="footer__ul-menu_link">
                                <a href="/dostavka" title="">Доставка</a>
                            <li class="footer__ul-menu_link">
                                <a href="/samovivoz" title="">Самовывоз</a>
                            </li>
                            <li class="footer__ul-menu_link">
                                <a href="/rehau-oplata" title="">Условия оплаты</a>
                            </li>
                            <li class="footer__ul-menu_link">
                                <a href="/vozvrat-tovara" title="">Гарантии и возврат</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="footer__ul-menu">
                            <li class="footer__ul-menu_name">Спецпредложения</li>
                            <li class="footer__ul-menu_link">
                                <a href="#" title="">Специальные предложения</a>
                            <li class="footer__ul-menu_link">
                                <a href="#" title="">Акции и скидки</a>
                            </li>
                            <li class="footer__ul-menu_link">
                                <a href="#" title="">Подарочные сертификаты</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2 pull-right col-xs-12">
                        <div class="mobile-position">
                            <a href="tel:+7 (495) 118-25-87" class="footer__phone">+7 (495) 118-25-87</a>
                            <div><a href="tel:8 (800) 350-09-61" class="footer__phone">8 (800) 350-09-61</a></div>
                        </div>
                        <div class="footer__work-time">
                            Пн-Пт: 09:00 - 21:00<br>
                            Сб-Вс: 09:00 - 19:00
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer__bottom">
                            <a href="/pravila-raboti" title="">Правила использования</a>
                            <a href="/o-magazine" title="">О магазине</a>
                            <a href="/contacts.html" title="">Контакты</a>
                            <span>© 2017 Официальный интернет-магазин REHAU</span>
                            <span class="footer__social">
            <a href="//vk.com/rehau_ru" target="_blank" title="" class="footer__social_vk"></a>
            <a href="//www.facebook.com/rehau.ru" target="_blank" title="" class="footer__social_fb"></a>
            <a href="//www.youtube.com/user/rehauea" target="_blank" title="" class="footer__social_youtube"></a>
            <a href="//www.instagram.com/rehaurussia/" target="_blank" title="" class="footer__social_instagram"></a>
        </span>
                        </div>
                    </div>                                    </div>
            </div>
        </div>
    </div>
    <!-- .footer end -->

</footer>
<!-- footer end --></div>




<div id="advancednewsletter-overlay" style="display:none"></div>
<div id="subscribe-please-wait" style="display:none;">
    <img src="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/images/opc-ajax-loader.gif" />&nbsp;Загружается...</div>
<div id="an-content" style="display:none"></div>
<script>var cartMessages={processing: "Обрабатывается",in_cart: "В корзину"}</script>
<script>var compareMessages={hide: "Скрыть",show: "Показать",_or_: " или "}</script>    <section id="compare-toolbar" class="compare-toolbar">
    <div class="compare-wrap">
        <div class="bar">
            <div class="title">
                <span class="groups"></span>
            </div>
        </div>
        <div class="content">
            <div class="compare-items">
                <p>У вас нет товаров для сравнения.</p>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var debugConfig;
    try {
        debugConfig = {
            "cache": {
                "config": {
                    "id": "config",
                    "cache_type": "Конфигурация",
                    "description": "System(config.xml, local.xml) and modules configuration files(config.xml).",
                    "tags": "CONFIG",
                    "status": 1
                },
                "layout": {
                    "id": "layout",
                    "cache_type": "Layouts",
                    "description": "Layout building instructions.",
                    "tags": "LAYOUT_GENERAL_CACHE_TAG",
                    "status": 1
                },
                "block_html": {
                    "id": "block_html",
                    "cache_type": "Blocks HTML output",
                    "description": "Page blocks HTML.",
                    "tags": "BLOCK_HTML",
                    "status": 0
                },
                "translate": {
                    "id": "translate",
                    "cache_type": "Переводы",
                    "description": "Translation files.",
                    "tags": "TRANSLATE",
                    "status": 1
                },
                "collections": {
                    "id": "collections",
                    "cache_type": "Collections Data",
                    "description": "Collection data files.",
                    "tags": "COLLECTION_DATA",
                    "status": 1
                },
                "eav": {
                    "id": "eav",
                    "cache_type": "EAV types and attributes",
                    "description": "Entity types declaration cache.",
                    "tags": "EAV",
                    "status": 1
                },
                "config_api": {
                    "id": "config_api",
                    "cache_type": "Web Services Configuration",
                    "description": "Web Services definition files (api.xml).",
                    "tags": "CONFIG_API",
                    "status": 1
                },
                "config_api2": {
                    "id": "config_api2",
                    "cache_type": "Web Services Configuration",
                    "description": "Web Services definition files (api2.xml).",
                    "tags": "CONFIG_API2",
                    "status": 1
                },
                "amshopby": {
                    "id": "amshopby",
                    "cache_type": "Amasty Improved Navigation",
                    "description": "Indexed data for filters and their options",
                    "tags": "AMSHOPBY",
                    "status": 1
                },
                "full_page": {
                    "id": "full_page",
                    "cache_type": "Page Cache",
                    "description": "Full page caching.",
                    "tags": "FPC",
                    "status": 1
                }
            }
        };
    } catch(e) {
        console.error('Ошибка получения конфигурации для панели отладки');
    }
</script>
<i id="aristos-debug-box-trigger" class="debug-box-trigger fa fa-2x fa-bug hvr-grow" title="Aristos Debug Panel"></i>
<nav id="aristos-debug-box" draggable="false">
    <header draggable="true">Aristos Debug Panel</header>
    <i id="debug-box-close" class="debug-box-trigger fa fa-times fa-2x hvr-grow" title="Закрыть панель"></i>
    <section class="content">
        <div class="panel-group" id="debug-panels" role="tablist" aria-multiselectable="true">
            <!--<div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Demo
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb2" type="checkbox"/>
                            <label class="tgl-btn" for="cb2"></label>
                            <span class="tgl-info">Config Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox" checked/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox"/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox"/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                        <div class="dbg-switch">
                            <input class="tgl tgl-ios" id="cb3" type="checkbox"/>
                            <label class="tgl-btn" for="cb3"></label>
                            <span class="tgl-info">Block Cache</span>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <section class="blocks"></section>
    </section>
    <i class="resize-handle fa fa-signal" aria-hidden="true"></i>
</nav>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/jquery.validate-1.17.0.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/jquery-validation/localization/messages_ru.min.js"></script>
<script type="text/javascript" src="//cdn.aristosgroup.ru/libs/bootstrap/bootstrap-3.3.7.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/fontawesome/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/js/lib/awesomplete/awesomplete.css" />
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/css/compare.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://pv.rehau.tesla.aristos.pw/skin/frontend/evo/rehau/css/compare.print.css" media="print" />
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/compare.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/clipboard/clipboard.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.move.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.swipe.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/awesomplete/awesomplete.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/sprintf/sprintf.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/modernizr.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/waypoints/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/jquery.countTo.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/vide/jquery.vide.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/jquery.browser.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/SmoothScroll.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/template.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/region.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/plugins/owl-carousel/owl.carousel2.thumbs.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/evo/rehau/libraries/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/evo/rehau/layout.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/evo/rehau/addtocart-input.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/debug.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/lib/jquery/jquery.pjax.js"></script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/js/aristos/pjax.js"></script>
<link rel="stylesheet" href="//cdn.aristosgroup.ru/fonts/helios-cond/font.css" />
<style type="text/css">@media print { #djDebug {display:none;}}</style>
<script type="text/javascript">
    // <![CDATA[
    var DEBUG_TOOLBAR_MEDIA_URL = "http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/debug/";
    // ]]>
</script>
<script type="text/javascript" src="http://pv.rehau.tesla.aristos.pw/skin/frontend/base/default/debug/js/toolbar.js"></script>

<div>Warning! Admitad Code for aristos_retag/product.js.phtml is not set</div><script type='application/ld+json'>
{"@context":"http:\/\/www.schema.org","@type":"Product","brand":"Rehau","logo":"http:\/\/pv.rehau.tesla.aristos.pw\/skin\/frontend\/evo\/rehau\/images\/logo.png","name":"Универсальная труба RAUTITAN flex для систем водоснабжения и отопления","category":"Универсальные трубы RAUTITAN","image":"http:\/\/pv.rehau.tesla.aristos.pw\/media\/catalog\/product\/M\/-\/M-4999_15.jpg","offers":{"@type":"Offer","sku":"11304301006","priceCurrency":"RUB","price":1401.4000000000001,"priceValidUntil":"2017-11-11","itemCondition":"http:\/\/schema.org\/NewCondition","availability":"http:\/\/schema.org\/InStock","seller":{"@type":"Organization","name":"интернет-магазин Rehau"}}}</script>
<!-- EXT:  -->
<script type="text/javascript">
    var product = {
        identifier: '11304301006',
        fn:         'Универсальная труба RAUTITAN flex для систем водоснабжения и отопления',
        price:      '1401.4',
        amount:     '1401.40',
        currency:   'RUB',
        brand:      '',
        description: '"\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b: \u043f\u043e\u043b\u0438\u044d\u0442\u0438\u043b\u0435\u043d, \u043c\u043e\u043b\u0435\u043a\u0443\u043b\u044f\u0440\u043d\u043e \u0441\u0448\u0438\u0442\u044b\u0439 \u043f\u0435\u0440\u043e\u043a\u0441\u0438\u0434\u043d\u044b\u043c \u043c\u0435\u0442\u043e\u0434\u043e\u043c (RAU-PE-Xa) \u0441\u043e\u0433\u043b\u0430\u0441\u043d\u043e DIN EN ISO 15875 \u0438 DVGW \u043d\u043e\u0440\u043c\u0430\u0442\u0438\u0432\u043e\u0432 W 544. \nDVGW-\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u043e\u043d\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440: DVGW DW-8501AU2200 (\u0434\u043e\u043f\u0443\u0441\u043a \u043a \u044d\u043a\u0441\u043f\u043b\u0443\u0430\u0442\u0430\u0446\u0438\u0438 \u0441\u0438\u0441\u0442\u0435\u043c\u044b). \n\u0420\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044f DIN Certco: 3V257 \u0420\u0415-\u0425\u0430.\n\u041a\u0438\u0441\u043b\u043e\u0440\u043e\u0434\u043e\u0437\u0430\u0449\u0438\u0442\u043d\u044b\u0439 \u0441\u043b\u043e\u0439 \u0441\u043e\u0433\u043b\u0430\u0441\u043d\u043e DIN 4726. \n\u0426\u0432\u0435\u0442: \u0441\u0435\u0440\u0435\u0431\u0440\u0438\u0441\u0442\u044b\u0439 RAL 9006 (\u0431\u0435\u043b\u044b\u0439 \u0430\u043b\u044e\u043c\u0438\u043d\u0438\u0439). \n\u041a\u043b\u0430\u0441\u0441 \u043e\u0433\u043d\u0435\u0441\u0442\u043e\u0439\u043a\u043e\u0441\u0442\u0438: \u04122, \u043d\u043e\u0440\u043c\u0430\u043b\u044c\u043d\u043e \u0432\u043e\u0441\u043f\u043b\u0430\u043c\u0435\u043d\u044f\u0435\u043c\u044b\u0439."',
        photo:      'http://pv.rehau.tesla.aristos.pw/media/catalog/product/cache/21/image/200x200/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4999_15.jpg',
        quantity:   '111',
        url:        'http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11304301006.html',
        category:   [],
        score:      '0',
        valid:      '1387238400'
    };

    product.category.push('RAUTITAN');
    product.category.push('Трубопроводная система RAUTITAN');
    product.category.push('Универсальные трубы RAUTITAN');

</script>
</body>
</html>