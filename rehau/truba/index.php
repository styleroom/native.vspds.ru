<?php require_once '_top.php'; ?>

<div class="container">
        <div class="row">
            <div class="main col-md-12" role="main">

                <div class="messages">
                </div>
                <!-- Modal rehauAlertSaved -->
                <div class="modal fade alert-saved" id="rehauAlertSaved" tabindex="-1" role="dialog" aria-labelledby="rehauAlertSavedLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Поздравляем!</strong> Подписка на оповещение сохранена!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="product-info" class="row mb-20">
                    <div class="col-md-7">

                        <div class="owl-carousel content-slider-with-thumbnails">
                            <div class="overlay-container overlay-visible">
                                <img src="http://pv.rehau.tesla.aristos.pw/media/catalog/product/cache/21/thumbnail/675x450/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4999_15.jpg" alt="Универсальная труба RAUTITAN flex для систем водоснабжения и отопления">
                                <a href="http://pv.rehau.tesla.aristos.pw/media/catalog/product/cache/21/image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-4999_15.jpg" class="popup-img overlay-link" title="Универсальная труба RAUTITAN flex для систем водоснабжения и отопления">
                                    <i class="icon-plus-1"></i>
                                </a>
                            </div>
                            <div class="overlay-container overlay-visible">
                                <img src="http://pv.rehau.tesla.aristos.pw/media/catalog/product/cache/21/thumbnail/675x450/9df78eab33525d08d6e5fb8d27136e95/M/-/M-33512_33.jpg" alt="Универсальная труба RAUTITAN flex для систем водоснабжения и отопления">
                                <a href="http://pv.rehau.tesla.aristos.pw/media/catalog/product/cache/21/image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-33512_33.jpg" class="popup-img overlay-link" title="Универсальная труба RAUTITAN flex для систем водоснабжения и отопления">
                                    <i class="icon-plus-1"></i>
                                </a>
                            </div>
                            <div class="overlay-container overlay-visible">
                                <img src="http://pv.rehau.tesla.aristos.pw/media/catalog/product/cache/21/thumbnail/675x450/9df78eab33525d08d6e5fb8d27136e95/M/-/M-34913_15.jpg" alt="Универсальная труба RAUTITAN flex для систем водоснабжения и отопления">
                                <a href="http://pv.rehau.tesla.aristos.pw/media/catalog/product/cache/21/image/800x/9df78eab33525d08d6e5fb8d27136e95/M/-/M-34913_15.jpg" class="popup-img overlay-link" title="Универсальная труба RAUTITAN flex для систем водоснабжения и отопления">
                                    <i class="icon-plus-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div id="product-description" class=" pv-30">
                            <div class="title-info">
                                <h1 class="page-title">Универсальная труба RAUTITAN flex для систем водоснабжения и отопления</h1>
                                <div class="headline"></div>
                                <div class="sku-row"> <span class="sku"><span class="sku-name">Артикул:</span> 11304301006</span>
                                    <span class="category-products compare"><input
                                                type="checkbox"
                                                data-product-id="9801"
                                                data-category-id="748"
                                                name="compare"
                                                value="http://pv.rehau.tesla.aristos.pw/catalog/product_compare/add/product/9801/"
                                                id="compare-9801"
                                        />
<label for="compare-9801">
<span>Сравнить</span>
</label></span>
                                </div>

                                <div class="short-descr">
                                    Материал: полиэтилен, молекулярно сшитый пероксидным методом (RAU-PE-Xa) согласно DIN EN ISO 15875 и DVGW нормативов W 544.
                                    DVGW-регистрационный номер: DVGW DW-8501AU2200 (допуск к эксплуатации системы).
                                    Регистрация DIN Certco: 3V257 РЕ-Ха.
                                    Кислородозащитный слой согласно DIN 4726.
                                    Цвет: серебристый RAL 9006 (белый алюминий).
                                    Класс огнестойкости: В2, нормально воспламеняемый.                </div>

                                <div class="rating">
                                </div>

                                <div class="price-wrap-input">

                                                        <span data-increment="6" class="price actual-price product1"
                                                              id="product-price-9801product">
                                    1 401 ₽                            </span>
                                    <span class="about-increment">6</span>
                                    <div id="product-buttons" class="product">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 6">
                                                Условия заказа: 6 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9801/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <div class="productPrice__buttonWrapper" data-sku="11304301006" data-increment="6" data-priceone="1401">
                                                <button class="productPrice__minus">-</button>
                                                <input class="productPrice__addRemoveProducts"
                                                       type="number" data-id="9801" name="qty" min="1" value="1">
                                                <button class="productPrice__plus">+</button>
                                                <div class="clearfix"></div>
                                            </div>
                                            <button type="submit" class="btn-add-to-cart margin-clear btn btn-sm btn-default btn-animated btn-in-stock">
                                                <span class="add-cart__text">Купить</span><span class="add-cart__icon"></span>
                                            </button>

                                        </form>                                                                    </div>
                                </div>

                                <div class="status-wrap">

                                    <div class="status">
                                        Доступность:    <div class="in-stock">
                                            <i class="material-icons pull-left">&#xE876;</i> Есть в наличии    </div>
                                    </div>
                                    <div class="shipping">
                                        <!--{SHIPPINGRATE_9353264ac15cd16120f3a41d38caaa00}-->    <a class="shipping citycourier_base">
                                            Доставка (г. Москва): <span class="price">350 ₽</span>    </a>
                                        <!--/{SHIPPINGRATE_9353264ac15cd16120f3a41d38caaa00}-->                        </div>

                                </div>
                            </div>
                            <div class="product-info-placeholder">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="alert alert-info" role="alert" style="font-size: 14px;">
                    <strong>Внимание!</strong> Все цены изначально указаны за едницу товара. Покупка же возможна только кратно определенному числу!
                </div>

                <section class="clearfix">
                    <!--{TARGETRULE_CATALOG_PRODUCT_LIST_RELATED_aec550be9b0896ed513d98470c9c9b17}--><!--/{TARGETRULE_CATALOG_PRODUCT_LIST_RELATED_aec550be9b0896ed513d98470c9c9b17}-->
                    <div class="product-nav-wrap">
                        <ul class="product-nav nav nav-tabs style-4 container">
                            <li>
                                <a href="#tab-json-to-table" id="tab_json-to-table">
                                    Типоразмеры                        </a>
                            </li>
                            <li>
                                <a href="#tab-productquestions" id="tab_productquestions">
                                    Вопросы и ответы                        </a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        <div  role="tabpanel" class="tab-pane" id="tab-json-to-table">
                            <h2>Типоразмеры</h2>
                            <table class="table table-striped jsontable-input" width="100%">
                                <thead>
                                <tr class="char-table__names">
                                    <td>Артикул</td>
                                    <td class="char-table__names_for-attributes">D (мм)</td>
                                    <td class="char-table__names_for-attributes">S (мм)</td>
                                    <td class="char-table__names_for-attributes">Dy (мм)</td>
                                    <td class="char-table__names_for-attributes">Объем (л/м)</td>
                                    <td class="char-table__names_for-attributes">Вес (кг/м)</td>
                                    <td class="char-table__names_for-attributes">Единица поставки</td>
                                    <td class="char-table__names_for-attributes">На поддоне (м)</td>
                                    <td class="char-table__names_for-attributes">Кол-во в упаковке (м)</td>
                                    <td>Цена</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="char-table__main "
                                    id="product-9811"
                                    data-sku="11303701006"
                                    data-id="9811"
                                    data-unit="6"
                                    data-increment="1" data-priceone="166">
                                    <td>
                                        <a class="char-table__product-name" href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11303701006.html">11303701006</a>
                                        <span class="mobile__char-table__arrow-down"></span>
                                    </td>
                                    <td class="char-table__product-attr">16</td>
                                    <td class="char-table__product-attr">2,2</td>
                                    <td class="char-table__product-attr">12</td>
                                    <td class="char-table__product-attr">0,106</td>
                                    <td class="char-table__product-attr">0,103</td>
                                    <td class="char-table__product-attr">6 м отрезки</td>
                                    <td class="char-table__product-attr">5400</td>
                                    <td class="char-table__product-attr">60</td>
                                    <td class="char-table__product-price input-price">                    <span class="price">166 ₽</span>                </td>
                                    <td class="char-table__product-buy">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 1">
                                                Условия заказа: 1 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9811/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <a href="http://pv.rehau.tesla.aristos.pw/productalert/add/stock/product_id/9811/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                Нет в наличии        </a>

                                        </form>                </td>
                                </tr>
                                <tr class="char-table__main "
                                    id="product-9809"
                                    data-sku="11303801006"
                                    data-id="9809"
                                    data-unit="6"
                                    data-increment="1" data-priceone="223">
                                    <td>
                                        <a class="char-table__product-name" href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11303801006.html">11303801006</a>
                                        <span class="mobile__char-table__arrow-down"></span>
                                    </td>
                                    <td class="char-table__product-attr">20</td>
                                    <td class="char-table__product-attr">2,8</td>
                                    <td class="char-table__product-attr">15</td>
                                    <td class="char-table__product-attr">0,163</td>
                                    <td class="char-table__product-attr">0,158</td>
                                    <td class="char-table__product-attr">6 м отрезки</td>
                                    <td class="char-table__product-attr">3240</td>
                                    <td class="char-table__product-attr">60</td>
                                    <td class="char-table__product-price input-price">                    <span class="price">223 ₽</span>                </td>
                                    <td class="char-table__product-buy">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 1">
                                                Условия заказа: 1 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9809/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <a href="http://pv.rehau.tesla.aristos.pw/productalert/add/stock/product_id/9809/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                Нет в наличии        </a>

                                        </form>                </td>
                                </tr>
                                <tr class="char-table__main "
                                    id="product-9807"
                                    data-sku="11303901006"
                                    data-id="9807"
                                    data-unit="6"
                                    data-increment="1" data-priceone="325">
                                    <td>
                                        <a class="char-table__product-name" href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11303901006.html">11303901006</a>
                                        <span class="mobile__char-table__arrow-down"></span>
                                    </td>
                                    <td class="char-table__product-attr">25</td>
                                    <td class="char-table__product-attr">3,5</td>
                                    <td class="char-table__product-attr">20</td>
                                    <td class="char-table__product-attr">0,254</td>
                                    <td class="char-table__product-attr">0,235</td>
                                    <td class="char-table__product-attr">6 м отрезки</td>
                                    <td class="char-table__product-attr">2160</td>
                                    <td class="char-table__product-attr">30</td>
                                    <td class="char-table__product-price input-price">                    <span class="price">325 ₽</span>                </td>
                                    <td class="char-table__product-buy">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 1">
                                                Условия заказа: 1 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9807/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <a href="http://pv.rehau.tesla.aristos.pw/productalert/add/stock/product_id/9807/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                Нет в наличии        </a>

                                        </form>                </td>
                                </tr>
                                <tr class="char-table__main "
                                    id="product-9805"
                                    data-sku="11304001006"
                                    data-id="9805"
                                    data-unit="6"
                                    data-increment="1" data-priceone="465">
                                    <td>
                                        <a class="char-table__product-name" href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11304001006.html">11304001006</a>
                                        <span class="mobile__char-table__arrow-down"></span>
                                    </td>
                                    <td class="char-table__product-attr">32</td>
                                    <td class="char-table__product-attr">4,4</td>
                                    <td class="char-table__product-attr">25</td>
                                    <td class="char-table__product-attr">0,423</td>
                                    <td class="char-table__product-attr">0,375</td>
                                    <td class="char-table__product-attr">6 м отрезки</td>
                                    <td class="char-table__product-attr">-</td>
                                    <td class="char-table__product-attr">30</td>
                                    <td class="char-table__product-price input-price">                    <span class="price">465 ₽</span>                </td>
                                    <td class="char-table__product-buy">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 1">
                                                Условия заказа: 1 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9805/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <a href="http://pv.rehau.tesla.aristos.pw/productalert/add/stock/product_id/9805/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                Нет в наличии        </a>

                                        </form>                </td>
                                </tr>
                                <tr class="char-table__main "
                                    id="product-9803"
                                    data-sku="11304101006"
                                    data-id="9803"
                                    data-unit="6"
                                    data-increment="1" data-priceone="722">
                                    <td>
                                        <a class="char-table__product-name" href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11304101006.html">11304101006</a>
                                        <span class="mobile__char-table__arrow-down"></span>
                                    </td>
                                    <td class="char-table__product-attr">40</td>
                                    <td class="char-table__product-attr">5,5</td>
                                    <td class="char-table__product-attr">32</td>
                                    <td class="char-table__product-attr">0,661</td>
                                    <td class="char-table__product-attr">0,584</td>
                                    <td class="char-table__product-attr">6 м отрезки</td>
                                    <td class="char-table__product-attr">-</td>
                                    <td class="char-table__product-attr">12</td>
                                    <td class="char-table__product-price input-price">                    <span class="price">722 ₽</span>                </td>
                                    <td class="char-table__product-buy">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 1">
                                                Условия заказа: 1 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9803/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <a href="http://pv.rehau.tesla.aristos.pw/productalert/add/stock/product_id/9803/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                Нет в наличии        </a>

                                        </form>                </td>
                                </tr>
                                <tr class="char-table__main "
                                    id="product-9802"
                                    data-sku="11304201006"
                                    data-id="9802"
                                    data-unit="6"
                                    data-increment="1" data-priceone="950">
                                    <td>
                                        <a class="char-table__product-name" href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11304201006.html">11304201006</a>
                                        <span class="mobile__char-table__arrow-down"></span>
                                    </td>
                                    <td class="char-table__product-attr">50</td>
                                    <td class="char-table__product-attr">6,9</td>
                                    <td class="char-table__product-attr">40</td>
                                    <td class="char-table__product-attr">1,029</td>
                                    <td class="char-table__product-attr">0,914</td>
                                    <td class="char-table__product-attr">6 м отрезки</td>
                                    <td class="char-table__product-attr">-</td>
                                    <td class="char-table__product-attr">6</td>
                                    <td class="char-table__product-price input-price">                    <span class="price">950 ₽</span>                </td>
                                    <td class="char-table__product-buy">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 1">
                                                Условия заказа: 1 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9802/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <a href="http://pv.rehau.tesla.aristos.pw/productalert/add/stock/product_id/9802/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/" class="btn-add-to-cart margin-clear btn btn-sm btn-gray-transparent btn-out-stock">
                                                Нет в наличии        </a>

                                        </form>                </td>
                                </tr>
                                <tr class="char-table__main char-table__main_this-product"
                                    id="product-9801"
                                    data-sku="11304301006"
                                    data-id="9801"
                                    data-unit="6"
                                    data-increment="6" data-priceone="1401">
                                    <td>
                                        <a class="char-table__product-name" href="http://pv.rehau.tesla.aristos.pw/truboprovodnaja-sistema-rautitan/truboprovodnaja-sistema-rautitan/universal-nye-truby-rautitan/universal-naja-truba-rautitan-flex-dlja-sistem-vodosnabzhenija-i-otoplenija-11304301006.html">11304301006</a>
                                        <span class="mobile__char-table__arrow-down"></span>
                                    </td>
                                    <td class="char-table__product-attr">63</td>
                                    <td class="char-table__product-attr">8,6</td>
                                    <td class="char-table__product-attr">50</td>
                                    <td class="char-table__product-attr">1,647</td>
                                    <td class="char-table__product-attr">1,458</td>
                                    <td class="char-table__product-attr">6 м отрезки</td>
                                    <td class="char-table__product-attr">-</td>
                                    <td class="char-table__product-attr">6</td>
                                    <td class="char-table__product-price input-price">                    <span class="price">1 401 ₽</span>                </td>
                                    <td class="char-table__product-buy">

                                        <div class="clearfix">
                                            <small class="order-term" data-toggle="popover" data-placement="top" data-container="body"
                                                   title="Условия заказа"
                                                   data-content="Товар продается в количестве кратном 6">
                                                Условия заказа: 6 <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </small>
                                        </div>

                                        <form class="card__buyButton" action="http://pv.rehau.tesla.aristos.pw/checkout/cart/add/uenc/aHR0cDovL3B2LnJlaGF1LnRlc2xhLmFyaXN0b3MucHcvdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdHJ1Ym9wcm92b2RuYWphLXNpc3RlbWEtcmF1dGl0YW4vdW5pdmVyc2FsLW55ZS10cnVieS1yYXV0aXRhbi91bml2ZXJzYWwtbmFqYS10cnViYS1yYXV0aXRhbi1mbGV4LWRsamEtc2lzdGVtLXZvZG9zbmFiemhlbmlqYS1pLW90b3BsZW5pamEtMTEzMDQzMDEwMDYuaHRtbA,,/product/9801/form_key/RSNCuhxl8gDEuYvR/" method="post"
                                        >
                                            <input name="form_key" type="hidden" value="RSNCuhxl8gDEuYvR" />
                                            <div class="productPrice__buttonWrapper" data-sku="11304301006" data-increment="6" data-priceone="1401">
                                                <button class="productPrice__minus">-</button>
                                                <input class="productPrice__addRemoveProducts"
                                                       type="number" data-id="9801" name="qty" min="1" value="1">
                                                <button class="productPrice__plus">+</button>
                                                <div class="clearfix"></div>
                                            </div>
                                            <button type="submit" class="btn-add-to-cart margin-clear btn btn-sm btn-default btn-animated btn-in-stock">
                                                <span class="add-cart__text">Купить</span><span class="add-cart__icon"></span>
                                            </button>

                                        </form>                </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div  role="tabpanel" class="tab-pane" id="tab-downloads">
                            <h2>Файлы</h2>

                        </div>
                        <div  role="tabpanel" class="tab-pane" id="tab-productquestions">
                            <h2>Вопросы и ответы</h2>
                            <div class="productquestions-container">

                                <h2>Вопросы по Универсальная труба RAUTITAN flex для систем водоснабжения и отопления</h2>

                                <p>По этому товару ещё не задавали вопросов</p>

                                <div class="hr"></div>


                                <script type="text/javascript">
                                    eval(function (p, a, c, k, e, d) {
                                        e = function (c) {
                                            return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
                                        };
                                        if (!''.replace(/^/, String)) {
                                            while (c--) {
                                                d[e(c)] = k[c] || e(c)
                                            }
                                            k = [function (e) {
                                                return d[e]
                                            }];
                                            e = function () {
                                                return '\\w+'
                                            };
                                            c = 1
                                        }
                                        ;
                                        while (c--) {
                                            if (k[c]) {
                                                p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c])
                                            }
                                        }
                                        return p
                                    }('1D=o(p){o Q(L,1x){h(L<<1x)|(L>>>(32-1x))}o e(1l,1j){f 1k,1o,C,D,w;C=(1l&1p);D=(1j&1p);1k=(1l&1n);1o=(1j&1n);w=(1l&1C)+(1j&1C);B(1k&1o){h(w^1p^C^D)}B(1k|1o){B(w&1n){h(w^2a^C^D)}1h{h(w^1n^C^D)}}1h{h(w^C^D)}}o F(x,y,z){h(x&y)|((~x)&z)}o G(x,y,z){h(x&z)|(y&(~z))}o H(x,y,z){h(x^y^z)}o I(x,y,z){h(y^(x|(~z)))}o m(a,b,c,d,x,s,u){a=e(a,e(e(F(b,c,d),x),u));h e(Q(a,s),b)};o j(a,b,c,d,x,s,u){a=e(a,e(e(G(b,c,d),x),u));h e(Q(a,s),b)};o i(a,b,c,d,x,s,u){a=e(a,e(e(H(b,c,d),x),u));h e(Q(a,s),b)};o l(a,b,c,d,x,s,u){a=e(a,e(e(I(b,c,d),x),u));h e(Q(a,s),b)};o 1F(p){f A;f P=p.1f;f 1q=P+8;f 1B=(1q-(1q%1G))/1G;f 1m=(1B+1)*16;f t=1J(1m-1);f S=0;f q=0;28(q<P){A=(q-(q%4))/4;S=(q%4)*8;t[A]=(t[A]|(p.1H(q)<<S));q++}A=(q-(q%4))/4;S=(q%4)*8;t[A]=t[A]|(1X<<S);t[1m-2]=P<<3;t[1m-1]=P>>>29;h t};o Y(L){f 1i="",1g="",1y,1d;1v(1d=0;1d<=3;1d++){1y=(L>>>(1d*8))&2h;1g="0"+1y.25(16);1i=1i+1g.26(1g.1f-2,2)}h 1i};o 1E(p){p=p.24(/\\r\\n/g,"\\n");f v="";1v(f n=0;n<p.1f;n++){f c=p.1H(n);B(c<1e){v+=J.E(c)}1h B((c>1Z)&&(c<1V)){v+=J.E((c>>6)|1W);v+=J.E((c&1t)|1e)}1h{v+=J.E((c>>12)|1Y);v+=J.E(((c>>6)&1t)|1e);v+=J.E((c&1t)|1e)}}h v};f x=1J();f k,1w,1r,1s,1u,a,b,c,d;f 1a=7,U=12,R=17,K=22;f 18=5,X=9,W=14,Z=20;f V=4,19=11,1b=16,M=23;f T=6,O=10,N=15,1c=21;p=1E(p);x=1F(p);a=2g;b=1U;c=2i;d=2f;1v(k=0;k<x.1f;k+=16){1w=a;1r=b;1s=c;1u=d;a=m(a,b,c,d,x[k+0],1a,2e);d=m(d,a,b,c,x[k+1],U,2b);c=m(c,d,a,b,x[k+2],R,2c);b=m(b,c,d,a,x[k+3],K,2d);a=m(a,b,c,d,x[k+4],1a,2j);d=m(d,a,b,c,x[k+5],U,1L);c=m(c,d,a,b,x[k+6],R,1K);b=m(b,c,d,a,x[k+7],K,1O);a=m(a,b,c,d,x[k+8],1a,1R);d=m(d,a,b,c,x[k+9],U,1Q);c=m(c,d,a,b,x[k+10],R,1M);b=m(b,c,d,a,x[k+11],K,1N);a=m(a,b,c,d,x[k+12],1a,1T);d=m(d,a,b,c,x[k+13],U,1P);c=m(c,d,a,b,x[k+14],R,1S);b=m(b,c,d,a,x[k+15],K,27);a=j(a,b,c,d,x[k+1],18,2H);d=j(d,a,b,c,x[k+6],X,3a);c=j(c,d,a,b,x[k+11],W,30);b=j(b,c,d,a,x[k+0],Z,2Q);a=j(a,b,c,d,x[k+5],18,2T);d=j(d,a,b,c,x[k+10],X,2k);c=j(c,d,a,b,x[k+15],W,2Z);b=j(b,c,d,a,x[k+4],Z,2V);a=j(a,b,c,d,x[k+9],18,2U);d=j(d,a,b,c,x[k+14],X,2X);c=j(c,d,a,b,x[k+3],W,2Y);b=j(b,c,d,a,x[k+8],Z,2O);a=j(a,b,c,d,x[k+13],18,2N);d=j(d,a,b,c,x[k+2],X,2P);c=j(c,d,a,b,x[k+7],W,2R);b=j(b,c,d,a,x[k+12],Z,37);a=i(a,b,c,d,x[k+5],V,39);d=i(d,a,b,c,x[k+8],19,3b);c=i(c,d,a,b,x[k+11],1b,3c);b=i(b,c,d,a,x[k+14],M,38);a=i(a,b,c,d,x[k+1],V,33);d=i(d,a,b,c,x[k+4],19,34);c=i(c,d,a,b,x[k+7],1b,35);b=i(b,c,d,a,x[k+10],M,36);a=i(a,b,c,d,x[k+13],V,31);d=i(d,a,b,c,x[k+0],19,2S);c=i(c,d,a,b,x[k+3],1b,2L);b=i(b,c,d,a,x[k+6],M,2t);a=i(a,b,c,d,x[k+9],V,2s);d=i(d,a,b,c,x[k+12],19,2m);c=i(c,d,a,b,x[k+15],1b,2l);b=i(b,c,d,a,x[k+2],M,2n);a=l(a,b,c,d,x[k+0],T,2o);d=l(d,a,b,c,x[k+7],O,2p);c=l(c,d,a,b,x[k+14],N,2x);b=l(b,c,d,a,x[k+5],1c,2y);a=l(a,b,c,d,x[k+12],T,2I);d=l(d,a,b,c,x[k+3],O,2J);c=l(c,d,a,b,x[k+10],N,2K);b=l(b,c,d,a,x[k+1],1c,2F);a=l(a,b,c,d,x[k+8],T,2E);d=l(d,a,b,c,x[k+15],O,2A);c=l(c,d,a,b,x[k+6],N,2z);b=l(b,c,d,a,x[k+13],1c,2C);a=l(a,b,c,d,x[k+4],T,2D);d=l(d,a,b,c,x[k+11],O,2q);c=l(c,d,a,b,x[k+2],N,2B);b=l(b,c,d,a,x[k+9],1c,2G);a=e(a,1w);b=e(b,1r);c=e(c,1s);d=e(d,1u)}f 1A=Y(a)+Y(b)+Y(c)+Y(d);h 1A.2M()};2r=o(){f 1z=$(\'1I\').2w();B(!2v(1z))$(\'1I\').2u(1D(1z));h 2W}', 62, 199, '||||||||||||||AddUnsigned|var||return|HH|GG||II|FF||function|string|lByteCount|||lWordArray|ac|utftext|lResult||||lWordCount|if|lX8|lY8|fromCharCode|||||String|S14|lValue|S34|S43|S42|lMessageLength|RotateLeft|S13|lBytePosition|S41|S12|S31|S23|S22|WordToHex|S24|||||||||S21|S32|S11|S33|S44|lCount|128|length|WordToHexValue_temp|else|WordToHexValue|lY|lX4|lX|lNumberOfWords|0x40000000|lY4|0x80000000|lNumberOfWords_temp1|BB|CC|63|DD|for|AA|iShiftBits|lByte|auth_key|temp|lNumberOfWords_temp2|0x3FFFFFFF|awpq_getSpamCodeValue|Utf8Encode|ConvertToWordArray|64|charCodeAt|#question_antispam_code_field|Array|0xA8304613|0x4787C62A|0xFFFF5BB1|0x895CD7BE|0xFD469501|0xFD987193|0x8B44F7AF|0x698098D8|0xA679438E|0x6B901122|0xEFCDAB89|2048|192|0x80|224|127|||||replace|toString|substr|0x49B40821|while||0xC0000000|0xE8C7B756|0x242070DB|0xC1BDCEEE|0xD76AA478|0x10325476|0x67452301|255|0x98BADCFE|0xF57C0FAF|0x2441453|0x1FA27CF8|0xE6DB99E5|0xC4AC5665|0xF4292244|0x432AFF97|0xBD3AF235|awpq_antispam|0xD9D4D039|0x4881D05|val|isNaN|val|0xAB9423A7|0xFC93A039|0xA3014314|0xFE2CE6E0|0x2AD7D2BB|0x4E0811A1|0xF7537E82|0x6FA87E4F|0x85845DD1|0xEB86D391|0xF61E2562|0x655B59C3|0x8F0CCC92|0xFFEFF47D|0xD4EF3085|toLowerCase|0xA9E3E905|0x455A14ED|0xFCEFA3F8|0xE9B6C7AA|0x676F02D9|0xEAA127FA|0xD62F105D|0x21E1CDE6|0xE7D3FBC8|true|0xC33707D6|0xF4D50D87|0xD8A1E681|0x265E5A51|0x289B7EC6||0xA4BEEA44|0x4BDECFA9|0xF6BB4B60|0xBEBFBC70|0x8D2A4C8A|0xFDE5380C|0xFFFA3942|0xC040B340|0x8771F681|0x6D9D6122'.split('|'), 0, {}));
                                </script>


                                <div class="productquestions-container">

                                    <h2>Задайте свой вопрос о товаре</h2>

                                    <form action="http://pv.rehau.tesla.aristos.pw/productquestions/index/post/id/9801/" method="post" id="productquestions-form"
                                          onsubmit="return awpq_antispam();">

                                        <input type="hidden" id="question_antispam_code_field" name="question_antispam_code"
                                               value="607176536">
                                        <input type="hidden" name="product_id" value="9801"/>

                                        <div class="productquestions-form">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-4">
                                                    <div class="form-field">
                                                        <label for="question_author_name_field"
                                                               class="required">Имя (будет отображено на сайте)</label>
                                                        <input class="form-control required-entry" name="question_author_name"
                                                               id="question_author_name_field" type="text"
                                                               value=""/>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <div class="form-field">
                                                        <label for="question_author_email_field"
                                                               class="required">Email (будет скрыт)</label>
                                                        <input class="form-control required-entry" name="question_author_email"
                                                               id="question_author_email_field" type="text"
                                                               value=""/>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <div class="form-field">
                                                        <label class="required"
                                                               for="questions_status_field">Видимость</label>
                                                        <select name="question_status" id="questions_status_field" class="form-control">
                                                            <option
                                                                    value="1">Публичный (увидят все посетители)</option>
                                                            <option
                                                                    value="2">Скрытый</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="form-field">
                                                        <label for="question_text_field" class="required">Вопрос</label>
                                                        <textarea rows="10" class="form-control required-entry" name="question_text"
                                                                  id="question_text_field"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-4">
                                                    <div class="hint">Пожалуйста, заполните все поля.
                                                        Ответ на непубличные вопросы мы
                                                        пришлем вам на электронную почту.
                                                        Убедитесь, что ввели адрес правильно.</div>
                                                    <div class="button-set">
                                                        <button class="btn btn-default form-button"
                                                                type="submit">Отправить</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </form>

                                </div>
                            </div>                </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

<?php require_once '_bottom.php'; ?>